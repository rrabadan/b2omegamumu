### B2omegaMuMu analysis

## Overview

This project contains workflows and tools to search for the $B^{0}\to\omega\mu^{+}\mu^{-}$ and $B_{s}^{0}\to\eta\mu^{+}\mu^{-}$ decays.

## Getting started

If running in lxplus or any other machine with CVMFS:

```
lb-conda-dev virtual-env default myenv
myenv/run bash
```

```
pip install poetry
poetry config virtualenvs.create false
# poetry config virtualenvs.path /path/to/myenv
```