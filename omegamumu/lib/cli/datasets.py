import click
import pprint

from omegamumu.datasets.remote import apd_data, apd_dataset
from omegamumu.datasets.local import get_datasets_from_json, get_local_data


@click.group()
def datasets_cli():
    """Collection of commands to explore romote and local datasets"""
    pass


@datasets_cli.command()
def show_apd_all():
    """
    Lists all AP datasets
    """
    for eventtype, year, polarity, decay in apd_data():
        print(eventtype, year, polarity, decay)


@datasets_cli.command()
@click.argument("eventtype", nargs=1, type=click.STRING)
@click.argument("datatype", nargs=1, type=click.STRING)
@click.argument("magpol", nargs=1, type=click.STRING)
@click.option(
    "--decay",
    "-d",
    nargs=1,
    type=click.STRING,
    multiple=False,
    default="omegamumu",
    help="The decay mode",
)
def show_apd_dataset(eventtype, datatype, magpol, decay):
    """
    Print dataset name and files for a given eventtype, datatype, magpol and decay
    """
    dataset = apd_dataset(eventtype, datatype, magpol, decay)
    print(dataset.keys)


@datasets_cli.command()
def private_datasets():
    """
    Lists privately produced datasets from a JSON file
    specified in the config file `privateNtuples`
    """
    datasets = get_datasets_from_json()
    for eventtype, polarity, datatype, files in datasets:
        print(eventtype, datatype, polarity)


@datasets_cli.command()
def local_data():
    """Shows the local data samples"""
    pprint.pprint(get_local_data())


if __name__ == "__main__":
    datasets_cli()
