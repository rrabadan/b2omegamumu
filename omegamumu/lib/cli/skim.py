import os
import click

from omegamumu.common.utils import check_file_to_write
from omegamumu.datasets.config import use_apdinput, get_stripping_ntuple_tree_name
from omegamumu.datasets.remote import apd_data, apd_dataset
from omegamumu.datasets.local import (
    get_files_from_json,
    get_datasets_from_json,
    get_local_data_file,
)

from omegamumu.selection.skimming import skim


@click.group()
def selection_cli():
    """Selection commands"""
    pass


@selection_cli.command(
    help="""
    Apply preselection to a single sample.
    
    DECAY: Decay mode [omegamumu, kstplusmumu, rhoplusmumu].
    EVENTTYPE: The event type to process.
    DATATYPE: The data type to process.
    POLARITY: The polarity to process.
    REPORTDIR: The directory to write the report.
"""
)
@click.argument("decay", nargs=1, type=click.STRING)
@click.argument("eventtype", nargs=1, type=click.STRING)
@click.argument("datatype", nargs=1, type=click.STRING)
@click.argument("polarity", nargs=1, type=click.STRING)
@click.argument("reportdir", nargs=1, type=click.STRING)
@click.option("--pid", is_flag=True, help="Apply PID cuts")
def preselection_skim(decay, eventtype, datatype, polarity, reportdir, pid):
    if use_apdinput():
        d = decay if eventtype == 90000000 else "all"
        files = apd_dataset(eventtype, datatype, polarity, decay=d).keys
    else:
        files = get_files_from_json(eventtype, datatype, polarity)

    tree = get_stripping_ntuple_tree_name(decay)
    print("Processing:", eventtype, datatype, polarity, tree)
    print("Number of files:", len(files))
    skimfile = get_local_data_file(
        decay, eventtype, datatype, polarity, group="preselection"
    )
    print("writting skimfile:", skimfile)
    if not check_file_to_write(skimfile):
        return
    label = f"skim_{decay}_{eventtype}_{polarity}_{datatype}"
    skimreportfile = os.path.join(reportdir, f"{label}.json")
    skim(tree, files, decay, skimfile, skimreportfile, pid=pid)


@selection_cli.command(
    help=""" 
    Apply preselection to samples.
    
    DECAY: Decay mode [omegamumu, kstplusmumu, rhoplusmumu]
"""
)
@click.argument("decay", nargs=1, type=click.STRING)
@click.argument("reportdir", nargs=1, type=click.STRING)
@click.option("--pid", is_flag=True, help="Apply PID cuts")
@click.option("--dataonly", is_flag=True, help="Skip mc datasets")
@click.option("--mconly", is_flag=True, help="Skip data datasets")
def skim_datasets(decay, reportdir, pid, dataonly, mconly):
    datasets = []
    if use_apdinput():
        for eventtype, datatype, polarity, mode in apd_data():
            dectag = decay if int(eventtype) == 90000000 else "all"
            if mconly and int(eventtype) == 90000000:
                continue
            if dataonly and int(eventtype) != 90000000:
                continue
            if int(eventtype) == 90000000 and mode != decay.lower():
                continue
            files = apd_dataset(eventtype, datatype, polarity, decay=dectag).keys
            datasets += [(eventtype, polarity, datatype, files)]
            print(eventtype, polarity, datatype, mode, len(files))
    else:
        datasets = get_datasets_from_json()

    tree = get_stripping_ntuple_tree_name(decay)
    for (
        eventtype,
        polarity,
        datatype,
        files,
    ) in datasets:
        print("Processing:", eventtype, datatype, polarity, "with", len(files), "files")
        skimfile = get_local_data_file(
            decay, eventtype, datatype, polarity, group="preselection"
        )
        print("writting skimfile:", skimfile)
        if not check_file_to_write(skimfile):
            continue
        label = f"skim_{decay}_{eventtype}_{polarity}_{datatype}"
        skimreportfile = reportdir + f"/{label}.json"
        skim(tree, files, decay, skimfile, skimreportfile, pid=pid)


if __name__ == "__main__":
    selection_cli()
