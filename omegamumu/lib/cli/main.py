import click

from .skim import selection_cli
from .datasets import datasets_cli
from .correction import corrections_cli

# from locations import Locations


@click.group()
def cli():
    """Collection of commands for the analysis"""
    pass


# @click.group()
# def artifacts_cli():
#    """Commands to manage the artifacts directory"""
#    pass
#
#
# @artifacts_cli.command()
# def init():
#    """Create the artifacts directory structure"""
#    Locations.create_artifacts_dirs()
#
#
# @artifacts_cli.command()
# def clean_all():
#    """Remove the artifacts directory"""
#    Locations.remove_artifacts_dirs()


# Include specific CLI commands from each directory
# cli.add_command(artifacts_cli, name="artifacts")
cli.add_command(selection_cli, name="skim")
cli.add_command(datasets_cli, name="datasets")
cli.add_command(corrections_cli, name="corrections")

if __name__ == "__main__":
    cli()
