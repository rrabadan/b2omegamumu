import click

from omegamumu.common.utils import file_exists
from omegamumu.datasets.local import get_local_data_file, get_local_data

from omegamumu.corrections.utils import copy_tree_pid


@click.group()
def corrections_cli():
    """some tools for mc corrections"""
    pass


@corrections_cli.command(
    help="""
    Generate a duplicate tree containing only the branches necessary for PID corrections.
                       
    DECAY: Decay mode [omegamumu, kstplusmumu, rhoplusmumu].
    EVENTTYPE: Event type.
    DATATYPE: Data type.
    POLARITY: Polarity.
"""
)
@click.argument("decay", default="omegamumu")
@click.argument("eventtype", nargs=1, type=click.INT)
@click.argument("datatype", nargs=1, type=click.INT)
@click.argument("polarity", nargs=1, type=click.STRING)
@click.option("--group", default="preselection")
@click.option("--treename", default="DecayTree")
def clone_pid_tree(decay, eventtype, datatype, polarity, group, treename):
    file = get_local_data_file(decay, eventtype, datatype, polarity, group=group)
    if not file_exists(file):
        raise ValueError(f"File {file} does not exist")

    copy_tree_pid(file, decay, treename)


@corrections_cli.command(
    help="""
    Generate duplicate trees containing only the branches necessary for PID corrections.
"""
)
@click.argument("decay", default="B2omegaMuMu")
@click.option("--group", default="preselection")
@click.option("--treename", default="DecayTree")
def clone_pid_trees(decay, group, treename):
    datasets = get_local_data()
    for eventtype in datasets[decay].keys():
        if eventtype == "90000000":
            continue
        for _group in datasets[decay][eventtype].keys():
            if _group != group:
                continue
            for ds in datasets[decay][eventtype][_group]:
                polarity, datatype = ds.split(".")[0].split("_")
                file = get_local_data_file(
                    decay, eventtype, datatype, polarity, group=_group
                )
                print(file)
                copy_tree_pid(file, decay, treename)


@corrections_cli.command(
    help="""
    Generate a correction from a histogram in a ROOT file.

    FILE: Path to the ROOT file.
    NAME: Name of the histogram.
    DESCRIPTION: Description of the correction.
    OUTPUT: Path to the output JSON file.
"""
)
@click.argument("file", type=click.Path(exists=True))
@click.argument("name", type=click.STRING)
@click.argument("description", type=click.STRING)
@click.argument("output", type=click.STRING)
def generate_json_correction(file, name, description, output):
    print(f"Generating {name} correction from {file}")
    print(f"Description: {description}")
    import omegamumu.corrections.correction as correction

    corr = correction.from_uproot_hist(file, name, description)
    if not output.endswith(".json"):
        output += ".json"
    with open(output, "w") as fout:
        fout.write(corr.json(exclude_unset=True))


@corrections_cli.command(
    help="""
    List the correction histogram in a ROOT file.

    FILE: Path to the ROOT file.
"""
)
@click.argument("file", type=click.Path(exists=True))
def dump_correction_hists(file):
    import uproot

    try:
        f = uproot.open(file)
    except FileNotFoundError:
        msg = "File", file, "not found"
        raise FileNotFoundError(msg) from None

    hist_keys = f.keys(filter_classname=["TH*"], cycle=False, recursive=False)
    for key in hist_keys:
        print(key)


if __name__ == "__main__":
    corrections_cli()
