import re
import numpy as np
import awkward as ak
import pandas as pd
import logging

import ROOT

import omegamumu.selection.cuts as cuts

from omegamumu.selection.skimming import preselection, pidselection
from omegamumu.common.rootutils import write_cuts_report
from omegamumu.selection.extend import (
    extend_dtf,
    extend_dimuon_q2,
    extend_pidsel,
    extend_gammapidsel,
)
from omegamumu.selection.selection import xcand_window, resonant_q2_veto
from omegamumu.selection.mctruth import extend_mctruth

from omegamumu.corrections import evaluate_corr_weights


decay_to_candidate = {
    "omegamumu": ("B", "omega"),
    "kstplusmumu": ("Bplus", "kstplus"),
    "rhoplusmumu": ("Bplus", "rhoplus"),
}


def load_and_process_sim_files(
    file,
    decay,
    mode,
    xwindow,
    year,
    pol,
    gfriend=None,
    cfriend=None,
    branches=None,
    br_pattern=None,
    mctruth=None,
    treename="DecayTree",
):
    """
    Load and process simulation files.

    Parameters:
    - file (str): Path to the main ROOT file.
    - decay (str): Type of decay (e.g., 'omegamumu').
    - mode (str): Dimuon decay mode (e.g., 'jpsi').
    - xwindow (string): Name of the x0 mass window (eta, omega, kst, rho, phi, None).
    - year (int): Year of the data.
    - pol (str): Polarity of the data.
    - gfriend (str): Path to the friend ROOT file.
    - cfriend (str): Path to the friend ROOT file.
    - branches (list): List of additional branches to include.
    - br_pattern (str or re.Pattern, optional): Regular expression pattern to match additional branches.
    - mctruth (dict, optional): Dictionary for MC truth matching.
    - treename (str, optional): Name of the tree in the ROOT file. Default is 'DecayTree'.

    Returns:
    - pd.DataFrame: Processed data as a pandas DataFrame.
    """

    f = ROOT.TFile.Open(file)
    t = f.Get(treename)
    t.ResetBit(ROOT.TTree.EStatusBits.kEntriesReshuffled)

    if gfriend:
        logging.info(f"Adding pidgen friend: {gfriend}")
        gffr = ROOT.TFile.Open(gfriend)
        t_gfr = gffr.Get(treename)
        t_gfr.ResetBit(ROOT.TTree.EStatusBits.kEntriesReshuffled)
        t.AddFriend(t_gfr, "pidgen")

    if cfriend:
        logging.info(f"Adding pidcorr friend: {cfriend}")
        cffr = ROOT.TFile.Open(cfriend)
        t_cfr = cffr.Get(treename)
        t_cfr.ResetBit(ROOT.TTree.EStatusBits.kEntriesReshuffled)
        t.AddFriend(t_cfr, "pidcorr")

    if mode == "jpsi":
        dimuon_mass_cut = "&&".join([c.format("dimuon") for c in cuts.jpsi_cut])
    elif mode == "psi2s":
        dimuon_mass_cut = "&&".join([c.format("dimuon") for c in cuts.psi2s_cut])
    else:
        dimuon_mass_cut = None

    b_cand = "B" if decay == "omegamumu" else "Bplus"

    df = ROOT.RDataFrame(t)
    ROOT.RDF.Experimental.AddProgressBar(df)

    df = preselection(df, decay=decay)

    if dimuon_mass_cut:
        df = df.Filter(dimuon_mass_cut, "resonant dimuon")

    x0_cand = (
        "omega"
        if decay == "omegamumu"
        else ("kstplus" if decay == "kstplusmumu" else "rhoplus")
    )
    if xwindow:
        xwcut = xcand_window(x0_cand, xwindow, op="&&")
        df = df.Filter(xwcut, "x mass window")

    df = extend_dimuon_q2(df)
    df = extend_dtf(df, decay=decay)
    df = extend_gammapidsel(df)

    if cfriend:
        df = extend_pidsel(df, decay=decay, prefix="", pidcorr=True)
    if gfriend:
        df = extend_pidsel(df, decay=decay, prefix="", pidgen=True)

    if mctruth is not None:
        df = extend_mctruth(df, mctruth)

    report = df.Report()

    # Define the base pattern
    base_pattern = re.compile(
        r"^(true|"
        r"(dimuon|kplus|pi|mu).*_(P|PE|PX|PY|PZ)$|"
        r".*_ProbNN(mu|pi|p|k)$|"
        r".*_PID(mu|p|K)$|"
        r".*_corr$|"
        r".*pidgen_default$|"
        r"pid.*_sel$)"
    )

    # Compile the branch pattern if provided
    if br_pattern:
        if isinstance(br_pattern, str):
            br_pattern = re.compile(br_pattern)
        pattern = re.compile(f"({base_pattern.pattern})|({br_pattern.pattern})")
    else:
        pattern = base_pattern

    # Get and filter column names
    cols = [
        str(c).split(".")[1] if "." in c else str(c)
        for c in df.GetColumnNames()
        if pattern.match(str(c))
    ]

    # Add additional columns
    cols += [
        "runNumber",
        "eventNumber",
        "Polarity",
        f"{b_cand}_BKGCAT",
        f"{x0_cand}_BKGCAT",
        "dimuon_q2",
        "gammapid_sel",
    ]

    if branches:
        cols += branches

    arr = ak.to_dataframe(ak.from_rdataframe(df, set(cols)))
    report.Print()

    corrs = pd.DataFrame(evaluate_corr_weights(arr, decay, year, pol))
    ycol = pd.DataFrame({"year": np.full_like(arr.runNumber, year)})

    arr = pd.concat([arr, corrs, ycol], axis=1)

    narr = arr.copy()

    del arr

    narr["candNumber"] = narr.groupby(["runNumber", "eventNumber"]).cumcount() + 1

    return narr


def load_and_process_data_files(
    files,
    decay,
    mode,
    xwindow=None,
    branches=None,
    br_pattern=None,
    treename="DecayTree",
    cutreport=None,
):
    """
    Load and process data files.

    Parameters:
    - files (str): list of ROOT files.
    - decay (str): Type of decay (e.g., 'omegamumu').
    - xwindow (string): Name of the x0 mass window (eta
    - branches (list): List of additional branches to include.
    - br_pattern (str or re.Pattern, optional): Regular expression pattern to match additional branches.
    - treename (str, optional): Name of the tree in the ROOT file. Default is 'DecayTree'.

    Returns:
    - pd.DataFrame: Processed data as a pandas DataFrame.
    """

    if mode == "jpsi":
        dimuon_mass_cut = "&&".join([c.format("dimuon") for c in cuts.jpsi_cut])
    elif mode == "psi2s":
        dimuon_mass_cut = "&&".join([c.format("dimuon") for c in cuts.psi2s_cut])
    elif mode == "rare":
        dimuon_mass_cut = resonant_q2_veto("dimuon")
    else:
        raise ValueError(f"Invalid decay mode: {mode}")
    # b_cand = 'B' if decay == 'omegamumu' else 'Bplus'

    ROOT.ROOT.EnableImplicitMT()
    df = ROOT.RDataFrame(treename, files)
    ROOT.RDF.Experimental.AddProgressBar(df)

    df = preselection(df, decay=decay)
    df = pidselection(df, decay=decay)
    df = extend_dimuon_q2(df)
    df = extend_dtf(df, decay=decay, psi2s=(mode == "psi2s"))

    df = df.Filter(dimuon_mass_cut, "dimuon mode mass cut")

    if xwindow:
        x0_cand = (
            "omega"
            if decay == "omegamumu"
            else ("kstplus" if decay == "kstplusmumu" else "rhoplus")
        )
        xwcut = xcand_window(x0_cand, xwindow, op="&&")
        df = df.Filter(xwcut, "x mass window")

    report = df.Report()

    base_pattern = re.compile(
        r"^(dimuon|kplus|rhoplus|pi|mu).*_(P|PE|PX|PY|PZ)$|"
        r".*_ProbNN(mu|pi|p|k)$|"
        r".*_PID(mu|p|K)$"
    )
    # base_pattern = re.compile(r"^(dimuon_P|kplus.*_P(?!ID)|pi.*_P(?!ID)|mu.*_P(?!ID))")

    # Compile the branch pattern if provided
    if br_pattern:
        if isinstance(br_pattern, str):
            br_pattern = re.compile(br_pattern)
        pattern = re.compile(f"({base_pattern.pattern})|({br_pattern.pattern})")
    else:
        pattern = base_pattern

    # Get and filter column names
    cols = [
        str(c).split(".")[1] if "." in c else str(c)
        for c in df.GetColumnNames()
        if pattern.match(str(c))
    ]

    # Add additional columns
    cols += ["runNumber", "eventNumber", "Polarity", "dimuon_q2", "GpsTime"]
    if branches:
        cols += branches

    arr = ak.to_dataframe(ak.from_rdataframe(df, set(cols)))

    arr["candNumber"] = arr.groupby(["runNumber", "eventNumber"]).cumcount() + 1

    report.Print()
    if cutreport:
        write_cuts_report(report, cutreport)

    return arr


def filter_dataframe(
    df,
    decay,
    x_cand=None,
    xwindow=None,
    ismc=True,
    gammapid=True,
    pidcorr=False,
    pidgen=False,
):
    """
    Filter the DataFrame by applying cuts specific to the decay channel.

    Parameters:
    - df (pd.DataFrame): Input DataFrame.
    - decay (str): Type of decay (e.g., 'omegamumu').
    - xwindow (string): Name of the x0 mass window (eta
    """
    decay_veto_cuts = {
        "omegamumu": "mumuphi_veto & mumukst_veto",
        "kstplusmumu": "mumukplus_veto & mumupiplus_veto",
        "rhoplusmumu": "mumukplus_veto & mumupiplus_veto",
    }

    df.query(decay_veto_cuts[decay], inplace=True)
    logging.info(f"Entries after vetoes: {df.shape[0]}")

    if ismc:
        if gammapid:
            df.query("gammapid_sel", inplace=True)
            logging.info(f"Entries after gamma pid sel: {df.shape[0]}")
        assert pidcorr == pidgen == False, "Cannot apply both pidcorr and pidgen"
        if pidcorr:
            df.query("pidcorr_sel", inplace=True)
            logging.info(f"Entries after pidsel: {df.shape[0]}")
        if pidgen:
            df.query("pidgen_sel", inplace=True)
            logging.info(f"Entries after pidsel: {df.shape[0]}")

    # x0 mass window
    if xwindow is not None:
        xwindow = xcand_window(x_cand, xwindow, op="&")
        df.query(xwindow, inplace=True)
        logging.info(f"Entries after xwindow cut: {df.shape[0]}")

    return df
