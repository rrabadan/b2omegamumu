# Locations in the reposisory
import os


class Locations:

    project_dir = os.path.dirname(os.path.dirname(__file__))
    cond_dir = os.path.join(project_dir, "conditions")
    artifacts_dir = os.path.join(project_dir, "artifacts")

    @classmethod
    def get_project_dir(cls):
        return cls.project_dir

    @classmethod
    def get_artifacts_dir(cls):
        return cls.artifacts_dir

    @classmethod
    def set_artifacts_dir(cls, artifacts_dir):
        cls.artifacts_dir = artifacts_dir

    @classmethod
    def get_conditions_dir(cls):
        return cls.cond_dir

    @classmethod
    def get_conditions_corrections_dir(cls):
        return os.path.join(cls.cond_dir, "corrections")

    @classmethod
    def get_conditions_weights_dir(cls):
        return os.path.join(cls.cond_dir, "weights")

    @classmethod
    def get_conditions_fitws_dir(cls):
        return os.path.join(cls.cond_dir, "roofit-ws")

    @classmethod
    def get_plots_dir(cls):
        return os.path.join(cls.artifacts_dir, "plots")

    @classmethod
    def get_tables_dir(cls):
        return os.path.join(cls.artifacts_dir, "tables")

    @classmethod
    def get_logs_dir(cls):
        return os.path.join(cls.artifacts_dir, "logs")

    @classmethod
    def get_tex_dir(cls):
        return os.path.join(cls.artifacts_dir, "tex")

    @classmethod
    def get_df_dir(cls):
        return os.path.join(cls.artifacts_dir, "dataframes")


def mkdir_artifacts():

    os.makedirs(Locations.artifacts_dir, exist_ok=True)
    for d in [
        Locations.get_artifacts_dir(),
        Locations.get_df_dir(),
        Locations.get_plots_dir(),
        Locations.get_tables_dir(),
        Locations.get_logs_dir(),
        Locations.get_tex_dir(),
    ]:
        if not os.path.exists(d):
            os.makedirs(d)
            print(f"Created directory {d}")


def remove_artifacts():
    if os.path.exists(Locations.get_artifacts_dir()):
        os.rmdir(Locations.get_artifacts_dir())
        print(f"Removed artifacts directory")


if __name__ == "__main__":
    Locations.create_dirs()
