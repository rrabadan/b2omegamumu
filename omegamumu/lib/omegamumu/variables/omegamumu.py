import numpy as np

from . import Var

Dimuon_M = Var(
    name="Dimuon_M",
    branch="dimuon_M",
    label=r"$m(\mu^{+}\mu^{-})$ $\mathregular{[MeV/c^{2}]}$",
    x_title=r"$m(\mu^{+}\mu^{-})$ $\mathregular{[MeV/c^{2}]}$",
    binning=(200, 0, 6000),
)

Jpis_M = Var(
    name="Jpsi_M",
    branch="dimuon_M",
    label=r"$m(\mu^{+}\mu^{-})$ $\mathregular{[MeV/c^{2}]}$",
    x_title=r"$m(\mu^{+}\mu^{-})$ $\mathregular{[MeV/c^{2}]}$",
    binning=(50, 2947, 3247),
)

Pizero_M = Var(
    name="Pizero_M",
    branch="pi0_M",
    label=r"$m(\gamma\gamma)$ $\mathregular{[MeV/c^{2}]}$",
    x_title=r"$m(\gamma\gamma)$ $\mathregular{[MeV/c^{2}]}$",
    binning=(50, 110, 160),
)

pipipiz_M = Var(
    name="pipipiz_M",
    branch="omega_M",
    label=r"$m(\pi^{+}\pi^{-}\pi^{0})$ $\mathregular{[MeV/c^{2}]}$",
    x_title=r"$m(\pi^{+}\pi^{-}\pi^{0})$ $\mathregular{[MeV/c^{2}]}$",
    binning=(100, 400, 1100),
)

eta_M = Var(
    name="eta_M",
    branch="omega_M",
    label=r"$m(\pi^{+}\pi^{-}\pi^{0})$ $\mathregular{[MeV/c^{2}]}$",
    x_title=r"$m(\pi^{+}\pi^{-}\pi^{0})$ $\mathregular{[MeV/c^{2}]}$",
    binning=(50, 500, 600),
)

omega_M = Var(
    name="omega_M",
    branch="omega_M",
    label=r"$m(\pi^{+}\pi^{-}\pi^{0})$ $\mathregular{[MeV/c^{2}]}$",
    x_title=r"$m(\pi^{+}\pi^{-}\pi^{0})$ $\mathregular{[MeV/c^{2}]}$",
    binning=(50, 730, 830),
)

phi_M = Var(
    name="phi_M",
    branch="omega_M",
    label=r"$m(\pi^{+}\pi^{-}\pi^{0})$ $\mathregular{[MeV/c^{2}]}$",
    x_title=r"$m(\pi^{+}\pi^{-}\pi^{0})$ $\mathregular{[MeV/c^{2}]}$",
    binning=(50, 970, 1070),
)

bmass = Var(
    name="bmass",
    branch="B_M",
    label=r"$m(\mu^{+}\mu^{-}\pi^{+}\pi^{-}$\gamma\gamma)$ $\mathregular{[MeV/c^{2}]}$",
    x_title=r"$m(\mu^{+}\mu^{-}\pi^{+}\pi^{-}$\gamma\gamma)$ $\mathregular{[MeV/c^{2}]}$",
    binning=(110, 4900, 6000),
)

bmass_piz = Var(
    name="bmass_piz",
    branch="B_piz_DTF_M",
    label=r"$m(\mu^{+}\mu^{-}\pi^{+}\pi^{-}\pi^{0})$ $\mathregular{[MeV/c^{2}]}$",
    x_title=r"$m(\mu^{+}\mu^{-}\pi^{+}\pi^{-}\pi^{0})$ $\mathregular{[MeV/c^{2}]}$",
    binning=(110, 4900, 6000),
)

bmass_jpsi = Var(
    name="bmass_jpsi",
    branch="B_Jpsi_DTF_M",
    label=r"$m(J/\psi\pi^{+}\pi^{-}\pi^{0})$ $\mathregular{[MeV/c^{2}]}$",
    x_title=r"$m(J/\psi\pi^{+}\pi^{-}\pi^{0})$ $\mathregular{[MeV/c^{2}]}$",
    binning=(110, 4900, 6000),
)

bmass_psi = Var(
    name="bmass_psi",
    branch="B_Psi2S_DTF_M",
    label=r"$m(\psi(2S)\pi^{+}\pi^{-}\pi^{0})$ $\mathregular{[MeV/c^{2}]}$",
    x_title=r"$m(\psi(2S)\pi^{+}\pi^{-}\pi^{0})$ $\mathregular{[MeV/c^{2}]}$",
    binning=(110, 4900, 6000),
)

masses = [
    Dimuon_M,
    Jpis_M,
    Pizero_M,
    pipipiz_M,
    eta_M,
    omega_M,
    phi_M,
    bmass,
    bmass_piz,
    bmass_jpsi,
    #    B_Psi2sDTF_M,
]

kintop = [
    Var(
        name="logBPT",
        branch="B_PT",
        label=r"$log(p_{T}(B))$",
        x_title=r"$log(p_{T}(B))$",
        binning=(30, 8, 11),
        expression=lambda x: np.log(x),
    ),
    Var(
        name="logBIPCHI2",
        branch="B_IPCHI2_OWNPV",
        label=r"$log(\chi^2(B))$",
        x_title=r"$log(\chi^2(B))$",
        binning=(30, -5.5, 2.5),
        expression=lambda x: np.log(x),
    ),
    Var(
        name="logBDIRA",
        branch="B_DIRA_OWNPV",
        label=r"$log(1-DIRA(B))$",
        x_title=r"$log(1-DIRA(B))$",
        binning=(30, -20, -10),
        expression=lambda x: np.log(1 - x),
    ),
    Var(
        name="logBCHI2NDF",
        branch=["B_ENDVERTEX_CHI2", "B_ENDVERTEX_NDOF"],
        label=r"$log(\chi^2_{vtx}(B))$",
        x_title=r"$log(\chi^2_{vtx}(B))$",
        binning=(30, -5.5, 2.5),
        expression=lambda x, y: np.log(x / y),
    ),
    Var(
        name="logBFDCHI2",
        branch="B_FDCHI2_OWNPV",
        label=r"$log(\chi^2_{FD}(B))$",
        x_title=r"$log(\chi^2_{FD}(B))$",
        binning=(30, 4, 10),
        expression=lambda x: np.log(x),
    ),
    Var(
        name="logJpsiPT",
        branch="dimuon_PT",
        label=r"$log(p_{T}(\mu^{+}\mu^{-}))$",
        x_title=r"$log(p_{T}(\mu^{+}\mu^{-}))$",
        binning=(30, 5, 12),
        expression=lambda x: np.log(x),
    ),
    Var(
        name="logJpsiIPCHI2",
        branch="dimuon_IPCHI2_OWNPV",
        label=r"$log(\chi^2_{IP}(\mu^{+}\mu^{-}))$",
        x_title=r"$log(\chi^2_{IP}(\mu^{+}\mu^{-}))$",
        binning=(30, -1, 10),
        expression=lambda x: np.log(x),
    ),
    Var(
        name="logJpsiCHI2NDF",
        branch=["dimuon_ENDVERTEX_CHI2", "dimuon_ENDVERTEX_NDOF"],
        label=r"$log(\chi^2_{vtx}(\mu^{+}\mu^{-}))$",
        x_title=r"$log(\chi^2_{vtx}(\mu^{+}\mu^{-}))$",
        binning=(30, -10, 2.5),
        expression=lambda x, y: np.log(x / y),
    ),
    Var(
        name="logX0PT",
        branch="omega_PT",
        label=r"$log(p_{T}(X^{0}))$",
        x_title=r"$log(p_{T}(X^{0}))$",
        binning=(30, 6.5, 10.5),
        expression=lambda x: np.log(x),
    ),
    Var(
        name="logPi0PT",
        branch="pi0_PT",
        label=r"$log(p_{T}(\pi^{0}))$",
        x_title=r"$log(p_{T}(\pi^{0}))$",
        binning=(30, 6.5, 9.5),
        expression=lambda x: np.log(x),
    ),
    Var(
        name="logGamMinPT",
        branch=["gamma1_PT", "gamma2_PT"],
        label=r"$log(min\;p_{T}(\gamma_{1},\gamma_{2}))$",
        x_title=r"$log(min\;p_{T}(\gamma_{1},\gamma_{2}))$",
        binning=(30, 5, 8),
        expression=lambda x, y: np.log(np.minimum(x, y)),
    ),
    Var(
        name="logGamMaxPT",
        branch=["gamma1_PT", "gamma2_PT"],
        label=r"$log(max\;p_{T}(\gamma_{1},\gamma_{2}))$",
        x_title=r"$log(max\;p_{T}(\gamma_{1},\gamma_{2}))$",
        binning=(30, 5, 8),
        expression=lambda x, y: np.log(np.maximum(x, y)),
    ),
    Var(
        name="logPiMinPT",
        branch=["piplus_PT", "piminus_PT"],
        label=r"$log(min\;p_{T}(\pi^{+},\pi^{-}))$",
        x_title=r"$log(min\;p_{T}(\pi^{+},\pi^{-}))$",
        binning=(30, 5, 10),
        expression=lambda x, y: np.log(np.minimum(x, y)),
    ),
    Var(
        name="logPiMaxPT",
        branch=["piplus_PT", "piminus_PT"],
        label=r"$log(max\;p_{T}(\pi^{+},\pi^{-}))$",
        x_title=r"$log(max\;p_{T}(\pi^{+},\pi^{-}))$",
        binning=(30, 5, 10),
        expression=lambda x, y: np.log(np.maximum(x, y)),
    ),
    Var(
        name="logPiMinIPCHI2",
        branch=["piplus_IPCHI2_OWNPV", "piminus_IPCHI2_OWNPV"],
        label=r"$log(min\;\chi^{2}_{IP}(\pi^{+},\pi^{-}))$",
        x_title=r"$log(min\;\chi^{2}_{IP}(\pi^{+},\pi^{-}))$",
        binning=(30, 1.5, 8.5),
        expression=lambda x, y: np.log(np.minimum(x, y)),
    ),
    Var(
        name="logPiMaxIPCHI2",
        branch=["piplus_IPCHI2_OWNPV", "piminus_IPCHI2_OWNPV"],
        label=r"$log(max\;\chi^{2}_{IP}(\pi^{+},\pi^{-}))$",
        x_title=r"$log(max\;\chi^{2}_{IP}(\pi^{+},\pi^{-}))$",
        binning=(30, 1.5, 8.5),
        expression=lambda x, y: np.log(np.maximum(x, y)),
    ),
    Var(
        name="logMuMinPT",
        branch=["muplus_PT", "muminus_PT"],
        label=r"$log(min\; p_{T}(\mu^{+},\mu^{-}))$",
        x_title=r"$log(min\; p_{T}(\mu^{+},\mu^{-}))$",
        binning=(30, 5.5, 10.5),
        expression=lambda x, y: np.log(np.minimum(x, y)),
    ),
    Var(
        name="logMuMaxPT",
        branch=["muplus_PT", "muminus_PT"],
        label=r"$log(max\; p_{T}(\mu^{+},\mu^{-}))$",
        x_title=r"$log(max\; p_{T}(\mu^{+},\mu^{-}))$",
        binning=(30, 5.5, 10.5),
        expression=lambda x, y: np.log(np.maximum(x, y)),
    ),
    Var(
        name="logMuMinIPCHI2",
        branch=["muplus_IPCHI2_OWNPV", "muminus_IPCHI2_OWNPV"],
        label=r"$log(min\; \chi^{2}_{IP}(\mu^{+},\mu^{-}))$",
        x_title=r"$log(min\; \chi^{2}_{IP}(\mu^{+},\mu^{-}))$",
        binning=(30, 1.5, 10.5),
        expression=lambda x, y: np.log(np.minimum(x, y)),
    ),
    Var(
        name="logMuMaxIPCHI2",
        branch=["muplus_IPCHI2_OWNPV", "muminus_IPCHI2_OWNPV"],
        label=r"$log(max\; \chi^{2}_{IP}(\mu^{+},\mu^{-}))$",
        x_title=r"$log(max\; \chi^{2}_{IP}(\mu^{+},\mu^{-}))$",
        binning=(30, 1.5, 10.5),
        expression=lambda x, y: np.log(np.maximum(x, y)),
    ),
]

isolation = [
    Var(
        name="bCONEMULT",
        branch="B_CONEMULT",
        label="B CONEMULT",
        x_title="B CONEMULT",
        binning=(30, 0, 30),
        expression=lambda x: x,
    ),
    Var(
        name="bCONEPTASYM",
        branch="B_CONEPTASYM",
        label="B CONEPTASYM",
        x_title="B CONEPTASYM",
        binning=(30, -1, 1.5),
        expression=lambda x: x,
    ),
    Var(
        name="logBVTXISODCHI2ONETRACK",
        branch="B_VTXISODCHI2ONETRACK",
        label="log(B_VTXISODCHI2ONETRACK)",
        x_title="log(B_VTXISODCHI2ONETRACK)",
        binning=(30, -10, 15),
        expression=lambda x: np.log(x),
    ),
    Var(
        name="logBVTXISODCHI2TWOTRACK",
        branch="B_VTXISODCHI2TWOTRACK",
        label="log(B_VTXISODCHI2TWOTRACK)",
        x_title="log(B_VTXISODCHI2TWOTRACK)",
        binning=(30, -5, 15),
        expression=lambda x: np.log(x),
    ),
]

pids = [
    Var(
        name="pionMinProbNNpi",
        branch=["piplus_ProbNNpi", "piminus_ProbNNpi"],
        label=r"$min\; ProbNNpi(\pi^{+}, \pi^{-})$",
        x_title=r"$min\; ProbNNpi(\pi^{+}, \pi^{-})$",
        binning=(30, 0, 1),
        expression=lambda x, y: 1.0 - (1.0 - np.minimum(x, y) ** 0.2) ** 0.2,
    ),
    Var(
        name="pionMaxProbNNpi",
        branch=["piplus_ProbNNpi", "piminus_ProbNNpi"],
        label=r"$max\; ProbNNpi(\pi^{+}, \pi^{-})$",
        x_title=r"$max\; ProbNNpi(\pi^{+}, \pi^{-})$",
        binning=(30, 0, 1),
        expression=lambda x, y: 1.0 - (1.0 - np.maximum(x, y) ** 0.2) ** 0.2,
    ),
    Var(
        name="muonMinProbNNmu",
        branch=["muplus_ProbNNmu", "muminus_ProbNNmu"],
        label=r"$min\; ProbNNmu(\mu^{+}, \mu^{-})$",
        x_title=r"$min\; ProbNNmu(\mu^{+}, \mu^{-})$",
        binning=(30, 0, 1),
        expression=lambda x, y: 1.0 - (1.0 - np.minimum(x, y) ** 0.2) ** 0.2,
    ),
    Var(
        name="muonMaxProbNNmu",
        branch=["muplus_ProbNNmu", "muminus_ProbNNmu"],
        label=r"$max\; ProbNNmu(\mu^{+}, \mu^{-})$",
        x_title=r"$max\; ProbNNmu(\mu^{+}, \mu^{-})$",
        binning=(30, 0, 1),
        expression=lambda x, y: 1.0 - (1.0 - np.maximum(x, y) ** 0.2) ** 0.2,
    ),
]

pidscorr = [
    Var(
        name="pionMinProbNNpi",
        branch=["piplus_ProbNNpi_pidcorr_default", "piminus_ProbNNpi_pidcorr_default"],
        label=r"$min\; ProbNNpi(\pi^{+}, \pi^{-})$",
        x_title=r"$min\; ProbNNpi(\pi^{+}, \pi^{-})$",
        binning=(30, 0, 1),
        expression=lambda x, y: 1.0 - (1.0 - np.minimum(x, y) ** 0.2) ** 0.2,
    ),
    Var(
        name="pionMaxProbNNpi",
        branch=["piplus_ProbNNpi_pidcorr_default", "piminus_ProbNNpi_pidcorr_default"],
        label=r"$max\; ProbNNpi(\pi^{+}, \pi^{-})$",
        x_title=r"$max\; ProbNNpi(\pi^{+}, \pi^{-})$",
        binning=(30, 0, 1),
        expression=lambda x, y: 1.0 - (1.0 - np.maximum(x, y) ** 0.2) ** 0.2,
    ),
    Var(
        name="muonMinProbNNmu",
        branch=["muplus_ProbNNmu_pidcorr_default", "muminus_ProbNNmu_pidcorr_default"],
        label=r"$min\; ProbNNmu(\mu^{+}, \mu^{-})$",
        x_title=r"$min\; ProbNNmu(\mu^{+}, \mu^{-})$",
        binning=(30, 0, 1),
        expression=lambda x, y: 1.0 - (1.0 - np.minimum(x, y) ** 0.2) ** 0.2,
    ),
    Var(
        name="muonMaxProbNNmu",
        branch=["muplus_ProbNNmu_pidcorr_default", "muminus_ProbNNmu_pidcorr_default"],
        label=r"$max\; ProbNNmu(\mu^{+}, \mu^{-})$",
        x_title=r"$max\; ProbNNmu(\mu^{+}, \mu^{-})$",
        binning=(30, 0, 1),
        expression=lambda x, y: 1.0 - (1.0 - np.maximum(x, y) ** 0.2) ** 0.2,
    ),
]


pidsgen = [
    Var(
        name="pionMinProbNNpi",
        branch=["piplus_ProbNNpi_pidgen_default", "piminus_ProbNNpi_pidgen_default"],
        label=r"$min\; ProbNNpi(\pi^{+}, \pi^{-})$",
        x_title=r"$min\; ProbNNpi(\pi^{+}, \pi^{-})$",
        binning=(30, 0, 1),
        expression=lambda x, y: 1.0 - (1.0 - np.minimum(x, y) ** 0.2) ** 0.2,
    ),
    Var(
        name="pionMaxProbNNpi",
        branch=["piplus_ProbNNpi_pidgen_default", "piminus_ProbNNpi_pidgen_default"],
        label=r"$max\; ProbNNpi(\pi^{+}, \pi^{-})$",
        x_title=r"$max\; ProbNNpi(\pi^{+}, \pi^{-})$",
        binning=(30, 0, 1),
        expression=lambda x, y: 1.0 - (1.0 - np.maximum(x, y) ** 0.2) ** 0.2,
    ),
    Var(
        name="muonMinProbNNmu",
        branch=["muplus_ProbNNmu_pidgen_default", "muminus_ProbNNmu_pidgen_default"],
        label=r"$min\; ProbNNmu(\mu^{+}, \mu^{-})$",
        x_title=r"$min\; ProbNNmu(\mu^{+}, \mu^{-})$",
        binning=(30, 0, 1),
        expression=lambda x, y: 1.0 - (1.0 - np.minimum(x, y) ** 0.2) ** 0.2,
    ),
    Var(
        name="muonMaxProbNNmu",
        branch=["muplus_ProbNNmu_pidgen_default", "muminus_ProbNNmu_pidgen_default"],
        label=r"$max\; ProbNNmu(\mu^{+}, \mu^{-})$",
        x_title=r"$max\; ProbNNmu(\mu^{+}, \mu^{-})$",
        binning=(30, 0, 1),
        expression=lambda x, y: 1.0 - (1.0 - np.maximum(x, y) ** 0.2) ** 0.2,
    ),
]
