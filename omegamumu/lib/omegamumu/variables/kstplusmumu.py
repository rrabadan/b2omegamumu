import numpy as np

from . import Var

Dimuon_M = Var(
    name="Dimuon_M",
    branch="dimuon_M",
    label=r"$m(\mu^{+}\mu^{-})$ $\mathregular{[MeV/c^{2}]}$",
    x_title=r"$m(\mu^{+}\mu^{-})$ $\mathregular{[MeV/c^{2}]}$",
    binning=(200, 0, 6000),
    expression=lambda x: x,
)

Jpsi_M = Var(
    name="Jpsi_M",
    branch="dimuon_M",
    label=r"$m(\mu^{+}\mu^{-})$ $\mathregular{[MeV/c^{2}]}$",
    x_title=r"$m(\mu^{+}\mu^{-})$ $\mathregular{[MeV/c^{2}]}$",
    binning=(50, 2947, 3247),
)

Pizero_M = Var(
    name="Pizero_M",
    branch="pi0_M",
    label=r"$m(\gamma\gamma)$ $\mathregular{[MeV/c^{2}]}$",
    x_title=r"$m(\gamma\gamma)$ $\mathregular{[MeV/c^{2}]}$",
    binning=(50, 110, 160),
)

Kstplus_M = Var(
    name="Kstplus_M",
    branch="kstplus_M",
    label=r"$m(K^{+}\pi^{0})$ $\mathregular{[MeV/c^{2}]}$",
    x_title=r"$m(K^{+}\pi^{0})$ $\mathregular{[MeV/c^{2}]}$",
    binning=(50, 790, 990),
)

B_M = Var(
    name="B_M",
    branch="Bplus_M",
    label=r"$m(\mu^{+}\mu^{-}\;K^{+}\gamma\gamma$ $\mathregular{[MeV/c^{2}]}$",
    x_title=r"$m(\mu^{+}\mu^{-}\;K^{+}\gamma\gamma$ $\mathregular{[MeV/c^{2}]}$",
    binning=(110, 4900, 6000),
)

B_pizDTF_M = Var(
    name="B_pizDTF_M",
    branch="Bplus_piz_DTF_M",
    label=r"$m(\mu^{+}\mu^{-}\;K^{+}\pi^{0})$ $\mathregular{[MeV/c^{2}]}$",
    x_title=r"$m(\mu^{+}\mu^{-}\;K^{+}\pi^{0})$ $\mathregular{[MeV/c^{2}]}$",
    binning=(110, 4900, 6000),
)

B_JpsiDTF_M = Var(
    name="B_JpsiDTF_M",
    branch="Bplus_Jpsi_DTF_M",
    label=r"$m(J/\psi\;K^{+}\pi^{0})$ $\mathregular{[MeV/c^{2}]}$",
    x_title=r"$m(J/\psi\;K^{+}\pi^{0})$ $\mathregular{[MeV/c^{2}]}$",
    binning=(110, 4900, 6000),
)

B_Psi2sDTF_M = Var(
    name="B_Psi2sDTF_M",
    branch="Bplus_Psi2S_DTF_M",
    label=r"$m(\psi(2S)\;K^{+}\pi^{0})$ $\mathregular{[MeV/c^{2}]}$",
    x_title=r"$m(\psi(2S)\;K^{+}\pi^{0})$ $\mathregular{[MeV/c^{2}]}$",
    binning=(110, 4900, 6000),
)

masses = [
    Dimuon_M,
    Jpsi_M,
    Pizero_M,
    Kstplus_M,
    B_M,
    B_pizDTF_M,
    B_JpsiDTF_M,
    # B_Psi2sDTF_M,
]

kintop = [
    Var(
        name="logBPT",
        branch="Bplus_PT",
        label=r"$log(p_{T}(B))$",
        x_title=r"$log(p_{T}(B))$",
        binning=(50, 7, 11),
        expression=lambda x: np.log(x),
    ),
    Var(
        name="logBIPCHI2",
        branch="Bplus_IPCHI2_OWNPV",
        label=r"$log(\chi^2(B))$",
        x_title=r"$log(\chi^2(B))$",
        binning=(30, -5.5, 2.5),
        expression=lambda x: np.log(x),
    ),
    Var(
        name="logBDIRA",
        branch="Bplus_DIRA_OWNPV",
        label=r"$log(1-DIRA(B))$",
        x_title=r"$log(1-DIRA(B))$",
        binning=(30, -20, -10),
        expression=lambda x: np.log(1 - x),
    ),
    Var(
        name="logBCHI2NDF",
        branch=["Bplus_ENDVERTEX_CHI2", "Bplus_ENDVERTEX_NDOF"],
        label=r"$log(\chi^2_{vtx}(B))$",
        x_title=r"$log(\chi^2_{vtx}(B))$",
        binning=(30, -5.5, 2.5),
        expression=lambda x, y: np.log(x / y),
    ),
    Var(
        name="logBFDCHI2",
        branch="Bplus_FDCHI2_OWNPV",
        label=r"$log(\chi^2_{FD}(B))$",
        x_title=r"$log(\chi^2_{FD}(B))$",
        binning=(30, 4, 10),
        expression=lambda x: np.log(x),
    ),
    Var(
        name="logJpsiPT",
        branch="dimuon_PT",
        label=r"$log(p_{T}(\mu^{+}\mu^{-}))$",
        x_title=r"$log(p_{T}(\mu^{+}\mu^{-}))$",
        binning=(30, 5, 12),
        expression=lambda x: np.log(x),
    ),
    Var(
        name="logJpsiIPCHI2",
        branch="dimuon_IPCHI2_OWNPV",
        label=r"$log(\chi^2_{IP}(\mu^{+}\mu^{-}))$",
        x_title=r"$log(\chi^2_{IP}(\mu^{+}\mu^{-}))$",
        binning=(30, -1, 10),
        expression=lambda x: np.log(x),
    ),
    Var(
        name="logJpsiCHI2NDF",
        branch=["dimuon_ENDVERTEX_CHI2", "dimuon_ENDVERTEX_NDOF"],
        label=r"$log(\chi^2_{vtx}(\mu^{+}\mu^{-}))$",
        x_title=r"$log(\chi^2_{vtx}(\mu^{+}\mu^{-}))$",
        binning=(30, -10, 2.5),
        expression=lambda x, y: np.log(x / y),
    ),
    Var(
        name="logX0PT",
        branch="kstplus_PT",
        label=r"$log(p_{T}(X^{0}))$",
        x_title=r"$log(p_{T}(X^{0}))$",
        binning=(30, 6.5, 10.5),
        expression=lambda x: np.log(x),
    ),
    Var(
        name="logPi0PT",
        branch="pi0_PT",
        label=r"$log(p_{T}(\pi^{0}))$",
        x_title=r"$log(p_{T}(\pi^{0}))$",
        binning=(30, 6.5, 9.5),
        expression=lambda x: np.log(x),
    ),
    Var(
        name="logGamMinPT",
        branch=["gamma1_PT", "gamma2_PT"],
        label=r"$log(min\;p_{T}(\gamma_{1},\gamma_{2}))$",
        x_title=r"$log(min\;p_{T}(\gamma_{1},\gamma_{2}))$",
        binning=(30, 5, 8),
        expression=lambda x, y: np.log(np.minimum(x, y)),
    ),
    Var(
        name="logGamMaxPT",
        branch=["gamma1_PT", "gamma2_PT"],
        label=r"$log(max\;p_{T}(\gamma_{1},\gamma_{2}))$",
        x_title=r"$log(max\;p_{T}(\gamma_{1},\gamma_{2}))$",
        binning=(30, 5, 8),
        expression=lambda x, y: np.log(np.maximum(x, y)),
    ),
    Var(
        name="logKPT",
        branch="kplus_PT",
        label=r"$log(\;p_{T}(K^{+}))$",
        x_title=r"$log(\;p_{T}(K^{+}))$",
        binning=(30, 5, 10),
        expression=lambda x: np.log(x),
    ),
    Var(
        name="logKIPCHI2",
        branch="kplus_IPCHI2_OWNPV",
        label=r"$log(\chi^{2}_{IP}(K^{+}))$",
        x_title=r"$log(\chi^{2}_{IP}(K^{+}))$",
        binning=(30, 1.5, 8.5),
        expression=lambda x: np.log(x),
    ),
    Var(
        name="logMuMinPT",
        branch=["muplus_PT", "muminus_PT"],
        label=r"$log(min\; p_{T}(\mu^{+},\mu^{-}))$",
        x_title=r"$log(min\; p_{T}(\mu^{+},\mu^{-}))$",
        binning=(30, 5.5, 10.5),
        expression=lambda x, y: np.log(np.minimum(x, y)),
    ),
    Var(
        name="logMuMaxPT",
        branch=["muplus_PT", "muminus_PT"],
        label=r"$log(max\; p_{T}(\mu^{+},\mu^{-}))$",
        x_title=r"$log(max\; p_{T}(\mu^{+},\mu^{-}))$",
        binning=(30, 5.5, 10.5),
        expression=lambda x, y: np.log(np.maximum(x, y)),
    ),
    Var(
        name="logMuMinIPCHI2",
        branch=["muplus_IPCHI2_OWNPV", "muminus_IPCHI2_OWNPV"],
        label=r"$log(min\; \chi^{2}_{IP}(\mu^{+},\mu^{-}))$",
        x_title=r"$log(min\; \chi^{2}_{IP}(\mu^{+},\mu^{-}))$",
        binning=(30, 1.5, 10.5),
        expression=lambda x, y: np.log(np.minimum(x, y)),
    ),
    Var(
        name="logMuMaxIPCHI2",
        branch=["muplus_IPCHI2_OWNPV", "muminus_IPCHI2_OWNPV"],
        label=r"$log(max\; \chi^{2}_{IP}(\mu^{+},\mu^{-}))$",
        x_title=r"$log(max\; \chi^{2}_{IP}(\mu^{+},\mu^{-}))$",
        binning=(30, 1.5, 10.5),
        expression=lambda x, y: np.log(np.maximum(x, y)),
    ),
]

isolation = [
    Var(
        name="bCONEMULT",
        branch="Bplus_CONEMULT",
        label="B CONEMULT",
        x_title="B CONEMULT",
        binning=(30, 0, 30),
        expression=lambda x: x,
    ),
    Var(
        name="bCONEPTASYM",
        branch="Bplus_CONEPTASYM",
        label="B CONEPTASYM",
        x_title="B CONEPTASYM",
        binning=(30, -1, 1.5),
        expression=lambda x: x,
    ),
    Var(
        name="logBVTXISODCHI2ONETRACK",
        branch="Bplus_VTXISODCHI2ONETRACK",
        label="log(B_VTXISODCHI2ONETRACK)",
        x_title="log(B_VTXISODCHI2ONETRACK)",
        binning=(30, -10, 15),
        expression=lambda x: np.log(x),
    ),
    Var(
        name="logBVTXISODCHI2TWOTRACK",
        branch="Bplus_VTXISODCHI2TWOTRACK",
        label="log(B_VTXISODCHI2TWOTRACK)",
        x_title="log(B_VTXISODCHI2TWOTRACK)",
        binning=(30, -5, 15),
        expression=lambda x: np.log(x),
    ),
]

pids = [
    Var(
        name="kaonProbNNpi",
        branch="kplus_ProbNNpi",
        label=r"$ProbNNpi(K^{+})$",
        x_title=r"$ProbNNpi(K^{+})$",
        binning=(30, 0, 0.6),
        expression=lambda x: 1.0 - (1.0 - x**0.2) ** 0.2,
    ),
    Var(
        name="kaonProbNNK",
        branch="kplus_ProbNNk",
        label=r"$ProbNNk(K^{+})$",
        x_title=r"$ProbNNk(K^{+})$",
        binning=(30, 0.4, 1),
        expression=lambda x: 1.0 - (1.0 - x**0.2) ** 0.2,
    ),
    Var(
        name="muonPlusProbNN",
        branch="muplus_ProbNNmu",
        label=r"$ProbNNmu(\mu^{+})$",
        x_title=r"$ProbNNmu(\mu^{+})$",
        binning=(30, 0.1, 1),
        expression=lambda x: 1.0 - (1.0 - x**0.2) ** 0.2,
    ),
    Var(
        name="muonMinusProbNN",
        branch="muminus_ProbNNmu",
        label=r"$ProbNNmu(\mu^{-})$",
        x_title=r"$ProbNNmu(\mu^{-})$",
        binning=(30, 0.1, 1),
        expression=lambda x: 1.0 - (1.0 - x**0.2) ** 0.2,
    ),
]

pidscorr = [
    Var(
        name="kaonProbNNpi",
        branch="kplus_ProbNNpi_corr",
        label=r"$ProbNNpi(K^{+}$",
        x_title=r"$ProbNNpi(K^{+}$",
        binning=(30, 0, 0.6),
        expression=lambda x: 1.0 - (1.0 - x**0.2) ** 0.2,
    ),
    Var(
        name="kaonProbNNK",
        branch="kplus_ProbNNK_corr",
        label=r"$ProbNNk(K^{+})$",
        x_title=r"$ProbNNk(K^{+})$",
        binning=(30, 0.4, 1),
        expression=lambda x: 1.0 - (1.0 - x**0.2) ** 0.2,
    ),
    Var(
        name="muonPlusProbNN",
        branch="muplus_ProbNNmu_corr",
        label=r"$ProbNNmu(\mu^{+})$",
        x_title=r"$ProbNNmu(\mu^{+})$",
        binning=(30, 0.1, 1),
        expression=lambda x: 1.0 - (1.0 - x**0.2) ** 0.2,
    ),
    Var(
        name="muonMinusProbNN",
        branch="muminus_ProbNNmu_corr",
        label=r"$ProbNNmu(\mu^{-})$",
        x_title=r"$ProbNNmu(\mu^{-})$",
        binning=(30, 0.1, 1),
        expression=lambda x: 1.0 - (1.0 - x**0.2) ** 0.2,
    ),
]

pidsgen = [
    Var(
        name="kaonProbNNpi",
        branch="kplus_ProbNNpi_pidgen_default",
        label=r"$ProbNNpi(K^{+}$",
        x_title=r"$ProbNNpi(K^{+}$",
        binning=(30, 0, 0.6),
        expression=lambda x: 1.0 - (1.0 - x**0.2) ** 0.2,
    ),
    Var(
        name="kaonProbNNK",
        branch="kplus_ProbNNK_pidgen_default",
        label=r"$ProbNNk(K^{+})$",
        x_title=r"$ProbNNk(K^{+})$",
        binning=(30, 0.4, 1),
        expression=lambda x: 1.0 - (1.0 - x**0.2) ** 0.2,
    ),
    Var(
        name="muonPlusProbNN",
        branch="muplus_ProbNNmu_pidgen_default",
        label=r"$ProbNNmu(\mu^{+})$",
        x_title=r"$ProbNNmu(\mu^{+})$",
        binning=(30, 0.1, 1),
        expression=lambda x: 1.0 - (1.0 - x**0.2) ** 0.2,
    ),
    Var(
        name="muonMinusProbNN",
        branch="muminus_ProbNNmu_pidgen_default",
        label=r"$ProbNNmu(\mu^{-})$",
        x_title=r"$ProbNNmu(\mu^{-})$",
        binning=(30, 0.1, 1),
        expression=lambda x: 1.0 - (1.0 - x**0.2) ** 0.2,
    ),
]
