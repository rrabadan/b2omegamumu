from math import gamma
import numpy as np
import order as od

from ..common.utils import is_collection


# Var = namedtuple("Var", ["name", "branch", "label", "range", "bins", "transform"])
# Extend od.Variable to include label and branches
class Var(od.Variable):
    def __init__(self, name, branch, label, *args, **kwargs):
        super().__init__(name, *args, **kwargs)
        self.label = label
        self.branch = branch if is_collection(branch) else [branch]
        if kwargs.get("expression", None) is None:
            self.expression = lambda x: x


def get_var_input_branches(variables):
    branches = set()
    for var in variables:
        if is_collection(var.branch):
            branches.update(var.branch)
        else:
            branches.add(var.branch)
    return list(branches)


nTracks = Var(
    name="nTracks",
    branch="nTracks",
    label="nTracks",
    binning=(50, 0, 500),
    x_title="Number of tracks",
    expression=lambda x: x,
)

nSPDhits = Var(
    name="nSPDhits",
    branch=["gamma1_PP_CaloNeutralSpd", "gamma2_PP_CaloNeutralSpd"],
    label="nSPDhits",
    binning=(15, 0, 15),
    x_title="spd1 + spd2",
    expression=lambda x, y: x + y,
)

multiplicity = [
    nTracks,
    nSPDhits,
]

pi0CL = Var(
    name="pi0CL",
    branch="pi0_CL",
    label=r"$CL(\pi^{0})$",
    binning=(30, 0, 1),
    x_title=r"$CL(\pi^{0})$",
    expression=lambda x: x,
)

gamma1CL = Var(
    name="gamma1CL",
    branch="gamma1_CL",
    label=r"$CL(\gamma_{1})$",
    binning=(50, 0, 1),
    x_title=r"$CL(\gamma_{1})$",
    expression=lambda x: x,
)

gamma2CL = Var(
    name="gamma2CL",
    branch="gamma2_CL",
    label=r"$CL(\gamma_{2})$",
    binning=(30, 0, 1),
    x_title=r"$CL(\gamma_{2})$",
    expression=lambda x: x,
)

gammaMinIsNotH = Var(
    name="gammaMinIsNotH",
    branch=["gamma1_PP_IsNotH", "gamma2_PP_IsNotH"],
    label=r"$min\; IsNotH(\gamma_1, \gamma_{2})$",
    binning=(30, 0, 0.9),
    x_title=r"$min\; IsNotH(\gamma_1, \gamma_{2})$",
    expression=lambda x, y: 1.0 - (1.0 - np.minimum(x, y) ** 0.2) ** 0.2,
)

gammaMaxIsNotH = Var(
    name="gammaMaxIsNotH",
    branch=["gamma1_PP_IsNotH", "gamma2_PP_IsNotH"],
    label=r"$max\; IsNotH(\gamma_1, \gamma_{2})$",
    binning=(30, 0, 0.9),
    x_title=r"$max\; IsNotH(\gamma_1, \gamma_{2})$",
    expression=lambda x, y: 1.0 - (1.0 - np.maximum(x, y) ** 0.2) ** 0.2,
)

neutrals = [
    pi0CL,
    gamma1CL,
    gamma2CL,
    gammaMinIsNotH,
    gammaMaxIsNotH,
]
