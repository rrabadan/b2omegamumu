import pandas as pd

from ..common.kinematics import eta
from .utils import load_correction_histogram
from .correction import from_uproot_hist, from_hist, import_from_json
from .gbreweight import create_reweighter, save_reweighter, load_reweighter

from locations import Locations


def evaluate_corr_weights(df, decay, year, magpol):
    """
    Get correction weights for the given DataFrame, decay type, year, and magpol.

    Parameters:
    df (pd.DataFrame): The input DataFrame
    decay (str): The decay type ('omegamumu' o other)
    year (str): The year of the data
    magpol (str): The magnet polarity ('magup' or 'magdown')

    Returns:
    dict: A dictionary of correction weights
    """

    pol_dict = {"magup": ("MagUp", "Up", "Up"), "magdown": ("MagDown", "Down", "Dn")}

    b_cand = "B" if decay == "omegamumu" else "Bplus"
    corrmc_dict = {}

    # B pT correction
    kin_corr = load_correction_histogram(
        Locations.get_conditions_corrections_dir(),
        "kin-hist",
        f"datamc_B_PT_histo_mcjpsik_{year}_{pol_dict[magpol][0]}.root",
        f"data_mc_ratio_B_PT_{year}_{pol_dict[magpol][1]}",
        "B pT correction",
    )
    corrmc_dict["kin_weight"] = kin_corr.to_evaluator().evaluate(df[f"{b_cand}_PT"])

    # TisTos correction
    tistos_corr = load_correction_histogram(
        Locations.get_conditions_corrections_dir(),
        "tistos",
        f"B2mumupi_TisTosMg{pol_dict[magpol][2]}{year}.root",
        "efficiency_ratio",
        "TisTos correction",
    )
    corrmc_dict["tistos_weight"] = tistos_corr.to_evaluator().evaluate(
        df.muplus_PT, df.muminus_PT
    )

    # Tracking correction
    trk_corr = load_correction_histogram(
        Locations.get_conditions_corrections_dir(),
        "track",
        f"Ratio_P-ETA_{year}_25ns.root",
        "Ratio",
        "Tracking correction",
    )
    mup_trk_weight = trk_corr.to_evaluator().evaluate(
        df.muplus_P, eta(df.muplus_PX, df.muplus_PY, df.muplus_PZ)
    )
    mum_trk_weight = trk_corr.to_evaluator().evaluate(
        df.muminus_P, eta(df.muminus_PX, df.muminus_PY, df.muminus_PZ)
    )
    corrmc_dict["muplus_trk_weight"] = mup_trk_weight
    corrmc_dict["muminus_trk_weight"] = mum_trk_weight

    if decay == "omegamumu":
        pip_trk_weight = trk_corr.to_evaluator().evaluate(
            df.piplus_P, eta(df.piplus_PX, df.piplus_PY, df.piplus_PZ)
        )
        pim_trk_weight = trk_corr.to_evaluator().evaluate(
            df.piminus_P, eta(df.piminus_PX, df.piminus_PY, df.piminus_PZ)
        )
        corrmc_dict["trk_weight"] = (
            mup_trk_weight * mum_trk_weight * pip_trk_weight * pim_trk_weight
        )
        corrmc_dict["piplus_trk_weight"] = pip_trk_weight
        corrmc_dict["piminus_trk_weight"] = pim_trk_weight
    else:
        track = "kplus" if decay == "kstplusmumu" else "piplus"
        t_trk_weight = trk_corr.to_evaluator().evaluate(
            df[f"{track}_P"],
            eta(df[f"{track}_PX"], df[f"{track}_PY"], df[f"{track}_PZ"]),
        )
        corrmc_dict["trk_weight"] = mup_trk_weight * mum_trk_weight * t_trk_weight
        corrmc_dict[f"{track}_trk_weight"] = t_trk_weight

    return corrmc_dict


def gammapid_reweighter_inputs(df):
    """
    Prepare inputs for the gammapid reweighter.

    Parameters:
        df (pandas.DataFrame): The input DataFrame containing the following columns:
            - nTracks: Number of tracks
            - nSPDHits: Sum of SPD hits for gamma1 and gamma2
            - pimass: Pi0 mass

    Returns:
        pandas.DataFrame: A DataFrame containing the prepared inputs for the gammapid reweighter.
    """
    arr_dict = {
        "nTracks": df["nTracks"],
        "nSPDHits": df["gamma1_PP_CaloNeutralSpd"] + df["gamma2_PP_CaloNeutralSpd"],
        "pimass": df["pi0_M"],
        #        'gamMinPT': np.minimum(df['gamma1_PT'], df['gamma2_PT']),
        #        'gamMaxPT': np.maximum(df['gamma1_PT'], df['gamma2_PT']),
    }
    return pd.DataFrame.from_dict(arr_dict)


__all__ = [
    "load_correction_histogram",
    "compute_eta",
    "from_uproot_hist",
    "from_hist",
    "import_from_json",
    "create_reweighter",
    "save_reweighter",
    "load_reweighter",
    "evaluate_corr_weights",
    "gammapid_reweighter_inputs",
]
