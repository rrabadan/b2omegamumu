import os
import sys
import itertools
import string
import random
import argparse
import subprocess

from ROOT import RDataFrame


def pidcorr(inputfile, tree, outpath, simversion):

    dataset = os.path.basename(inputfile).split(".")[0]
    magpol, year, _ = dataset.split(
        "_",
    )
    magpol = magpol[0].upper() + magpol[1:3] + magpol[3].upper() + magpol[4:]
    dataset = "_".join([magpol, year])

    dirname = os.path.dirname(inputfile).split("/")

    decay = dirname[-3]
    evttype = dirname[-2]
    group = dirname[-1]
    # List of input ROOT files with MC ntuples. Format:
    #   (inputfile, outputfile, dataset)
    # Name of the input tree
    # Could also include ROOT directory, e.g. "Dir/Ntuple"

    # Postfixes of the Pt, Eta and Ntracks variables (ntuple variable name w/o branch name)
    # e.g. if the ntuple contains "pion_PT", it should be just "PT"
    ptvar = "PT"
    etavar = ""
    pvar = "P"  # Could use P variable instead of eta
    ntrvar = "nTracks"  # This should correspond to the number of "Best tracks", not "Long tracks"!
    # seed = None   # No initial seed
    seed = 1234  # Alternatively, could set initial random seed

    # Dictionary of tracks with their PID variables, in the form {branch name}:{pidvars}
    # For each track branch name, {pidvars} is a dictionary in the form {ntuple variable}:{pid config},
    #   where
    #     {ntuple variable} is the name of the corresponding ntuple PID variable without branch name,
    #   and
    #     {pid_config} is the string describing the PID configuration.
    # Run PIDCorr.py without arguments to get the full list of PID configs

    probnn_version = "MC15TuneV1_%s_Brunel"
    probnn2_version = "MC15TuneV1_%s_Brunel_Mod2"
    dll_version = "%s_Brunel"
    dllmu_version = "%s_IsMuon_Brunel"

    tracks = {
        "muplus": {
            "ProbNNmu": "mu_" + probnn_version % "ProbNNmu",
            "PIDmu": "mu_" + dllmu_version % "CombDLLmu",
        },
        "muminus": {
            "ProbNNmu": "mu_" + probnn_version % "ProbNNmu",
            "PIDmu": "mu_" + dllmu_version % "CombDLLmu",
        },
    }

    if decay == "omegamumu":
        tracks.update(
            {
                "piplus": {
                    "ProbNNk": "pi_" + probnn_version % "ProbNNK",
                    "ProbNNpi": "pi_" + probnn2_version % "ProbNNpi",
                    "ProbNNp": "pi_" + probnn_version % "ProbNNp",
                    "PIDK": "pi_" + dll_version % "CombDLLK",
                    "PIDp": "pi_" + dll_version % "CombDLLp",
                },
                "piminus": {
                    "ProbNNk": "pi_" + probnn_version % "ProbNNK",
                    "ProbNNpi": "pi_" + probnn2_version % "ProbNNpi",
                    "ProbNNp": "pi_" + probnn_version % "ProbNNp",
                    "PIDK": "pi_" + dll_version % "CombDLLK",
                    "PIDp": "pi_" + dll_version % "CombDLLp",
                },
            }
        )
    elif decay == "kstplusmumu":
        tracks.update(
            {
                "kplus": {
                    "ProbNNk": "K_" + probnn2_version % "ProbNNK",
                    "ProbNNpi": "K_" + probnn_version % "ProbNNpi",
                    "ProbNNp": "K_" + probnn_version % "ProbNNp",
                    "PIDK": "K_" + dll_version % "CombDLLK",
                    "PIDp": "K_" + dll_version % "CombDLLp",
                }
            }
        )
    elif decay == "rhoplusmumu":
        tracks.update(
            {
                "piplus": {
                    "ProbNNk": "pi_" + probnn_version % "ProbNNK",
                    "ProbNNpi": "pi_" + probnn2_version % "ProbNNpi",
                    "ProbNNp": "pi_" + probnn_version % "ProbNNp",
                    "PIDK": "pi_" + dll_version % "CombDLLK",
                    "PIDp": "pi_" + dll_version % "CombDLLp",
                }
            }
        )
    else:
        raise ValueError(f"Unknown decay {decay}")

    rdmstr = "".join(
        random.choice(string.ascii_uppercase + string.digits) for _ in range(5)
    )
    tmpinfile = inputfile
    tmpoutfile = "tmp1_{}_{}_{}_{}.root".format(decay, evttype, dataset, rdmstr)
    # tmpoutfile = "tmp1_{}_{}.root".format(dataset, rdmstr)

    for track, subst in tracks.items():
        for var, config in subst.items():
            command = "python $PIDPERFSCRIPTSROOT/scripts/python/PIDGenUser/PIDCorr.py"
            command += " -m %s_%s" % (track, ptvar)
            if etavar:
                command += " -e %s_%s" % (track, etavar)
            elif pvar:
                command += " -q %s_%s" % (track, pvar)
            else:
                print("Specify either ETA or P branch name per track")
                sys.exit(1)
            command += " -n %s" % ntrvar
            command += " -t %s" % tree
            command += " -p %s_%s_corr" % (track, var)
            command += " -s %s_%s" % (track, var)
            command += " -c %s" % config
            command += " -d %s" % dataset
            command += " -i %s" % tmpinfile
            command += " -o %s" % tmpoutfile
            command += " -S %s" % simversion
            # command += " -f 1.15"
            # command += " --noclone"

            tmpinfile = tmpoutfile
            if "tmp1" in tmpoutfile:
                tmpoutfile = tmpoutfile.replace("tmp1", "tmp2")
            else:
                tmpoutfile = tmpoutfile.replace("tmp2", "tmp1")

            # print(command)
            # print()
            # os.system(command)
            subprocess.run(command, shell=True, check=True)
    # exit()

    tmpcorrfile = tmpinfile.replace(".root", "_pidcorr.root")

    df = RDataFrame(tree, tmpinfile)
    df.Snapshot(tree, tmpcorrfile, r".*_corr")

    if outpath is None:
        outpath = os.path.join(os.path.dirname(inputfile))

    subprocess.run(
        "mv %s %s"
        % (tmpcorrfile, os.path.join(outpath, f"{dataset.lower()}_pidcorr.root")),
        shell=True,
        check=True,
    )
    subprocess.run("rm %s" % (tmpoutfile), shell=True, check=True)
    subprocess.run("rm %s" % (tmpinfile), shell=True, check=True)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Run PIDCorr.py for MC ntuples")
    parser.add_argument("inputfile", help="input file")
    parser.add_argument("--tree", nargs="?", default="DecayTree", help="tree name")
    parser.add_argument("--outpath", nargs="?", default=None, help="output path")
    parser.add_argument("--simversion", nargs="?", default="run2", help="sim version")
    args = parser.parse_args()
    pidcorr(args.inputfile, args.tree, args.outpath, args.simversion)
