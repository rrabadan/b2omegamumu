import uproot
import correctionlib
import correctionlib.convert


def from_hist(h, description, flow="clamp"):
    """
    Convert a histogram to a correction object.

    Args:
        h (histogram): The input histogram.
        description (str): The description of the correction.
        flow (str, optional): The flow mode. Defaults to 'clamp'.

    Returns:
        correction: The converted correction object.
    """
    corr = correctionlib.convert.from_histogram(h)
    corr.description = description
    corr.data.flow = flow
    return corr


def from_uproot_hist(file, name, description, flow="clamp"):
    """
    Convert a histogram stored in a ROOT file to a correction object.

    Args:
        file (str): The path to the ROOT file.
        name (str): The name of the histogram to retrieve.
        description (str): A description of the correction.
        flow (str, optional): The method to handle underflow and overflow bins. Defaults to 'clamp'.

    Returns:
        Correction: The correction object created from the histogram.

    Raises:
        FileNotFoundError: If the specified file is not found.
        KeyError: If the specified histogram is not found in the file.
    """
    try:
        f = uproot.open(file)
    except FileNotFoundError:
        msg = "File", file, "not found"
        raise FileNotFoundError(msg) from None

    try:
        h = f[name]
    except KeyError:
        msg = "Histogram", name, "not found in", file
        raise KeyError(msg) from None

    return from_hist(h.to_hist(), description, flow)


def import_from_json(file):
    """
    Imports a correction set from a JSON file.

    Args:
        file (str): The path to the JSON file.

    Returns:
        correctionlib.CorrectionSet: The imported correction set.

    Raises:
        FileNotFoundError: If the specified file is not found.
    """
    try:
        with open(file) as f:
            corr = correctionlib.CorrectionSet.from_file(f)
    except FileNotFoundError:
        msg = "File", file, "not found"
        raise FileNotFoundError(msg) from None

    return corr
