import os

import numpy as np
import uproot

from .correction import from_uproot_hist


def copy_tree_pid(file, decay, tree="DecayTree"):
    """
    Copy the PID branches necessary to run PID corr for the specified decay mode into a new ROOT file.

    Args:
        file (str): The path to the input ROOT file.
        decay (str): The decay mode ('B2omegaMuMu', 'Bplus2KstPlusMuMu', or 'Bplus2RhoPlusMuMu').
        tree (str, optional): The name of the tree to copy. Defaults to 'DecayTree'.

    Raises:
        ValueError: If an unknown decay mode is provided.

    Returns:
        None
    """
    outfile = file.replace(".root", "_pid.root")
    t = uproot.open(f"{file}:{tree}")
    arr = t.arrays(["nTracks", "nLongTracks"], library="np")
    arr.update(
        t.arrays(filter_name="/^(muplus|muminus)_(PIDmu|ProbNNmu).*/", library="np")
    )
    arr.update(t.arrays(filter_name="/^(muplus|muminus)_(P|PT)$/", library="np"))
    if decay == "omegamumu":
        arr.update(
            t.arrays(
                filter_name="/^(piplus|piminus)_(PID|ProbNN)(k|K|pi|p)$/", library="np"
            )
        )
        arr.update(t.arrays(filter_name="/^(piplus|piminus)_(P|PT)$/", library="np"))
    elif decay == "kstplusmumu":
        arr.update(
            t.arrays(filter_name="/^(kplus)_(PID|ProbNN)(k|K|pi|p)$/", library="np")
        )
        arr.update(t.arrays(filter_name="/^(kplus)_(P|PT)$/", library="np"))
    elif decay == "rhoplusmumu":
        arr.update(
            t.arrays(filter_name="/^(piplus)_(PID|ProbNN)(k|K|pi|p)$/", library="np")
        )
        arr.update(t.arrays(filter_name="/^(piplus)_(P|PT)$/", library="np"))
    else:
        raise ValueError(f"Unknown decay: {decay}")
    of = uproot.recreate(outfile)
    of["DecayTree"] = arr
    print("Copied pid tree:", outfile)


def load_correction_histogram(base_dir, sub_dir, file_name, hist_name, description):
    """
    Load a correction histogram from a ROOT file.

    Parameters:
    base_dir (str): The base directory
    sub_dir (str): The sub-directory
    file_name (str): The file name
    hist_name (str): The histogram name
    description (str): The description of the correction

    Returns:
    uproot_hist: The loaded correction histogram
    """
    file_path = os.path.join(base_dir, sub_dir, file_name)
    if not os.path.exists(file_path):
        raise FileNotFoundError(f"File {file_path} does not exist.")
    return from_uproot_hist(file_path, hist_name, description)
