import os
import pickle
import logging

from hep_ml import reweight


def create_reweighter(
    original, target, original_weight=None, target_weight=None, n_folds=5
):
    """
    Reweights the original dataset to match the target dataset using gradient boosting.

    Args:
        original (array-like): The original dataset.
        target (array-like): The target dataset.
        original_weight (array-like, optional): The weights of the original dataset. Defaults to None.
        target_weight (array-like, optional): The weights of the target dataset. Defaults to None.
        n_folds (int, optional): The number of folds for folding reweighting. Defaults to 2.

    Returns:
        reweighter (object): The reweighter object.
    """
    reweighter_base = reweight.GBReweighter(
        n_estimators=100,
        learning_rate=0.1,
        max_depth=3,
        min_samples_leaf=1000,
        gb_args={"subsample": 0.4},
    )
    reweighter = reweight.FoldingReweighter(reweighter_base, n_folds=n_folds)
    reweighter.fit(
        original, target, original_weight=original_weight, target_weight=target_weight
    )
    return reweighter


def save_reweighter(reweighter, dir, filename):
    """
    Save the reweighter to a pickle file.

    Args:
        reweighter (object): The reweighter object.
        dir (str): The directory where the pickle file will be saved.
        filename (str): The name of the pickle file.
    """

    # check if filename ends with .pkl and add it if not
    if not filename.endswith(".pkl"):
        filename += ".pkl"

    with open(os.path.join(dir, filename), "wb") as f:
        pickle.dump(reweighter, f)
    logging.info(f"Reweighter saved to {os.path.join(dir, filename)}")


def load_reweighter(dir, filename):
    """
    Load the reweighter from a pickle file.

    Args:
        dir (str): The directory where the pickle file is saved.
        filename (str): The name of the pickle file.

    Returns:
        object: The reweighter object.
    """

    # check if filename ends with .pkl and add it if not
    if not filename.endswith(".pkl"):
        filename += ".pkl"

    rwfile = os.path.join(dir, filename)
    if not os.path.exists(rwfile):
        logging.error(f"File {rwfile} does not exist")
        exit(1)

    logging.info(f"Loading reweighter from {rwfile}")
    with open(rwfile, "rb") as f:
        reweighter = pickle.load(f)
    return reweighter
