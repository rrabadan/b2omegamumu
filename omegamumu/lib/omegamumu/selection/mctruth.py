def cand_truth(cand, trueid, ancestry=[], op="&&"):
    cut = f"abs({cand}_TRUEID)=={trueid}"
    lab = "MC"
    for m in ancestry:
        cut = f"{cut} && abs({cand}_{lab}_MOTHER_ID)=={m}"
        lab = "_".join([lab, "GD"])
    return cut


def omegamumu_resonant_truth_info(b, psires, xres, optype="rdf"):
    """
    Returns a chain of truth cuts for the resonant decay chain
    B -> psi(mumu) x(pipipi0)
    Parameters:
    -----------
    b      : name and pdgid of the mother b (B, 511)
    psires : name and pdgid of the psi resonance (Jpsi, 443)
    xres   : name and pdgid of the x resonance (omega, 223)
    """

    b_name, b_pdgid = b
    cc_name, cc_pdgid = psires
    x_name, x_pdgid = xres

    if optype == "rdf":
        AND, OR, NOT = " && ", " || ", "!"
    elif optype == "pd":
        AND, OR, NOT = "&", "|", "~"
    elif optype == "np":
        AND, OR, NOT = "&", "|", "~"
    else:
        raise ValueError(
            f"optype {optype} not recognized. Choose between 'rdf' and 'pd' or 'np'"
        )

    true_info = {}

    mup = cand_truth("muplus", 13, ancestry=[cc_pdgid, b_pdgid], op=AND)
    mum = cand_truth("muminus", 13, ancestry=[cc_pdgid, b_pdgid], op=AND)
    true_info["true_muplus"] = mup
    true_info["true_muminus"] = mum

    psi = AND.join(
        ["true_muplus", "true_muminus", "muplus_MC_MOTHER_KEY==muminus_MC_MOTHER_KEY"]
    )
    true_info["true_dimuon"] = psi

    g1 = cand_truth("gamma1", 22, ancestry=[111, x_pdgid, b_pdgid], op=AND)
    g2 = cand_truth("gamma2", 22, ancestry=[111, x_pdgid, b_pdgid], op=AND)
    true_info["true_gamma1"] = g1
    true_info["true_gamma2"] = g2

    pi0 = AND.join(
        ["true_gamma1", "true_gamma2", "gamma1_MC_MOTHER_KEY==gamma2_MC_MOTHER_KEY"]
    )
    true_info["true_pi0"] = pi0

    pip = cand_truth("piplus", 211, ancestry=[x_pdgid, b_pdgid], op=AND)
    pim = cand_truth("piminus", 211, ancestry=[x_pdgid, b_pdgid], op=AND)
    true_info["true_piplus"] = pip
    true_info["true_piminus"] = pim

    pimpip = AND.join(
        ["true_piplus", "true_piminus", "piplus_MC_MOTHER_KEY==piminus_MC_MOTHER_KEY"]
    )
    true_info["true_dipion"] = pimpip

    x0 = AND.join(
        ["true_dipion", "true_pi0", "piplus_MC_MOTHER_KEY==gamma1_MC_GD_MOTHER_KEY"]
    )
    true_info["true_x0"] = x0

    mother = "piplus_MC_GD_MOTHER_KEY==muplus_MC_GD_MOTHER_KEY"

    b_2trueg = f"true_dimuon{AND}true_x0{AND}{mother}"
    b_1trueg = f"true_dimuon{AND}true_dipion{AND}{mother}{AND}((true_gamma1{AND}{NOT}true_gamma2){OR}({NOT}true_gamma1{AND}true_gamma2))"
    b_0trueg = f"true_dimuon{AND}true_dipion{AND}{mother}{AND}{NOT}true_gamma1{AND}{NOT}true_gamma2"
    true_info["true_b2g"] = b_2trueg
    true_info["true_b1g"] = b_1trueg
    true_info["true_b0g"] = b_0trueg

    return true_info


def omegamumu_rare_truth_info(b, xres, optype="rdf"):
    """
    Returns a chain of truth cuts for the resonant decay chain
    B -> mumu x(pipipi0)
    Parameters:
    -----------
    b  : name and pdgid of the mother b (B, 511)
    x  : name and pdgid of the x resonance (omega, 223)
    """

    b_name, b_pdgid = b
    x_name, x_pdgid = xres

    if optype == "rdf":
        AND, OR, NOT = " && ", " || ", "!"
    elif optype == "pd":
        AND, OR, NOT = "&", "|", "~"
    elif optype == "np":
        AND, OR, NOT = "&", "|", "~"
    else:
        raise ValueError(
            f"optype {optype} not recognized. Choose between 'rdf' and 'pd' or 'np'"
        )

    true_info = {}

    mup = cand_truth("muplus", 13, ancestry=[b_pdgid], op=AND)
    mum = cand_truth("muminus", 13, ancestry=[b_pdgid], op=AND)
    true_info["true_muplus"] = mup
    true_info["true_muminus"] = mum

    dimuon = AND.join(
        ["true_muplus", "true_muminus", "muplus_MC_MOTHER_KEY==muminus_MC_MOTHER_KEY"]
    )
    true_info["true_dimuon"] = dimuon

    g1 = cand_truth("gamma1", 22, ancestry=[111, x_pdgid, b_pdgid], op=AND)
    g2 = cand_truth("gamma2", 22, ancestry=[111, x_pdgid, b_pdgid], op=AND)
    true_info["true_gamma1"] = g1
    true_info["true_gamma2"] = g2

    pi0 = AND.join([g1, g2, "gamma1_MC_MOTHER_KEY==gamma2_MC_MOTHER_KEY"])
    true_info["true_pi0"] = pi0

    pip = cand_truth("piplus", 211, ancestry=[x_pdgid, b_pdgid], op=AND)
    pim = cand_truth("piminus", 211, ancestry=[x_pdgid, b_pdgid], op=AND)
    true_info["true_piplus"] = pip
    true_info["true_piminus"] = pim

    pimpip = AND.join(
        ["true_piplus", "true_piminus", "piplus_MC_MOTHER_KEY==piminus_MC_MOTHER_KEY"]
    )
    true_info["true_dipion"] = pimpip

    x0 = AND.join(
        ["true_dipion", "true_pi0", "piplus_MC_MOTHER_KEY==pi0_MC_MOTHER_KEY"]
    )
    true_info["true_x0"] = x0

    mother = "piplus_MC_GD_MOTHER_KEY==muplus_MC_MOTHER_KEY"

    b_2trueg = f"true_dimuon{AND}true_x0{AND}{mother}"
    b_1trueg = f"true_dimuon{AND}true_dipion{AND}{mother}{AND}((true_gamma1{AND}{NOT}true_gamma2){OR}({NOT}true_gamma1{AND}true_gamma2))"
    b_0trueg = f"true_dimuon{AND}true_dipion{AND}{mother}{AND}{NOT}true_gamma1{AND}{NOT}true_gamma2"
    true_info["true_b2g"] = b_2trueg
    true_info["true_b1g"] = b_1trueg
    true_info["true_b0g"] = b_0trueg

    return true_info


def kstplusmumu_resonant_truth_info(b, psires, xres, hplus, optype="rdf"):
    """
    Returns a chain of truth cuts for the resonant decay chain
    Bplus -> psi(mumu) x(hpluspi0)
    Parameters:
    -----------
    b      : name and pdgid of the mother b (Bplus, 511)
    psires : name and pdgid of the psi resonance (Jpsi, 443)
    xres   : name and pdgid of the x resonance (omega, 223)
    hp     : name and pdgid of the hplus resonance (kplus, 321)
    """

    b_name, b_pdgid = b
    cc_name, cc_pdgid = psires
    x_name, x_pdgid = xres
    hp_name, hp_pdgid = hplus

    if optype == "rdf":
        AND, OR, NOT = " && ", " || ", "!"
    elif optype == "pd":
        AND, OR, NOT = "&", "|", "~"
    elif optype == "np":
        AND, OR, NOT = "&", "|", "~"
    else:
        raise ValueError(
            f"optype {optype} not recognized. Choose between 'rdf' and 'pd' or 'np'"
        )

    true_info = {}

    mup = cand_truth("muplus", 13, ancestry=[cc_pdgid, b_pdgid], op=AND)
    mum = cand_truth("muminus", 13, ancestry=[cc_pdgid, b_pdgid], op=AND)
    true_info["true_muplus"] = mup
    true_info["true_muminus"] = mum

    psi = AND.join(
        ["true_muplus", "true_muminus", "muplus_MC_MOTHER_KEY==muminus_MC_MOTHER_KEY"]
    )
    true_info["true_dimuon"] = psi

    g1 = cand_truth("gamma1", 22, ancestry=[111, x_pdgid, b_pdgid], op=AND)
    g2 = cand_truth("gamma2", 22, ancestry=[111, x_pdgid, b_pdgid], op=AND)
    true_info["true_gamma1"] = g1
    true_info["true_gamma2"] = g2

    pi0 = AND.join(
        ["true_gamma1", "true_gamma2", "gamma1_MC_MOTHER_KEY==gamma2_MC_MOTHER_KEY"]
    )
    true_info["true_pi0"] = pi0

    hp = cand_truth(hp_name, hp_pdgid, ancestry=[x_pdgid, b_pdgid], op=AND)
    hp = AND.join([hp, f"{hp_name}_MC_GD_MOTHER_KEY==muplus_MC_GD_MOTHER_KEY"])
    true_info["true_hplus"] = hp

    b_2trueg = f"true_dimuon{AND}true_hplus{AND}true_pi0"
    b_1trueg = f"true_dimuon{AND}true_hplus{AND}((true_gamma1{AND}{NOT}true_gamma2){OR}({NOT}true_gamma1{AND}true_gamma2))"
    b_0trueg = f"true_dimuon{AND}true_hplus{AND}{NOT}true_gamma1{AND}{NOT}true_gamma2"
    true_info["true_b2g"] = b_2trueg
    true_info["true_b1g"] = b_1trueg
    true_info["true_b0g"] = b_0trueg

    return true_info


def extend_mctruth(df, truth_info, type="rdf"):
    """
    Extends the dataframe with the truth information
    Parameters:
    -----------
    df : dataframe
    truth_info : dictionary with the truth information
    type : type of dataframe, either 'rdf' or 'pd'
    """
    if type == "rdf":
        rdf = df
        for key, value in truth_info.items():
            rdf = rdf.Define(key, value)
        return rdf
    elif type == "pd":
        for key, value in truth_info.items():
            df.eval(f"{key} = {value}", inplace=True)
        return df
    else:
        raise ValueError(f"Type {type} not recognized. Choose between 'rdf' and 'pd'")
