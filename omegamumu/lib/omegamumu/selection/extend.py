from .selection import muons_pid, hadrons_pid, gamma_pid


def extend_dtf(df, decay="omegamumu", omega=False, psi2s=False):
    B = "B" if decay == "omegamumu" else "Bplus"
    df = (
        df.Define(f"{B}_Jpsi_DTF_M", f"{B}_JpsiConsDTF_M[0]")
        .Define(
            f"{B}_Jpsi_DTF_nChi2", f"{B}_JpsiConsDTF_chi2[0]/{B}_JpsiConsDTF_nDOF[0]"
        )
        .Define(
            f"{B}_Jpsi_DTF_ctausig",
            f"{B}_JpsiConsDTF_ctau[0]/{B}_JpsiConsDTF_ctauErr[0]",
        )
        .Define(f"{B}_piz_DTF_M", f"{B}_pi0ConsDTF_M[0]")
        .Define(f"{B}_piz_DTF_nChi2", f"{B}_pi0ConsDTF_chi2[0]/{B}_pi0ConsDTF_nDOF[0]")
        .Define(
            f"{B}_piz_DTF_ctausig", f"{B}_pi0ConsDTF_ctau[0]/{B}_pi0ConsDTF_ctauErr[0]"
        )
    )
    if psi2s:
        df = (
            df.Define(f"{B}_Psi2S_DTF_M", f"{B}_Psi2SConsDTF_M[0]")
            .Define(
                f"{B}_Psi2S_DTF_nChi2",
                f"{B}_Psi2SConsDTF_chi2[0]/{B}_Psi2SConsDTF_nDOF[0]",
            )
            .Define(
                f"{B}_Psi2S_DTF_ctausig",
                f"{B}_Psi2SConsDTF_ctau[0]/{B}_Psi2SConsDTF_ctauErr[0]",
            )
        )
    if omega and decay == "omegamumu":
        df = (
            df.Define("B_omega_DTF_M", "B_omegaConsDTF_M[0]")
            .Define(
                "B_omega_DTF_nChi2", "B_omegaConsDTF_chi2[0]/B_omegaConsDTF_nDOF[0]"
            )
            .Define(
                "B_omega_DTF_ctausig",
                "B_omegaConsDTF_ctau[0]/B_omegaConsDTF_ctauErr[0]",
            )
            .Define("B_omegaJpsi_DTF_M", "B_omegaJpsiConsDTF_M[0]")
            .Define(
                "B_omegaJpsi_DTF_nChi2",
                "B_omegaJpsiConsDTF_chi2[0]/B_omegaJpsiConsDTF_nDOF[0]",
            )
            .Define(
                "B_omegaJpsi_DTF_ctausig",
                "B_omegaJpsiConsDTF_ctau[0]/B_omegaJpsiConsDTF_ctauErr[0]",
            )
        )
    return df


def extend_dimuon_q2(df):
    df = df.Define("dimuon_q2", "dimuon_M*dimuon_M/1000000")
    return df


def extend_gammapidsel(df):
    gammapid = "&&".join([gamma_pid("gamma1"), gamma_pid("gamma2")])
    df = df.Define("gammapid_sel", gammapid)
    return df


def extend_pidsel(df, decay="omegamumu", prefix="", pidcorr=False, pidgen=False):

    assert not (pidcorr and pidgen), "Only one of pidcorr and pidgen can be True"

    branch_suffix = "pidcorr_sel" if pidcorr else "pidgen_sel" if pidgen else "pid_sel"

    muonpid = muons_pid(prefix, pidcorr, pidgen)
    df = df.Define(f"muon_{branch_suffix}", muonpid)

    hadronpid = hadrons_pid(decay, prefix, pidcorr, pidgen)
    df = df.Define(f"hadron_{branch_suffix}", hadronpid)

    df = df.Define(
        f"{branch_suffix}", f"muon_{branch_suffix} && hadron_{branch_suffix}"
    )

    return df
