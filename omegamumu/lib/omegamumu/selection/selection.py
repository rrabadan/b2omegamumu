from .cuts import *


def obj_sel(obj, cuts, op="&&"):
    return op.join([c.format(obj) for c in cuts])


def trigger_sel(obj, triggers, op="||", dec="TOS"):
    return op.join([f"{obj}_{t}_{dec}" for t in triggers])


def multiplicity_filter(op="&&"):
    return op.join(multiplicity)


def muon_filter(cand, op="&&"):
    return obj_sel(cand, muoncuts, op=op)


def pion_filter(cand, op="&&"):
    return obj_sel(cand, pioncuts, op=op)


def gamma_filter(cand, op="&&"):
    return obj_sel(cand, gammacuts, op=op)


def pizero_filter(cand, op="&&"):
    return obj_sel(cand, pizerocuts, op=op)


def dimuon_filter(cand, op="&&"):
    return obj_sel(cand, dimuoncuts, op=op)


def meson_filter(cand, op="&&"):
    return obj_sel(cand, mesoncuts, op=op)


def b_filter(cand, op="&&"):
    return obj_sel(cand, bcuts, op=op)


def pion_pid(cand, op="&&", pidcorr=False, pidgen=False):
    assert not (pidcorr and pidgen), "Cannot apply both PIDgen and PIDcorr"
    sel = obj_sel(cand, pionpid, op=op)
    if pidcorr:
        return sel.replace("ProbNNpi", "ProbNNpi_corr").replace("PIDK", "PIDK_corr")
    if pidgen:
        return sel.replace("ProbNNpi", "ProbNNpi_pidgen_default").replace(
            "PIDK", "PIDK_pidgen_default"
        )
    return sel


def kaon_pid(cand, op="&&", pidcorr=False, pidgen=False):
    assert not (pidcorr and pidgen), "Cannot apply both PIDgen and PIDcorr"
    sel = obj_sel(cand, kaonpid, op=op)
    if pidcorr:
        return sel.replace("ProbNNK", "ProbNNK_corr").replace("PIDK", "PIDK_corr")
    if pidgen:
        return sel.replace("ProbNNK", "ProbNNK_pidgen_default").replace(
            "PIDK", "PIDK_pidgen_default"
        )
    return sel


def muon_pid(cand, op="&&", pidcorr=False, pidgen=False):
    assert not (pidcorr and pidgen), "Cannot apply both PIDgen and PIDcorr"
    sel = obj_sel(cand, muonpid, op=op)
    if pidcorr:
        return sel.replace("ProbNNmu", "ProbNNmu_corr").replace("PIDmu", "PIDmu_corr")
    if pidgen:
        return sel.replace("ProbNNmu", "ProbNNmu_pidgen_default").replace(
            "PIDmu", "PIDmu_pidgen_default"
        )
    return sel


def muons_pid(prefix, pidcorr, pidgen):
    return "&&".join(
        [
            muon_pid(f"{prefix}muplus", pidcorr=pidcorr, pidgen=pidgen),
            muon_pid(f"{prefix}muminus", pidcorr=pidcorr, pidgen=pidgen),
        ]
    )


def hadrons_pid(decay, prefix, pidcorr, pidgen):
    if decay == "omegamumu":
        return "&&".join(
            [
                pion_pid(f"{prefix}piplus", pidcorr=pidcorr, pidgen=pidgen),
                pion_pid(f"{prefix}piminus", pidcorr=pidcorr, pidgen=pidgen),
            ]
        )
    elif decay == "kstplusmumu":
        return kaon_pid(f"{prefix}kplus", pidcorr=pidcorr, pidgen=pidgen)
    elif decay == "rhoplusmumu":
        return pion_pid(f"{prefix}piplus", pidcorr=pidcorr, pidgen=pidgen)
    else:
        raise ValueError(f"Unknown decay {decay}")


def gamma_pid(cand, op="&&"):
    return obj_sel(cand, gammapid, op=op)


def lowq2_filter(cand, op="&&"):
    return obj_sel(cand, lowq2, op=op)


def highq2_filter(cand, op="&&"):
    return obj_sel(cand, highq2, op=op)


def centralq2_filter(cand, op="&&"):
    return obj_sel(cand, centralq2, op=op)


def resonant_q2_veto(cand, op="&&"):
    jpsi = obj_sel(cand, jpsiq2, op=op)
    psi = obj_sel(cand, psiq2, op=op)
    return f"!({jpsi} || {psi})"


def eta_window(cand, op="&&"):
    return obj_sel(cand, eta_cut, op=op)


def omega_window(cand, op="&&"):
    return obj_sel(cand, omega_cut, op=op)


def kstplus_window(cand, op="&&"):
    return obj_sel(cand, kstar_cut, op=op)


def rhoplus_window(cand, op="&&"):
    return obj_sel(cand, rho_cut, op=op)


def phi_window(cand, op="&&"):
    return obj_sel(cand, phi_cut, op=op)


def xcand_window(cand, xcand, op="&&"):
    if xcand == "eta":
        return eta_window(cand, op=op)
    elif xcand == "omega":
        return omega_window(cand, op=op)
    elif xcand == "phi":
        return phi_window(cand, op=op)
    elif xcand == "kst":
        return kstplus_window(cand, op=op)
    elif xcand == "rho":
        return rhoplus_window(cand, op=op)
    else:
        raise ValueError(f"Unknown xcand meson {xcand}")
