# Names of triggers used
triggers = {
    "L0": [
        "L0MuonDecision",
        #'L0DiMuonDecision'
    ],
    "Hlt1": ["Hlt1TrackMVADecision", "Hlt1TrackMuonDecision"],
    "Hlt2": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
        "Hlt2TopoMu2BodyDecision",
        "Hlt2TopoMu3BodyDecision",
        #'Hlt2DiMuonDetachedDecision',
        #'Hlt2TopoMuMu2BodyDecision',
        #'Hlt2TopoMuMu3BodyDecision'
    ],
}

####################
## Stripping cuts ##
####################
# Multiplicity cuts
multiplicity = ["nSPDHits<600"]

# Charged tracks cuts
trkcuts = ["{}_IPCHI2_OWNPV>6", "{}_TRACK_GhostProb<0.5", "{}_TRACK_CHI2NDOF<3"]

# dimuon
dimuoncuts = [
    "({0}_ENDVERTEX_CHI2/{0}_ENDVERTEX_NDOF)<12",
    "{0}_DIRA_OWNPV>-0.9",
    "{0}_FDCHI2_OWNPV>9",
]

# B candidate's cuts
bcuts = [
    "{0}_IPCHI2_OWNPV<16",
    "{0}_DIRA_OWNPV>0.9999",
    "{0}_FDCHI2_OWNPV>64",
    "({0}_ENDVERTEX_CHI2/{0}_ENDVERTEX_NDOF)<8",
]
####################

pioncuts = trkcuts + [
    "{}_hasRich",
    "{}_PT>250",
    "{}_P>2000",
    "{}_P<200000",
]

muoncuts = trkcuts + [
    "{}_hasRich",
    "{}_PT>300",
    "{}_P>3000",
    "{}_P<200000",
    "{}_PIDmu>-3.0",  # from stripping
    "{}_isMuon==1",  # from stripping
]

multiplicity += ["nTracks<300"]

# pi0 and meson cuts
pizerocuts = [
    "{}_PT>800",
    #    '{}_CL>0.02'
]
gammacuts = ["{}_PT>200"]
mesoncuts = ["{}_PT>1000"]

bcuts += ["{0}_M>4800", "{0}_M<6800"]

# PID
pionpid = [
    #    '{}_PIDK<5',
    #    '{}_isMuon==0',
    "{}_ProbNNpi>0.05"
]
kaonpid = [
    #    '{0}_PIDK>0',
    #    '{0}_isMuon==0',
    "{0}_ProbNNk>0.1",  # tight kaon PID for K*+ mode
    "{0}_ProbNNk*(1.-{0}_ProbNNpi)>0.5",
]
muonpid = [
    #'{}_PIDmu>0',
    "{}_ProbNNmu>0.05"
]
gammapid = ["{}_PP_IsNotH>0.05"]

# Masses
pizero_cut = ["{}_M>110", "{}_M<160"]

eta_cut = ["{}_M>448", "{}_M<648"]
omega_cut = ["{}_M>680", "{}_M<880"]
phi_cut = ["{}_M>970", "{}_M<1070"]
kstar_cut = ["{}_M>792", "{}_M<992"]
rho_cut = ["{}_M>500", "{}_M<1000"]

eta_narrow = ["{}_M>498", "{}_M<598"]
omega_narrow = ["{}_M>730", "{}_M<830"]

jpsi_cut = ["{}_M>3047", "{}_M<3147"]
psi2s_cut = ["{}_M>3636", "{}_M<3736"]

lowq2 = ["{}_q2>1.1", "{}_q2<8.0"]
centralq2 = ["{}_q2>11.0", "{}_q2<12.5"]
highq2 = ["{}_q2>15.0", "{}_q2<19.0"]
jpsiq2 = ["{}_q2>8.0", "{}_q2<11.0"]
psiq2 = ["{}_q2>12.5", "{}_q2<15.0"]
