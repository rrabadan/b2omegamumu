from ..common.rootutils import book_df, write_df, write_cuts_report
from .selection import *


def preselection(df, decay="omegamumu"):

    muons = "&&".join([muon_filter("muplus"), muon_filter("muminus")])
    gammas = "&&".join([gamma_filter("gamma1"), gamma_filter("gamma2")])
    pizero = pizero_filter("pi0")

    if decay == "omegamumu":
        bprefix = "B"
        xprefix = "omega"
        hadrons = "&&".join([pion_filter("piplus"), pion_filter("piminus")])
    elif decay == "kstplusmumu":
        bprefix = "Bplus"
        xprefix = "kstplus"
        hadrons = pion_filter("kplus")
    elif decay == "rhoplusmumu":
        bprefix = "Bplus"
        xprefix = "rhoplus"
        hadrons = pion_filter("piplus")
    else:
        raise ValueError(f"Unknown decay {decay}")

    l0 = trigger_sel("dimuon", triggers["L0"])
    hlt1 = trigger_sel("dimuon", triggers["Hlt1"])
    hlt2 = trigger_sel(bprefix, triggers["Hlt2"])

    dimuon = dimuon_filter("dimuon")
    xmeson = meson_filter(xprefix)
    bmeson = b_filter(bprefix)
    if decay == "kstplusmumu":
        xmeson = xmeson + "&&" + kstplus_window("kstplus")

    mult = multiplicity_filter()

    df = (
        df.Filter(l0, "L0 trigger")
        .Filter(hlt1, "Hlt1 trigger")
        .Filter(hlt2, "Hlt2 trigger")
        .Filter(mult, "multiplicity")
        .Filter(muons, "muons")
        .Filter(dimuon, "dimuon")
        .Filter(hadrons, "hadrons")
        .Filter(gammas, "photons")
        .Filter(pizero, "pi zero")
        .Filter(xmeson, "light meson")
        .Filter(bmeson, "b meson")
    )

    return df


def pidselection(df, decay="omegamumu", pidcorr=False):
    muonpid = "&&".join(
        [muon_pid("muplus", pidcorr=pidcorr), muon_pid("muminus", pidcorr=pidcorr)]
    )
    gammapid = "&&".join([gamma_pid("gamma1"), gamma_pid("gamma2")])
    df = df.Filter(muonpid, "muon PID").Filter(gammapid, "gamma PID")
    if decay == "omegamumu":
        pionspid = "&&".join(
            [pion_pid("piplus", pidcorr=pidcorr), pion_pid("piminus", pidcorr=pidcorr)]
        )
        df = df.Filter(pionspid, "hadron PID")
    elif decay == "kstplusmumu":
        df = df.Filter(kaon_pid("kplus", pidcorr=pidcorr), "hadron PID")
    elif decay == "rhoplusmumu":
        df = df.Filter(pion_pid("piplus", pidcorr=pidcorr), "hadron PID")
    else:
        raise ValueError(f"Unknown decay {decay}")
    return df


def skim(
    tree,
    files,
    decay,
    skimfile,
    skimreport,
    pid=False,
):
    df = book_df(tree, files)
    df = preselection(df, decay=decay)

    if pid:
        df = pidselection(df, decay=decay)
    report = df.Report()

    # from plotting.rootplots import setLHCbStyle
    # from .histograms import define_histograms, draw_hists

    # setLHCbStyle()
    # hists = [book_histogram(df, h) for h in define_histograms(decay)]

    write_df(df, "DecayTree", skimfile)
    report.Print()
    write_cuts_report(report, skimreport)

    # c = draw_hists("mass_plots", hists)
    # c.SaveAs(skimcanvas)
