from math import pi
from particle import Particle
from omegamumu.common.kinematics import vect_pxpypzpe, vect_pxpypzm


def omegamumu(arr):

    dimuon = vect_pxpypzpe(arr, "dimuon")

    piz = vect_pxpypzpe(arr, "pi0")
    pip = vect_pxpypzpe(arr, "piplus")
    pim = vect_pxpypzpe(arr, "piminus")
    mup = vect_pxpypzpe(arr, "muplus")
    mum = vect_pxpypzpe(arr, "muminus")

    kp = vect_pxpypzm(arr, "piplus", Particle.from_pdgid(321).mass)
    km = vect_pxpypzm(arr, "piminus", Particle.from_pdgid(-321).mass)

    pi_mup = vect_pxpypzm(arr, "piplus", Particle.from_pdgid(13).mass)
    pi_mum = vect_pxpypzm(arr, "piminus", Particle.from_pdgid(-13).mass)

    pimup_mum = pi_mup + mum
    pimum_mup = pi_mum + mup

    mumukk = dimuon + kp + km
    mumukmpip = dimuon + km + pip
    mumukppim = dimuon + kp + pim
    mumupipi = dimuon + pip + pim

    mumukppiz = dimuon + kp + piz
    mumukmpiz = dimuon + km + piz

    mumupippiz = dimuon + pip + piz
    mumupimpiz = dimuon + pim + piz

    Bu = Particle.from_pdgid(521)
    Bd = Particle.from_pdgid(511)
    Bs = Particle.from_pdgid(531)

    mumuphi_veto = (mumukk.mass > Bs.mass + 50) | (mumukk.mass < Bs.mass - 50)
    mumupipi_veto = (mumupipi.mass > Bd.mass + 50) | (mumupipi.mass < Bd.mass - 50)

    mumukst_veto = (mumukppim.mass > Bd.mass + 50) | (mumukppim.mass < Bd.mass - 50)
    mumukst_veto = mumukst_veto & (mumukmpip.mass > Bd.mass + 50) | (
        mumukmpip.mass < Bd.mass - 50
    )

    mumukstplus_veto = (mumukppiz.mass > Bu.mass + 50) | (mumukppiz.mass < Bu.mass - 50)
    mumukstplus_veto = mumukstplus_veto & (mumukmpiz.mass > Bu.mass + 50) | (
        mumukmpiz.mass < Bu.mass - 50
    )

    mumurhoplus_veto = (mumupippiz.mass > Bu.mass + 50) | (
        mumupippiz.mass < Bu.mass - 50
    )
    mumurhoplus_veto = mumurhoplus_veto & (mumupimpiz.mass > Bu.mass + 50) | (
        mumupimpiz.mass < Bu.mass - 50
    )

    return dict(
        {
            "pimup_mum_mass": pimup_mum.mass,
            "pimum_mup_mass": pimum_mup.mass,
            "mumuphi_veto": mumuphi_veto,
            "mumukst_veto": mumukst_veto,
            "mumukstplus_veto": mumukstplus_veto,
            "mumurhoplus_veto": mumurhoplus_veto,
            "mumupipi_veto": mumupipi_veto,
            "mumukk_mass": mumukk.mass,
            "mumupipi_mass": mumupipi.mass,
            "mumukmpip_mass": mumukmpip.mass,
            "mumukppim_mass": mumukppim.mass,
            "mumukmpiz_mass": mumukmpiz.mass,
            "mumukppiz_mass": mumukppiz.mass,
            "mumupimpiz_mass": mumupimpiz.mass,
            "mumupippiz_mass": mumupippiz.mass,
        }
    )


def kstplusmumu(arr):

    dimuon = vect_pxpypzpe(arr, "dimuon")

    piz = vect_pxpypzpe(arr, "pi0")
    kp = vect_pxpypzpe(arr, "kplus")
    mum = vect_pxpypzpe(arr, "muminus")

    pip = vect_pxpypzm(arr, "kplus", Particle.from_pdgid(211).mass)
    k_mup = vect_pxpypzm(arr, "kplus", Particle.from_pdgid(13).mass)
    mu_kp = vect_pxpypzm(arr, "muplus", Particle.from_pdgid(321).mass)

    mumukplus = dimuon + kp
    mumupiplus = dimuon + pip
    mumupizero = dimuon + piz
    mumurhoplus = dimuon + pip + piz

    kmup_mum = k_mup + mum
    mukp_piz = mu_kp + piz

    Bu = Particle.from_pdgid(521)
    Bd = Particle.from_pdgid(511)

    mumukplus_veto = (mumukplus.mass > Bu.mass + 50) | (mumukplus.mass < Bu.mass - 50)
    mumupiplus_veto = (mumupiplus.mass > Bu.mass + 50) | (
        mumupiplus.mass < Bu.mass - 50
    )
    mumupizero_veto = (mumupizero.mass > Bd.mass + 50) | (
        mumupizero.mass < Bd.mass - 50
    )
    mumurhoplus_veto = (mumurhoplus.mass > Bd.mass + 50) | (
        mumurhoplus.mass < Bd.mass - 50
    )

    return dict(
        {
            "mumukplus_veto": mumukplus_veto,
            "mumupiplus_veto": mumupiplus_veto,
            "mumupizero_veto": mumupizero_veto,
            "mumurhoplus_veto": mumurhoplus_veto,
            "mumukplus_mass": mumukplus.mass,
            "mumupiplus_mass": mumupiplus.mass,
            "mumupizero_mass": mumupizero.mass,
            "mumurhoplus_mass": mumurhoplus.mass,
            "kmup_mum_mass": kmup_mum.mass,
            "mukp_piz_mass": mukp_piz.mass,
            "kmup_mum_muKp_mass": (k_mup + mum + mu_kp).mass,
        }
    )
