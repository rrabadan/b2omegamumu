from collections import namedtuple

from ..plotting.rootplots import divide_canvas

HistT = namedtuple("HistT", ["name", "title", "bins", "low", "high", "val", "logy"])

omegamumuHistsT = [
    HistT(
        "dimuon_mass",
        "; M(#mu^{+}#mu^{-}) [MeV]; Candidates",
        200,
        0,
        6000,
        "dimuon_M",
        True,
    ),
    HistT(
        "pizero_mass",
        "; M(#gamma#gamma) [MeV]; Candidates",
        80,
        100,
        180,
        "pi0_M",
        False,
    ),
    HistT(
        "omega_mass",
        "; M(#pi^{+}#pi^{-}#pi^{0}) [MeV]; Candidates",
        100,
        400,
        1100,
        "omega_M",
        False,
    ),
    HistT(
        "B_mass",
        "; M(J/#psi #pi^{+} #pi^{-} #pi^{0}) [MeV]; Candidates",
        200,
        4800,
        6600,
        "B_M",
        False,
    ),
]

kstmumuHistsT = [
    HistT(
        "dimuon_mass",
        "; M(#mu^{+}#mu^{-}) [MeV]; Candidates",
        200,
        0,
        6000,
        "dimuon_M",
        True,
    ),
    HistT(
        "pizero_mass",
        "; M(#gamma#gamma) [MeV]; Candidates",
        80,
        100,
        180,
        "pi0_M",
        False,
    ),
    HistT(
        "kstplus_mass",
        "; M(K^{+}#pi^{0}) [MeV]; Candidates",
        100,
        750,
        1050,
        "kstplus_M",
        False,
    ),
    HistT(
        "Bplus_mass",
        "; M(J/#psi K^{+} #pi^{0}) [MeV]; Candidates",
        200,
        4800,
        6600,
        "Bplus_M",
        False,
    ),
]

rhomumuHistsT = [
    HistT(
        "dimuon_mass",
        "; M(#mu^{+}#mu^{-}) [MeV]; Candidates",
        200,
        0,
        6000,
        "dimuon_M",
        True,
    ),
    HistT(
        "pizero_mass",
        "; M(#gamma#gamma) [MeV]; Candidates",
        80,
        100,
        180,
        "pi0_M",
        False,
    ),
    HistT(
        "rhoplus_mass",
        "; M(#pi^{+}#pi^{0}) [MeV]; Candidates",
        100,
        370,
        1170,
        "rhoplus_M",
        False,
    ),
    HistT(
        "Bplus_mass",
        "; M(J/#psi #pi^{+} #pi^{0}) [MeV]; Candidates",
        200,
        4800,
        6600,
        "Bplus_M",
        False,
    ),
]


# Define the histograms
def define_histograms(decay="omegamumu"):
    if decay == "omegamumu":
        return omegamumuHistsT
    elif decay == "kstplusmumu":
        return kstmumuHistsT
    elif decay == "rhoplusmumu":
        return rhomumuHistsT
    else:
        raise ValueError(f"Unknown decay {decay}")


def draw_hists(name, hists, cols=2, size=(1000, 1000)):
    c = divide_canvas(name, len(hists), cols=cols, size=size)
    for i, h in enumerate(hists):
        pad = c.cd(i + 1)
        h.Draw()
        if h.GetName() == "dimuon_mass":
            pad.SetLogy()
    return c
