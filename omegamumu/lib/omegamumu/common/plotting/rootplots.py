import ROOT
import math
from array import array

lhcbFont = 132  # Old LHCb style: 62;
lhcbWidth = 2  # Old LHCb style: 3.00;
lhcbTSize = 0.06

lhcbStyle = ROOT.TStyle("lhcbStyle", "Standard LHCb plots style")
lhcbLabel = ROOT.TText()
lhcbLatex = ROOT.TLatex()


def setLHCbStyle(suppress_Xerrors=True):

    ROOT.gROOT.SetBatch(ROOT.kTRUE)
    ROOT.gROOT.SetStyle("Plain")

    # define names for colours
    black = 1
    red = 2
    green = 3
    blue = 4
    yellow = 5
    magenta = 6
    cyan = 7
    purple = 9

    #  don't suppress the error bar along X
    if suppress_Xerrors:
        lhcbStyle.SetErrorX(0)

    lhcbStyle.SetFillColor(1)
    lhcbStyle.SetFillStyle(1001)
    # solid
    lhcbStyle.SetFrameFillColor(0)
    lhcbStyle.SetFrameBorderMode(0)
    lhcbStyle.SetPadBorderMode(0)
    lhcbStyle.SetPadColor(0)
    lhcbStyle.SetCanvasBorderMode(0)
    lhcbStyle.SetCanvasColor(0)
    lhcbStyle.SetStatColor(0)
    lhcbStyle.SetLegendBorderSize(0)
    lhcbStyle.SetLegendFont(lhcbFont)

    # If you want the usual gradient palette (blue . red)
    lhcbStyle.SetPalette(1)
    # If you want colors that correspond to gray scale in black and white:
    colors = array("i", [0, 5, 7, 3, 6, 2, 4, 1])
    lhcbStyle.SetPalette(8, colors)

    # set the paper & margin sizes
    lhcbStyle.SetPaperSize(20, 26)
    lhcbStyle.SetPadTopMargin(0.05)
    lhcbStyle.SetPadRightMargin(0.05)
    # increase for colz plots
    lhcbStyle.SetPadBottomMargin(0.16)
    lhcbStyle.SetPadLeftMargin(0.14)

    # use large fonts
    lhcbStyle.SetTextFont(lhcbFont)
    lhcbStyle.SetTextSize(lhcbTSize)
    lhcbStyle.SetLabelFont(lhcbFont, "x")
    lhcbStyle.SetLabelFont(lhcbFont, "y")
    lhcbStyle.SetLabelFont(lhcbFont, "z")
    lhcbStyle.SetLabelSize(lhcbTSize * 1.05, "x")
    lhcbStyle.SetLabelSize(lhcbTSize * 1.05, "y")
    lhcbStyle.SetLabelSize(lhcbTSize * 1.05, "z")
    lhcbStyle.SetTitleFont(lhcbFont)
    lhcbStyle.SetTitleFont(lhcbFont, "x")
    lhcbStyle.SetTitleFont(lhcbFont, "y")
    lhcbStyle.SetTitleFont(lhcbFont, "z")
    lhcbStyle.SetTitleSize(1.2 * lhcbTSize, "x")
    lhcbStyle.SetTitleSize(1.2 * lhcbTSize, "y")
    lhcbStyle.SetTitleSize(1.2 * lhcbTSize, "z")

    # use medium bold lines and thick markers
    lhcbStyle.SetLineWidth(lhcbWidth)
    lhcbStyle.SetFrameLineWidth(lhcbWidth)
    lhcbStyle.SetHistLineWidth(lhcbWidth)
    lhcbStyle.SetFuncWidth(lhcbWidth)
    lhcbStyle.SetGridWidth(lhcbWidth)
    lhcbStyle.SetLineStyleString(2, "[12 12]")
    # postscript dashes
    lhcbStyle.SetMarkerStyle(20)
    lhcbStyle.SetMarkerSize(1.0)

    # label offsets
    lhcbStyle.SetLabelOffset(0.010, "X")
    lhcbStyle.SetLabelOffset(0.010, "Y")

    # by default, do not display histogram decorations:
    lhcbStyle.SetOptStat(0)
    # lhcbStyle.SetOptStat("emr");  # show only nent -e , mean - m , rms -r
    # full opts at http:#root.cern.ch/root/html/TStyle.html#TStyle:SetOptStat
    lhcbStyle.SetStatFormat("6.3g")
    # specified as c printf options
    lhcbStyle.SetOptTitle(0)
    lhcbStyle.SetOptFit(0)
    # lhcbStyle.SetOptFit(1011); # order is probability, Chi2, errors, parameters
    # titles
    lhcbStyle.SetTitleOffset(0.95, "X")
    lhcbStyle.SetTitleOffset(0.95, "Y")
    lhcbStyle.SetTitleOffset(1.2, "Z")
    lhcbStyle.SetTitleFillColor(0)
    lhcbStyle.SetTitleStyle(0)
    lhcbStyle.SetTitleBorderSize(0)
    lhcbStyle.SetTitleFont(lhcbFont, "title")
    lhcbStyle.SetTitleX(0.0)
    lhcbStyle.SetTitleY(1.0)
    lhcbStyle.SetTitleW(1.0)
    lhcbStyle.SetTitleH(0.05)

    # look of the statistics box:
    lhcbStyle.SetStatBorderSize(0)
    lhcbStyle.SetStatFont(lhcbFont)
    lhcbStyle.SetStatFontSize(0.05)
    lhcbStyle.SetStatX(0.9)
    lhcbStyle.SetStatY(0.9)
    lhcbStyle.SetStatW(0.25)
    lhcbStyle.SetStatH(0.15)

    # put tick marks on top and RHS of plots
    lhcbStyle.SetPadTickX(1)
    lhcbStyle.SetPadTickY(1)

    # histogram divisions: only 5 in x to avoid label overlaps
    lhcbStyle.SetNdivisions(505, "x")
    lhcbStyle.SetNdivisions(510, "y")

    ROOT.gROOT.SetStyle("lhcbStyle")
    ROOT.gROOT.ForceStyle()

    # define style for text
    lhcbLabel.SetTextFont(lhcbFont)
    lhcbLabel.SetTextColor(1)
    lhcbLabel.SetTextSize(0.04)
    lhcbLabel.SetTextAlign(12)

    # define style of latex text
    lhcbLatex.SetTextFont(lhcbFont)
    lhcbLatex.SetTextColor(1)
    lhcbLatex.SetTextSize(0.04)
    lhcbLatex.SetTextAlign(12)

    ROOT.gROOT.SetStyle("lhcbStyle")
    ROOT.gROOT.ForceStyle()

    print("-------------------------")
    print("Set LHCb Style - Feb 2012")
    print("-------------------------")

    return


def printLHCb(optLR="L", isPrelim=False, optText=""):

    lhcbName = None

    if optLR == "R":
        lhcbName = ROOT.TPaveText(
            0.70 - lhcbStyle.GetPadRightMargin(),
            0.85 - lhcbStyle.GetPadTopMargin(),
            0.95 - lhcbStyle.GetPadRightMargin(),
            0.95 - lhcbStyle.GetPadTopMargin(),
            "BRNDC",
        )
    elif optLR == "L":
        lhcbName = ROOT.TPaveText(
            lhcbStyle.GetPadLeftMargin() + 0.05,
            0.87 - lhcbStyle.GetPadTopMargin(),
            lhcbStyle.GetPadLeftMargin() + 0.30,
            0.97 - lhcbStyle.GetPadTopMargin(),
            "BRNDC",
        )
    elif optLR == "BR":
        lhcbName = ROOT.TPaveText(
            0.70 - lhcbStyle.GetPadRightMargin(),
            0.10 + lhcbStyle.GetPadBottomMargin(),
            0.95 - lhcbStyle.GetPadRightMargin(),
            0.20 + lhcbStyle.GetPadBottomMargin(),
            "BRNDC",
        )

    if isPrelim:
        lhcbName.AddText("#splitline{LHCb}{#scale[1.0]{Preliminary}}")
    else:
        lhcbName.AddText("LHCb")

    lhcbName.SetFillColor(0)
    lhcbName.SetFillStyle(0)
    lhcbName.SetTextAlign(12)
    lhcbName.SetBorderSize(0)
    lhcbName.Draw()

    return [lhcbName]


def drawStackedPlot(name, var, func, data, components=[], useLogY=False):
    """Draw stacked histogram"""
    canvas = ROOT.TCanvas("canvas", "canvas")

    plot = var.frame()
    data.plotOn(plot)

    component = ",".join([c[0] for c in components])

    for c in components:
        print(component)
        func.plotOn(
            plot,
            ROOT.RooFit.Components(component),
            ROOT.RooFit.DrawOption("F"),
            ROOT.RooFit.FillColor(c[1]),
        )
        component = component.split("%s," % c[0])[-1]

    func.plotOn(plot)
    data.plotOn(plot)
    plot.Draw()

    if useLogY:
        canvas.SetLogy()

    canvas.Update()
    canvas.Print(name + ".pdf")
    canvas.Print(name + ".png")
    return


def drawResidualPlot(name, var, func, data, useLogY=False):
    """Draw residul plot"""
    canvas = ROOT.TCanvas("canvas", "canvas", 800, 800)

    upper = ROOT.TPad("upper", "upper", 0.0, 0.25, 1.0, 1.0)
    lower = ROOT.TPad("lower", "lower", 0.0, 0.0, 1.0, 0.25)

    upper.Draw()
    lower.Draw()

    upper.cd()

    plot = var.frame()
    data.plotOn(plot)
    func.plotOn(plot)
    plot.Draw()

    if useLogY:
        upper.SetLogy()

    lower.cd()

    hist = plot.pullHist()
    hist.SetName("pull")
    hist.GetYaxis().SetLabelSize(3 * hist.GetYaxis().GetLabelSize())
    hist.GetXaxis().SetLabelSize(0)
    hist.GetYaxis().SetNdivisions(503)
    hist.SetMaximum(5.0)
    hist.SetMinimum(-5.0)
    hist.GetXaxis().SetTitle("")
    hist.GetYaxis().SetTitle("")

    hist.GetXaxis().SetLimits(var.getMin(), var.getMax())

    hist.Draw("AP")

    lmid = ROOT.TLine(var.getMin(), 0.0, var.getMax(), 0.0)
    llow = ROOT.TLine(var.getMin(), -3.0, var.getMax(), -3.0)
    lupp = ROOT.TLine(var.getMin(), 3.0, var.getMax(), 3.0)

    lmid.SetLineColor(ROOT.kGray + 2)
    llow.SetLineColor(ROOT.kGray + 2)
    lupp.SetLineColor(ROOT.kGray + 2)

    lmid.Draw()
    llow.Draw()
    lupp.Draw()

    lower.RedrawAxis()
    canvas.Update()

    canvas.Print(name + ".pdf")
    canvas.Print(name + ".png")

    return


def divide_canvas(name, n, cols=2, size=(800, 600)):
    c = ROOT.TCanvas(name, name, size[0], size[1])
    rows = math.ceil(n / cols)
    c.Divide(cols, rows)
    return c
