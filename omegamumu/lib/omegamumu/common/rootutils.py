import os

from .utils import is_collection


def book_df(treename, files):
    import ROOT

    ROOT.ROOT.EnableImplicitMT()
    df = ROOT.RDataFrame(treename, files)
    ROOT.RDF.Experimental.AddProgressBar(df)
    return df


def book_dask_df(treename, files, workers=2, threads=1, memory_limit="4GiB"):
    import ROOT
    from dask.distributed import LocalCluster, Client

    RDataFrame = ROOT.RDF.Experimental.Distributed.Dask.RDataFrame
    cluster = LocalCluster(
        n_workers=workers,
        threads_per_worker=threads,
        processes=True,
        memory_limit=memory_limit,
    )
    client = Client(cluster)
    df = RDataFrame(treename, files, daskclient=client)
    return df, client
    # ROOT.ROOT.EnableImplicitMT()


def write_df(df, treename, outfile, columns=None):
    if columns is not None:
        df.Snapshot(treename, outfile, columns)
        return
    df.Snapshot(treename, outfile)


def write_cuts_report(report, outfile):
    cut_flow = {}
    allEntries = report.begin().GetAll()
    allcuts = []
    for v in report.GetValue():
        allcuts.append([v.GetName(), v.GetPass(), v.GetAll(), v.GetEff()])
    passedEntries = allcuts[-1][1]
    cut_flow["Final"] = [passedEntries, allEntries, float(passedEntries / allEntries)]
    cut_flow["AllCuts"] = allcuts

    import json

    if not outfile.endswith(".json"):
        outfile += ".json"
    with open(outfile, "w") as f:
        json.dump(cut_flow, f, indent=4)
    print("Cuts report written to:", outfile)


def book_histogram(df, h):
    """
    Books histograms for a given RDataFrame
    df: RDataFrame
    h: histogram definition as a namedtuple
           [h.name, h.title, h.bins, h.low, h.high, h.val]
    """
    if not is_collection(h.val):
        return df.Histo1D((h.name, h.title, h.bins, h.low, h.high), h.val)
    else:
        ndim = len(h.val)
        assert (
            ndim == len(h.bins) == len(h.low) == len(h.high)
        ), "All histogram parameters must have the same length"
        if ndim == 1:
            return df.Histo1D(
                (h.name, h.title, h.bins[0], h.low[0], h.high[0]), h.val[0]
            )
        elif ndim == 2:
            return df.Histo2D(
                (
                    h.name,
                    h.title,
                    h.bins[0],
                    h.low[0],
                    h.high[0],
                    h.bins[1],
                    h.low[1],
                    h.high[1],
                ),
                h.val[0],
                h.val[1],
            )
        else:
            raise ValueError("Histograms with more than 2 dimensions are not supported")
