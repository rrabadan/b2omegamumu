import numpy as np
import vector


def vect_pxpypzpe(arr, prefix):
    """
    Create a vector array from the given array and prefix.

    Parameters:
    arr (dict): A dictionary containing the components of the vectors.
    prefix (str): The prefix used to identify the components in the dictionary.

    Returns:
    vector.array: A vector array with keys 'px', 'py', 'pz', and 'E' corresponding to the components of the vectors.
    """
    return vector.array(
        {
            "px": arr[prefix + "_PX"],
            "py": arr[prefix + "_PY"],
            "pz": arr[prefix + "_PZ"],
            "E": arr[prefix + "_PE"],
        }
    )


def vect_pxpypzm(arr, prefix, mass):
    """
    Constructs a vector array with momentum components and mass.

    Parameters:
    arr (dict or DataFrame): A dictionary or DataFrame containing the momentum components.
    prefix (str): The prefix used in the keys of the dictionary or DataFrame to identify the momentum components.
    mass (float): The mass to be assigned to each vector.

    Returns:
    vector.array: An array of vectors with the specified momentum components and mass.
    """
    return vector.array(
        {
            "px": arr[prefix + "_PX"],
            "py": arr[prefix + "_PY"],
            "pz": arr[prefix + "_PZ"],
            "M": np.full_like(arr[prefix + "_PZ"], mass),
        }
    )


def eta(px, py, pz):
    """
    Auxiliary function to compute the pseudorapidity (eta) from momentum components.

    Parameters:
    px (float): Momentum component in the x direction
    py (float): Momentum component in the y direction
    pz (float): Momentum component in the z direction

    Returns:
    float: The pseudorapidity (eta)
    """
    p = np.sqrt(px**2 + py**2 + pz**2)
    theta = np.arccos(pz / p)
    eta = -np.log(np.tan(theta / 2))
    return eta
