import os

from collections.abc import Collection


def is_collection(arg):
    """
    Check if the argument is a collection.
    Parameters:
    - arg: The argument to be checked.
    Returns:
    - bool: True if the argument is a collection False otherwise.
    """
    return isinstance(arg, Collection) and not isinstance(arg, (str, bytes))


def get_keys(nested_dict, key_path=""):
    for key, value in nested_dict.items():
        new_key_path = f"{key_path}_{key}" if key_path else key
        if isinstance(value, dict):
            yield from get_keys(value, new_key_path)
        else:
            yield new_key_path


def get_domain(hostname):
    parts = hostname.split(".")
    if len(parts) > 1:
        return ".".join(parts[1:])
    else:
        return None


def list_dirs(path):
    subdirs = [d for d in os.listdir(path) if os.path.isdir(os.path.join(path, d))]
    return subdirs


def dir_exists(path):
    return os.path.exists(path) or not os.path.isdir(path)


def file_exists(file):
    return os.path.exists(file)


def check_parent_dir(path):
    parent = os.path.dirname(path)
    if not os.path.exists(parent):
        os.makedirs(parent)
        print("Created directory:", parent)


def check_file_to_write(file, overwrite=True):
    if file_exists(file):
        if overwrite:
            print("Overwritting:", file)
            os.remove(file)
            return True
        else:
            print("File:", file, "already exists. Use --overwrite to overwrite it.")
            return False
    check_parent_dir(file)
    return True


class KeyValMap:
    def __init__(self, data=None):
        self.key_to_value = {}
        self.value_to_key = {}
        if data:
            if isinstance(data, dict):
                for key, value in data.items():
                    self.insert(key, value)
            elif isinstance(data, list):
                for key, value in data:
                    self.insert(key, value)

    def insert(self, key, value):
        if key in self.key_to_value or value in self.value_to_key:
            raise Exception("This key or value already exists in the map.")
        self.key_to_value[key] = value
        self.value_to_key[value] = key

    def get_key(self, value):
        return self.value_to_key[value]

    def get_value(self, key):
        return self.key_to_value[key]

    def get_pair(self, param):
        if param in self.key_to_value:
            return (param, self.get_value(param))
        elif param in self.value_to_key:
            return (self.get_key(param), param)
        else:
            raise Exception("This key or value does not exist in the map.")
