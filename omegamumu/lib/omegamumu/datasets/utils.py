from collections.abc import Collection


def is_collection(arg):
    """
    Check if the argument is a collection.
    Parameters:
    - arg: The argument to be checked.
    Returns:
    - bool: True if the argument is a collection False otherwise.
    """
    return isinstance(arg, Collection) and not isinstance(arg, (str, bytes))


def get_keys(nested_dict, key_path=""):
    for key, value in nested_dict.items():
        new_key_path = f"{key_path}_{key}" if key_path else key
        if isinstance(value, dict):
            yield from get_keys(value, new_key_path)
        else:
            yield new_key_path


def get_domain(hostname):
    parts = hostname.split(".")
    if len(parts) > 1:
        return ".".join(parts[1:])
    else:
        return None
