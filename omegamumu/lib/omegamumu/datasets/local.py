import os
import json
import re

from .config import get_local_data_main_path, get_private_ntuples_jsonfile
from ..common.utils import get_keys


def get_datasets_from_json():
    """Format of json file:
    {
        "eventtype": {
            "polarity_datatype": [
                "file1",
                "file2",
                 ...
            ]
        }
    }
    """
    jsonfile = get_private_ntuples_jsonfile()
    if jsonfile is None:
        raise ValueError("JSON file not specified in configuration file.")
    with open(jsonfile, "r") as f:
        data = json.load(f)
    # return get_keys(data)
    for ds in get_keys(data):
        d = tuple(ds.split("_"))
        yield (d[0], d[1], d[2], data[d[0]][f"{d[1]}_{d[2]}"])


def get_files_from_json(eventtype, datatype, polarity):
    """Format of json file:
    {
        "eventtype": {
            "polarity_datatype": [
                "file1",
                "file2",
                 ...
            ]
        }
    }
    """
    jsonfile = get_private_ntuples_jsonfile()
    if jsonfile is None:
        raise ValueError("JSON file not specified in configuration file.")
    with open(jsonfile, "r") as f:
        files = json.load(f)[f"{eventtype}"][f"{polarity}_{datatype}"]
    return files


def get_local_data():
    datadir = get_local_data_main_path()
    datadir = datadir.rstrip(os.sep)
    start = len(datadir) + 1
    ds_dict = {}
    for path, dirs, files in os.walk(datadir):
        folders = path[start:].split(os.sep)
        if folders[-1] == "":
            continue
        subdir_dict = ds_dict
        for folder in folders[:-1]:
            if folder:
                subdir_dict = subdir_dict[folder]
        matching_files = [file for file in files if re.match(r"\w+_\d+\.root", file)]
        if len(matching_files) > 0:
            subdir_dict[folders[-1]] = matching_files
        else:
            subdir_dict[folders[-1]] = {}
    return ds_dict


def get_local_data_path(decay, eventtype, group="preselection"):
    return os.path.join(get_local_data_main_path(), decay, f"{eventtype}", group)


def get_local_data_file(decay, eventtype, datatype, polarity, group="preselection"):
    polarity = polarity.lower()
    return os.path.join(
        get_local_data_path(decay, eventtype, group), f"{polarity}_{datatype}.root"
    )
