import os
import toml
import socket

from ..common.utils import get_domain


class Config:
    def __init__(
        self,
        config_file_path=os.path.join(
            os.path.dirname(
                os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
            ),
            "pyproject.toml",
        ),
    ):
        self.config = toml.load(config_file_path)

    def get(self, *keys, default=None):
        """
        Retrieve a value from the configuration using a sequence of keys.
        """
        value = self.config
        for key in keys:
            value = value.get(key, default)
            if value is default:
                break
        return value


# Create an instance of Config
config_instance = Config()


def use_apdinput():
    return config_instance.get("tool", "analysis", "datasources", "apd-input")


def get_apd_analysis_info():
    """
    Get the APD analysis data for a given working group and analysis identifier.
    - wg (str): Working group identifier (default is "rd").
    - anaid (str): Analysis identifier (default is "b2omegamumu").
    """
    wg = config_instance.get("tool", "analysis", "datasources", "apd-wg")
    anaid = config_instance.get("tool", "analysis", "datasources", "apd-anaid")
    dataver = config_instance.get("tool", "analysis", "datasources", "apd-version-data")
    simver = config_instance.get("tool", "analysis", "datasources", "apd-version-mc")
    return wg, anaid, dataver, simver


def get_private_ntuples_jsonfile():
    return config_instance.get("tool", "analysis", "datasources", "privatentuples")


def get_local_data_main_path():
    return config_instance.get(
        "tool", "analysis", "localstorage", get_domain(socket.gethostname())
    )


def get_datatypes():
    run1 = config_instance.get("tool", "analysis", "datatypes", "run1")
    run2 = config_instance.get("tool", "analysis", "datatypes", "run2")
    if config_instance.get("tool", "analysis", "datatypes", "userun1only"):
        return run1
    elif config_instance.get("tool", "analysis", "datatypes", "userun2only"):
        return run2
    else:
        return run1 + run2


def get_stripping_ntuple_tree_name(decay):
    return config_instance.get("tool", "analysis", "strippingtuples", decay)


def get_eventtypes():
    return config_instance.get("tool", "analysis", "mctypes")


def get_eventtype_name(eventtype):
    return config_instance.get("tool", "analysis", "mctypes", eventtype)
