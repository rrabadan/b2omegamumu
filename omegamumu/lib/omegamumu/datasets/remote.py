import apd
from arrow import get
import order as od

from itertools import product

from ..common.utils import is_collection

from .config import get_apd_analysis_info, get_datatypes, get_eventtype_name


def apd_data():
    wg, anaid, _, _ = get_apd_analysis_info()
    ana_data = apd.get_analysis_data(wg, anaid)
    samples = sorted(ana_data.summary()["tags"]["name"])
    datatypes = get_datatypes()
    for sample in samples:
        s_info = sample.split("_")
        if len(s_info) == 3:
            eventtype = "90000000"
            decay, datatype, polarity = s_info
        elif len(s_info) == 4:
            decay = "b2omegamumu,bplus2kstplusmumu,bplus2rhoplusmumu"
            eventtype, datatype, polarity = s_info[1:]
            polarity = polarity.split(",")[0]
        else:
            raise ValueError("Invalid sample name.")
        if datatype not in datatypes:
            continue
        yield (eventtype, datatype, polarity, decay)


def apd_dataset_files(
    ana_data, event_type, data_type, polarity, decay="all", version=None
):
    """
    Get APD files for a given event type, data type, and polarity.

    Parameters:
    - data: APD analysis data.
    - eventtype (int): The event type.
    - datatype (int or list of int): The data type(s) for the analysis.
    - polarity (str or list of str): The magnet polarity (e.g., 'magdown' or 'magup').
    - version (str): The version of the APD data to use.

    Returns:
    - list: A list of APD files for the specified parameters.
    """
    files = []
    if not is_collection(data_type):
        data_type = [data_type]
    if not is_collection(polarity):
        polarity = [polarity]
    for dt, pol in product(data_type, polarity):
        files += ana_data(
            eventtype=event_type,
            datatype=dt,
            polarity=pol,
            decay=decay,
            version=version,
        )
    return files


def apd_dataset(event_type, data_type, polarity, decay="all"):
    """
    Get APD datasets for a given event type, data type, and polarity.

    Parameters:
    - eventtype (int): The event type.
    - datatype (int or list of int): The data type(s) for the analysis.
    - polarity (str or list of str): The magnet polarity (e.g., 'magdown' or 'magup').
    - decay (str): The decay mode (default is 'all').

    Returns:
    - list: A list of APD datasets for the specified parameters.
    """
    wg, anaid, dataver, simver = get_apd_analysis_info()
    ana_data = apd.get_analysis_data(wg, anaid)

    if int(event_type) == 90000000:
        assert decay != "all", "Decay mode must be specified for event type 90000000."
        name = "_".join([decay, data_type, polarity])
        files = apd_dataset_files(
            ana_data, event_type, data_type, polarity, decay=decay, version=dataver
        )
        dataset = od.Dataset(name, id=0, keys=files, n_files=len(files))
        return dataset

    evttype_name = get_eventtype_name(event_type)
    name = "_".join([evttype_name, data_type, polarity])
    files = apd_dataset_files(
        ana_data, event_type, data_type, polarity, decay="all", version=simver
    )
    return od.Dataset(name, id=0, keys=files, n_files=len(files))


# def apd_dataset_files(event_type, data_type, polarity, decay="all"):
#    dataset = apd_dataset(event_type, data_type, polarity, decay)
#    return dataset.keys
