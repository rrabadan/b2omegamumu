import os
from catboost import CatBoostClassifier, Pool
from catboost import EShapCalcType, EFeaturesSelectionAlgorithm


def init_model(params):
    """
    Initialize the CatBoost model with the given parameters.

    Parameters:
    - params (dict): Dictionary of CatBoost parameters.

    Returns:
    - model (CatBoostClassifier): Initialized CatBoost model.
    """
    return CatBoostClassifier(**params)


def train_model(
    model,
    X_train,
    y_train,
    X_val=None,
    y_val=None,
    weights=None,
    verbose=100,
    plot=True,
):
    """
    Train the CatBoost model on the training dataset.

    Parameters:
    - model (CatBoostClassifier): The CatBoost model to train.
    - X_train (array-like): Feature matrix for training.
    - y_train (array-like): Target vector for training.
    - X_val (array-like, optional): Feature matrix for validation. Default is None.
    - y_val (array-like, optional): Target vector for validation. Default is None.
    - weights (array-like, optional): Sample weights. Default is None.
    - verbose (int, optional): Verbosity level. Default is 100.

    Returns:
    - model (CatBoostClassifier): Trained CatBoost model.
    """
    train_pool = Pool(data=X_train, label=y_train, weight=weights)
    eval_set = (
        Pool(data=X_val, label=y_val)
        if X_val is not None and y_val is not None
        else None
    )

    model.fit(train_pool, eval_set=eval_set, verbose=verbose, plot=plot)
    return model


def select_features(
    train_pool: Pool,
    val_pool: Pool,
    params: dict,
    features_names: list,
    num_features: int,
    steps: int = 1,
    algorithm: EFeaturesSelectionAlgorithm = EFeaturesSelectionAlgorithm.RecursiveByShapValues,
    train_final_model: bool = True,
):
    """
    Selects the optimal features for a machine learning model using the CatBoostClassifier algorithm.
    Args:
        train_pool (Pool): The training dataset.
        val_pool (Pool): The validation dataset.
        params (dict): The parameters for the CatBoostClassifier model.
        algorithm (EFeaturesSelectionAlgorithm): The algorithm to use for feature selection.
        features_names (list): The list of feature names.
        num_features (int): The number of features to select.
        steps (int, optional): The number of steps for the feature selection algorithm. Defaults to 1.
        train_final_model (bool, optional): Whether to train the final model after feature selection. Defaults to True.
    Returns:
        tuple: A tuple containing the summary of the feature selection process and the trained model.
    """

    model = CatBoostClassifier(**params)

    summary = model.select_features(
        train_pool,
        eval_set=val_pool,
        features_for_select=features_names,
        num_features_to_select=num_features,
        steps=steps,
        algorithm=algorithm,
        shap_calc_type=EShapCalcType.Regular,
        train_final_model=train_final_model,
        logging_level="Silent",
        plot=True,
    )
    print("Selected features:\n", summary["selected_features_names"])
    return summary, model


def save_model(model, path, train_pool=None):
    """
    Save the trained model to disk.

    Parameters:
    - model (CatBoostClassifier): The trained CatBoost model.
    - model_path (str): Path to save the model.

    Returns:
    - None
    """
    model.save_model(path, format="json", pool=train_pool)


def load_model(path, format="json"):
    """
    Load a trained model from disk.

    Parameters:
    - model_path (str): Path to the saved model.

    Returns:
    - model (CatBoostClassifier): Loaded CatBoost model.
    """
    model = CatBoostClassifier()
    model.load_model(path, format=format)
    return model
