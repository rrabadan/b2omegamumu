# __init__.py

from .preprocessing import (
    test_set_check,
    split_train_test_by_id,
    prepare_training_data,
    ColumnSelector,
    ColumnTransformer,
)

from .training import init_model, train_model, save_model, load_model, select_features

from .evaluation import (
    model_eval_cv,
    calculate_metrics,
    plot_learning_curve,
    plot_train_test_response,
    plot_roc_auc,
    plot_roc_auc_train_test,
    plot_loss_by_eliminated_features,
    compute_shap_values,
    plot_shap_summary,
)

from .utils import plot_correlations, plot_sig_bkg_comparison

__all__ = [
    "test_set_check",
    "split_train_test_by_id",
    "prepare_training_data",
    "ColumnSelector",
    "ColumnTransformer",
    "init_model",
    "train_model",
    "save_model",
    "load_model",
    "select_features",
    "model_eval_cv",
    "calculate_metrics",
    "plot_learning_curve",
    "plot_train_test_response",
    "plot_roc_auc",
    "plot_roc_auc_train_test",
    "plot_loss_by_eliminated_features",
    "compute_shap_values",
    "plot_shap_summary",
    "plot_correlations",
    "plot_sig_bkg_comparison",
]
