import numpy as np
import matplotlib.pyplot as plt

from ..common.histograms import multi_hist1d_comparison


def plot_correlations(
    df, columns=None, transform=None, xlabels=None, fig_size=(5, 5), **kwargs
):
    """
    Plot a correlation matrix heatmap for the given DataFrame.
    Parameters:
    - df (pandas.DataFrame): The DataFrame containing the data.
    - columns (list, optional): The columns to include in the correlation matrix. If None, all columns are included. Default is None.
    - transform (callable, optional): A function to transform the data before calculating the correlation matrix. Default is None.
    - xlabels (list, optional): The labels for the x-axis. If None, the column names are used. Default is None.
    - fig_size (tuple, optional): The size of the figure. Default is (5, 5).
    - **kwargs: Additional keyword arguments to be passed to the correlation calculation.
    Returns:
    - None
    """

    data = df.copy()
    if columns is not None:
        data = data[columns]

    if transform is not None:
        data = data.transform(transform)

    corrmat = data.corr(**kwargs)
    fig, ax1 = plt.subplots(ncols=1, figsize=fig_size)
    opts = {"cmap": plt.get_cmap("RdBu"), "vmin": -1, "vmax": +1}
    heatmap1 = ax1.pcolor(corrmat, **opts)
    plt.colorbar(heatmap1, ax=ax1)
    ax1.set_title("Correlations")

    if xlabels is None:
        xlabels = corrmat.columns.values
    for ax in (ax1,):
        # shift location of ticks to center of the bins
        ax.set_xticks(np.arange(len(xlabels)) + 0.5, minor=False)
        ax.set_yticks(np.arange(len(xlabels)) + 0.5, minor=False)
        ax.set_xticklabels(xlabels, minor=False, ha="right", rotation=90)
        ax.set_yticklabels(xlabels, minor=False)
        # remove gridlines
        ax.grid(False)

    # save_fig(fig_id)
    return None


def plot_sig_bkg_comparison(
    hist_sig,
    hist_bkg,
    legends=["SIG", "BKG"],
    histtypes=["fill", "fill"],
    colors=["r", "b"],
    plot_height=2.5,
    plot_width=2.5,
    alpha=0.5,
):
    # markers = ['o', 's']
    multi_hist1d_comparison(
        (hist_sig, hist_bkg),
        legends,
        histtypes,
        colors,
        plot_height=plot_height,
        plot_width=plot_width,
        alpha=alpha,
    )
