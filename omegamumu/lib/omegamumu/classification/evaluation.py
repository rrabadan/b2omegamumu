import numpy as np
import matplotlib.pyplot as plt

from sklearn.metrics import (
    accuracy_score,
    precision_score,
    recall_score,
    f1_score,
    roc_auc_score,
    roc_curve,
)
from sklearn.model_selection import cross_val_score, StratifiedKFold

import shap
import catboost
from catboost import Pool


def calculate_metrics(y_true, y_pred, y_prob=None):
    """
    Calculate various evaluation metrics for binary classification.
    Parameters:
    - y_true (array-like): True labels.
    - y_pred (array-like): Predicted labels.
    - y_prob (array-like, optional): Predicted probabilities for positive class.
    Returns:
    - metrics (dict): Dictionary containing the calculated metrics:
        - accuracy (float): Accuracy score.
        - precision (float): Precision score.
        - recall (float): Recall score.
        - f1_score (float): F1 score.
        - roc_auc (float, optional): ROC AUC score (only if y_prob is provided).
    """
    metrics = {
        "accuracy": accuracy_score(y_true, y_pred),
        "precision": precision_score(y_true, y_pred),
        "recall": recall_score(y_true, y_pred),
        "f1_score": f1_score(y_true, y_pred),
    }

    if y_prob is not None:
        metrics["roc_auc"] = roc_auc_score(y_true, y_prob)

    return metrics


def cross_validate_model(model, X, y, cv=5, scoring="accuracy"):
    """
    Cross-validates a machine learning model using the specified number of folds and scoring metric.

    Parameters:
        model (object): The machine learning model to be cross-validated.
        X (array-like): The input features.
        y (array-like): The target variable.
        cv (int, optional): The number of folds for cross-validation. Default is 5.
        scoring (str, optional): The scoring metric to evaluate the model performance. Default is 'accuracy'.

    Returns:
        float: The mean score of the model across all folds.
    """
    scores = cross_val_score(model, X, y, cv=cv, scoring=scoring)
    return np.mean(scores)


def stratified_k_fold_cv(model, X, y, n_splits=5, scoring="accuracy"):
    """
    Perform stratified k-fold cross-validation on a given model.

    Parameters:
    - model: The machine learning model to be evaluated.
    - X: The feature matrix.
    - y: The target variable.
    - n_splits: The number of folds. Default is 5.
    - scoring: The scoring metric to be used. Default is 'accuracy'.

    Returns:
    - The mean score of the cross-validation.
    """
    skf = StratifiedKFold(n_splits=n_splits)
    scores = cross_val_score(model, X, y, cv=skf, scoring=scoring)
    return np.mean(scores)


def model_eval_cv(
    pool: Pool,
    params: dict,
    nfolds: int = 1,
    plot: bool = True,
    stratified: bool = False,
):
    """
    Perform cross-validation for evaluating a CatBoost model.
    Args:
        pool (Pool): The training dataset.
        params (dict): The parameters for the CatBoost model.
        nfolds (int, optional): The number of folds for cross-validation. Defaults to 1.
        plot (bool, optional): Whether to plot the cross-validation results. Defaults to True.
        stratified (bool, optional): Whether to use stratified sampling for cross-validation. Defaults to False.
    Returns:
        dict: The cross-validation results.
    """

    cv_catb = catboost.cv(
        params=params,
        pool=pool,
        fold_count=nfolds,
        shuffle=True,
        partition_random_seed=0,
        plot=plot,
        stratified=stratified,
        verbose=False,
    )

    return cv_catb


def plot_learning_curve(model, metric="Logloss", color="green"):
    """
    Plots the learning curve for a given model.
    Parameters:
    - model: The trained model object.
    - metric: The evaluation metric to plot. Valid options are 'Logloss', 'AUC', and 'Accuracy'. Default is 'Logloss'.
    - color: The color of the plotted lines and points. Default is 'green'.
    Returns:
    None
    """

    assert metric in ["Logloss", "AUC", "Accuracy"], "Invalid metric"

    val_metrics = model.get_evals_result()["validation"]
    n = len(val_metrics[metric])
    plt.plot(range(n), val_metrics[metric], label="Validation", c=color)

    if metric != "AUC":
        train_metrics = model.get_evals_result()["learn"]
        plt.plot(
            range(n), train_metrics[metric], label="Training", c=color, linestyle="--"
        )

    # Plot best iteration point
    best_iter = np.argmin(val_metrics["Logloss"])
    plt.scatter(best_iter, val_metrics[metric][best_iter], c=color)

    plt.xlabel("Iteration")
    plt.ylabel(metric)
    plt.legend()


def plot_train_test_response(
    clf, X_train, y_train, X_test, y_test, bins=30, log_y=True
):
    """
    Compare the classifier response on training and testing data.
    Parameters:
    - clf: The classifier object.
    - X_train: The training data features.
    - y_train: The training data labels.
    - X_test: The testing data features.
    - y_test: The testing data labels.
    - bins: The number of bins for the histogram (default: 30).
    Returns:
    None
    """
    decisions = []
    for X, y in ((X_train, y_train), (X_test, y_test)):
        d1 = clf.predict_proba(X[y > 0.5])[:, 1].ravel()
        d2 = clf.predict_proba(X[y < 0.5])[:, 1].ravel()
        decisions += [d1, d2]

    low = min(np.min(d) for d in decisions)
    high = max(np.max(d) for d in decisions)
    low_high = (low, high)

    plt.figure()
    plt.hist(
        decisions[0],
        color="r",
        alpha=0.5,
        range=low_high,
        bins=bins,
        histtype="stepfilled",
        density=True,
        label="Signal (train)",
    )
    plt.hist(
        decisions[1],
        color="b",
        alpha=0.5,
        range=low_high,
        bins=bins,
        histtype="stepfilled",
        density=True,
        label="Background (train)",
    )

    hist, bins = np.histogram(decisions[2], bins=bins, range=low_high, density=True)
    scale = len(decisions[2]) / sum(hist)
    err = np.sqrt(hist * scale) / scale

    center = (bins[:-1] + bins[1:]) / 2
    plt.errorbar(center, hist, yerr=err, fmt="o", c="r", label="Signal (test)")

    hist, bins = np.histogram(decisions[3], bins=bins, range=low_high, density=True)
    scale = len(decisions[2]) / sum(hist)
    err = np.sqrt(hist * scale) / scale

    plt.errorbar(center, hist, yerr=err, fmt="o", c="b", label="Background (test)")

    plt.xlabel("BDT output")
    plt.ylabel("Arbitrary units")
    plt.legend(loc="best")
    plt.grid()

    # show in log y axis
    if log_y:
        plt.yscale("log")


def plot_roc_auc(tpr, fpr, roc_auc):
    """
    Plots the Receiver Operating Characteristic (ROC) curve and the Area Under the Curve (AUC) for binary classification.

    Parameters:
    - tpr (array-like): True Positive Rate values.
    - fpr (array-like): False Positive Rate values.
    - roc_auc (float): Area Under the Curve (AUC) value.

    Returns:
    - None

    """
    plt.figure()
    plt.plot(tpr, fpr, lw=1, label="ROC (area = %0.5f)" % (roc_auc))
    plt.xlim([-0.05, 1.05])
    plt.ylim([-0.05, 1.05])
    plt.xlabel("Signal efficiency")
    plt.ylabel("Background rejection")
    plt.title("Background rejection vs signal efficiency")
    plt.legend(loc="best")
    plt.grid()


def plot_roc_auc_train_test(tpr, fpr, roc_auc):
    plt.figure()
    plt.plot(
        tpr[0],
        fpr[0],
        lw=1,
        label="Train ROC (area = %0.5f)" % (roc_auc[0]),
        linestyle="-",
    )
    plt.plot(
        tpr[0],
        fpr[0],
        lw=1,
        label="Test ROC (area = %0.5f)" % (roc_auc[1]),
        linestyle="--",
    )
    plt.xlim([-0.05, 1.05])
    plt.ylim([-0.05, 1.05])
    plt.xlabel("Signal efficiency")
    plt.ylabel("Background rejection")
    plt.title("Background rejection vs signal efficiency")
    plt.legend(loc="best")
    plt.grid()


def plot_loss_by_eliminated_features(
    summary: dict, show_names: bool = False, figsize: tuple = (10, 6)
):
    """
    Plots the loss value against the count of removed features.
    Parameters:
    - summary (dict): A dictionary containing the summary information.
    - show_names (bool): Flag indicating whether to show feature names on the plot. Default is False.
    - figsize (tuple): A tuple specifying the figure size. Default is (10, 6).
    Returns:
    None
    """

    loss_graph = summary["loss_graph"]

    loss_values = loss_graph["loss_values"]
    removed_features_count = loss_graph["removed_features_count"]

    # Create the plot
    plt.figure(figsize=figsize)
    plt.scatter(removed_features_count, loss_values, color="blue")
    plt.plot(removed_features_count, loss_values, color="blue", linestyle="--")
    plt.xlabel("Removed Features Count")
    plt.ylabel("Loss Value")
    plt.title("Loss Value vs Removed Features Count")

    if show_names:
        feature_names = summary["eliminated_features_names"]

        # Annotate each point with the feature name
        for i, feature_name in enumerate(feature_names):
            plt.annotate(
                feature_name,
                (removed_features_count[i + 1], loss_values[i + 1]),
                textcoords="offset points",
                xytext=(0, 8),
                ha="center",
                fontsize=8,
            )


def compute_shap_values(model, pool):
    """
    Compute SHAP values for the given model and dataset.
    Parameters:
    - model: The trained model.
    - pool: The dataset.
    Returns:
    - shap_values: The SHAP values.
    """
    shap_values = model.get_feature_importance(pool, type="ShapValues")
    return shap_values[:, :-1]


def plot_shap_summary(shap_values, X):
    """
    Plots the summary of SHAP values.

    Parameters:
    - shap_values (numpy.ndarray): The SHAP values.
    - X (numpy.ndarray): The input data.

    Returns:
    None
    """
    shap.summary_plot(shap_values, X, show=False)


def plot_feature_importance(model, feature_names, max_num_features=20):
    """
    Plot the feature importance of a model.

    Parameters:
    - model: The trained model.
    - feature_names: List of feature names.
    - max_num_features: Maximum number of features to display (default: 20).

    Returns:
    - None
    """
    feature_importances = model.feature_importances_
    indices = np.argsort(feature_importances)[-max_num_features:]
    plt.figure(figsize=(10, 6))
    plt.title("Feature Importances")
    plt.barh(
        range(len(indices)), feature_importances[indices], color="b", align="center"
    )
    plt.yticks(range(len(indices)), [feature_names[i] for i in indices])
    plt.xlabel("Relative Importance")
