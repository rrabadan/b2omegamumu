import numpy as np
import pandas as pd
from zlib import crc32

from sklearn.base import BaseEstimator, TransformerMixin
from sklearn import set_config

set_config(transform_output="pandas")


def test_set_check(
    row, test_ratio, col1="runNumber", col2="eventNumber", col3="candNumber"
):
    """
    Check if a row belongs to the test set based on a given test ratio.
    (Inspired by: Hands-On Machine Learning with Scikit-Learn, Keras, and TensorFlow, 2nd Edition)

    Parameters:
    - row: pandas.Series
        The row containing the data.
    - test_ratio: float
        The ratio of the test set size to the total dataset size.
    - col1: str
        The name of the first column.
    - col2: str
        The name of the second column.
    - col3: str
        The name of the third column.

    Returns:
    - bool
        True if the row belongs to the test set, False otherwise.
    """
    # Concatenate the three columns to form a unique identifier
    identifier = f"{row[col1]}_{row[col2]}_{row[col3]}"
    # Convert the concatenated string to bytes
    identifier_bytes = identifier.encode("utf-8")
    # Compute the checksum
    checksum = crc32(identifier_bytes) & 0xFFFFFFFF
    # Check if the checksum falls within the test ratio
    return checksum < test_ratio * 2**32


def split_train_test_by_id(
    data, data_ids, test_ratio, col1="runNumber", col2="eventNumber", col3="candNumber"
):
    """
    Splits the given data into training and testing sets based on the provided test ratio.
    (Taken From: Hands-On Machine Learning with Scikit-Learn, Keras, and TensorFlow, 2nd Edition)

    Parameters:
    - data: pd.DataFrame
        The input data to be split.
    - test_ratio: The ratio of data to be allocated for testing.
    - col1: The name of the column representing the run number. Default is 'runNumber'.
    - col2: The name of the column representing the event number. Default is 'eventNumber'.
    - col3: The name of the column representing the candidate number. Default is 'candNumber'.

    Returns:
    - train_set: The training set containing the data allocated for training.
    - test_set: The testing set containing the data allocated for testing.
    """
    in_test_set = data_ids.apply(
        lambda row: test_set_check(row, test_ratio, col1, col2, col3), axis=1
    )
    return data.loc[~in_test_set], data.loc[in_test_set]


def prepare_training_data(
    sig_df, bkg_df, sig_inputvars, bkg_inputvars, sig_weights=None, bkg_weights=None
):
    """
    Prepare the data for training by transforming the input variables and adding necessary columns.
    Args:
        sig_df (pandas.DataFrame): DataFrame containing the signal data.
        bkg_df (pandas.DataFrame): DataFrame containing the background data.
        sig_inputvars (list): List of input variables for the signal data.
        bkg_inputvars (list): List of input variables for the background data.
        sig_weights (array-like, optional): Array-like object containing the weights for the signal data. Defaults to None.
        bkg_weights (array-like, optional): Array-like object containing the weights for the background data. Defaults to None.
    Returns:
        tuple: A tuple containing two DataFrames. The first DataFrame contains the transformed input variables and the label column. The second DataFrame contains the id columns.
    """

    def features_and_transforms(df, inputvars):
        features = [var.name for var in inputvars]
        transform = [var.expression for var in inputvars]
        branches = [
            var.branch if isinstance(var.branch, list) else [var.branch]
            for var in inputvars
        ]

        arrs = []
        for br, tr in zip(branches, transform):
            arrs.append(tr(*(df[b] for b in br)))

        df = pd.DataFrame.from_dict(
            {features[i]: arrs[i] for i in range(len(features))}
        )

        return df

    idcolumns = ["runNumber", "eventNumber", "candNumber"]

    sigdf = features_and_transforms(sig_df, sig_inputvars)
    bkgdf = features_and_transforms(bkg_df, bkg_inputvars)

    ## Add columns runNumber, eventNumber, candNumber
    for col in idcolumns:
        sigdf.loc[:, col] = sig_df[col]
        bkgdf.loc[:, col] = bkg_df[col]

    if sig_weights is not None or bkg_weights is not None:
        sigdf.loc[:, "weights"] = (
            sig_weights if sig_weights is not None else np.ones(len(sig_df))
        )
        bkgdf.loc[:, "weights"] = (
            bkg_weights if bkg_weights is not None else np.ones(len(bkg_df))
        )

    sig = sigdf.copy()
    bkg = bkgdf.copy()
    sig.reset_index(drop=True, inplace=True)
    bkg.reset_index(drop=True, inplace=True)
    sig["label"] = pd.Series(np.ones(len(sigdf)).astype(int))
    bkg["label"] = pd.Series(np.zeros(len(bkgdf)).astype(int))
    sample = pd.concat([sig, bkg], ignore_index=True)

    sample.dropna(inplace=True)
    return sample.drop(idcolumns, axis=1), sample[idcolumns]


class ColumnSelector(BaseEstimator, TransformerMixin):
    def __init__(self, col_names, weights="weights"):
        self.columns = [
            var.branch if isinstance(var.branch, list) else [var.branch] for var in vars
        ]

    def fit(self, X, y=None):
        return self

    def transform(self, X):

        return X[self.columns]


class ColumnTransformer(BaseEstimator, TransformerMixin):
    def __init__(self, vars, id_cols=["runNumber", "eventNumber", "candNumber"]):
        self.features = [var.name for var in vars]
        self.columns = [
            var.branch if isinstance(var.branch, list) else [var.branch] for var in vars
        ]
        self.transforms = [var.expression for var in vars]
        self.id_cols = id_cols

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        arrs = []
        for br, tr in zip(self.columns, self.transforms):
            arrs.append(tr(*(X[b] for b in br)))

        df = pd.DataFrame(
            {self.features[i]: arrs[i] for i in range(len(self.features))}
        )

        for col in self.id_cols:
            df.loc[:, col] = X[col]

        df.dropna(inplace=True)
        df.reset_index(drop=True, inplace=True)

        self.id_df = df[self.id_cols].copy()

        return df
