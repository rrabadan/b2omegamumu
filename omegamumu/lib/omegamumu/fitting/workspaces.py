import ROOT

from . import wsmodels as models


def pizero_workspace(name, title, m_range=(105, 165)):
    """
    Create a RooWorkspace for fitting a Pi0 mass spectrum.

    Parameters:
    - name (str): Name of the workspace.
    - title (str): Title of the workspace.
    - m_range (tuple): Mass range for the Pi0 fit (default is (105, 165)).

    Returns:
    - ROOT.RooWorkspace: The created RooWorkspace.
    """
    ws = ROOT.RooWorkspace(name, title)

    m_min, m_max = m_range

    pimass = ROOT.RooRealVar("pimass", "m(#gamma #gamma)(MeV/c^{2})", m_min, m_max)

    sigmodel = models.dscb(ws, pimass, "Pi0_sig", "Pi0_sig", prefix="Pi0_sig_")

    bkgexp = models.exp(ws, pimass, "Pi0_bkg_exp", "Pi0_bkg_exp", prefix="Pi0_bkg_exp_")
    bkgexpfrac = ROOT.RooRealVar("Pi0_bkg_exp_frac", "Pi0_bkg_exp_frac", 0.5, 0.0, 1.0)
    bkgmodel = ROOT.RooAddPdf("Pi0_bkg", "Pi0_bkg", [bkgexp, sigmodel], [bkgexpfrac])
    ws.Import(bkgmodel)

    nbkg = ROOT.RooRealVar("Pi0_nbkg", "Pi0_nbkg", 1000, 0, 1e6)
    nsig = ROOT.RooRealVar("Pi0_nsig", "Pi0_nsig", 1000, 0, 1e6)
    bkgmodel_ext = ROOT.RooAddPdf(
        "Pi0_bkg_ext", "Pi0_bkg_ext", [bkgexp, sigmodel], [nbkg, nsig]
    )
    ws.Import(bkgmodel_ext)

    sigparams = sigmodel.getParameters({pimass})
    bkgparams = bkgmodel.getParameters({pimass})
    ws.defineSet("sigparams", sigparams)
    ws.defineSet("bkgparams", bkgparams)
    return ws


def bpeak_reduced_workspace(name, title, **kwargs):
    """
    Create a RooWorkspace for b fit.
    Args:
        name (str): The name of the RooWorkspace.
        title (str): The title of the RooWorkspace.
        **kwargs: Additional keyword arguments.
            m_var (str or RooAbsReal): The mass variable or RooAbsReal object.
                Default is 'bmass'.
            m_title (str): The title of the mass variable. Default is 'm'.
            m_range (tuple): The range of the mass variable. Default is (5000, 5800).
    Returns:
        RooWorkspace: The created RooWorkspace.
    """

    ws = ROOT.RooWorkspace(name, title)

    # Retrieve parameters from kwargs or set default values
    m_var = kwargs.get("m_var", "bmass")
    m_title = kwargs.get("m_title", "m")
    m_range = kwargs.get("m_range", (5000, 5800))

    # Check if the mass variable is already a RooAbsReal
    if isinstance(m_var, ROOT.RooAbsReal):
        bmass = m_var
    else:
        m_min, m_max = m_range
        bmass = ROOT.RooRealVar(m_var, m_title + " (MeV/c^{2})", m_min, m_max)

    sigmodel = models.dscb(ws, bmass, "B_sig", "B_sig", prefix="B_sig_", calib=True)
    ws.Import(sigmodel)

    shift = ws["B_sig_shift"]
    shift.setVal(0.0)
    scale = ws["B_sig_scale"]
    scale.setVal(1.0)

    gbkgfrac = ROOT.RooRealVar("B_gbkg_frac", "B_gbkg_frac", 0.5, 0.0, 1.0)
    ##gbkg_model = models.ege(ws, bmass, 'B_gbkg', 'B_gbkg', prefix='B_gbkg_', calib=False)
    gbkgmodel = models.ege(
        ws,
        bmass,
        "B_gbkg",
        "B_gbkg",
        prefix="B_gbkg_",
        calib=True,
        shift=shift,
        scale=scale,
    )
    ##gbkg_model = models.ege_gauss(ws, mass, 'B_gbkg', 'B_gbkg', prefix='B_gbkg_', share_mean=True)
    # gbkgmodel = models.cbs_gauss(ws, bmass, 'B_gbkg', 'B_gbkg', prefix='B_gbkg_', calib=True, shift=shift, scale=scale, share_mean=True, share_sigma=True)
    bkgmodel = ROOT.RooAddPdf("B_bkg", "B_bkg", [gbkgmodel, sigmodel], [gbkgfrac])
    ws.Import(bkgmodel)

    # Signal + background model
    # Define fractions of signal and background
    N = ROOT.RooRealVar("B_peak_N", "B_peak_N", 1000, 0.0, 1.0e6)
    Fsig = ROOT.RooRealVar("B_peak_Fsig", "Fsig", 1000, 0.0, 1.0e6)
    Fbkg = ROOT.RooRealVar("B_peak_Fbkg", "Fbkg", 1000, 0.0, 1.0e6)
    S = ROOT.RooLinearVar(
        "B_peak_Nsig", "B_peak_Nsig", Fsig, N, ROOT.RooFit.RooConst(0.0)
    )
    B = ROOT.RooLinearVar(
        "B_peak_Nbkg", "B_peak_Nbkg", Fbkg, N, ROOT.RooFit.RooConst(0.0)
    )

    # ws.Import(S)
    # ws.Import(B)

    # model = RooAddPdf(name, title, [sigmod, bkgmod], [Fsig])
    model = ROOT.RooAddPdf("B_peak", "B_peak", [sigmodel, gbkgmodel], [S, B])
    ws.Import(model)

    sigparams = sigmodel.getParameters({bmass})
    gbkgparams = gbkgmodel.getParameters({bmass})
    bkgparams = bkgmodel.getParameters({bmass})
    peakparams = model.getParameters({bmass})

    ws.defineSet("sigparams", sigparams)
    ws.defineSet("gbkgparams", gbkgparams)
    ws.defineSet("bkgparams", bkgparams)
    ws.defineSet("peakparams", peakparams)

    components = [sigmodel, gbkgmodel]
    yields = [S, B]
    yield_frac = [Fsig, Fbkg]

    ws.defineSet("peak_yields", ROOT.RooArgList(*yields))
    ws.defineSet("peak_frac", ROOT.RooArgList(*yield_frac))
    ws.defineSet("peak_components", ROOT.RooArgList(*components))

    ws.Print()  # Important for some reason to save to file

    return ws


def bpeak_workspace(name, title, **kwargs):
    """
    Create a RooWorkspace for b fit.
    Args:
        name (str): The name of the RooWorkspace.
        title (str): The title of the RooWorkspace.
        **kwargs: Additional keyword arguments.
            m_var (str or RooAbsReal): The mass variable or RooAbsReal object.
                Default is 'bmass'.
            m_title (str): The title of the mass variable. Default is 'm'.
            m_range (tuple): The range of the mass variable. Default is (5000, 5800).
    Returns:
        RooWorkspace: The created RooWorkspace.
    """

    ws = ROOT.RooWorkspace(name, title)

    # Retrieve parameters from kwargs or set default values
    m_var = kwargs.get("m_var", "bmass")
    m_title = kwargs.get("m_title", "m")
    m_range = kwargs.get("m_range", (5000, 5800))

    # Check if the mass variable is already a RooAbsReal
    if isinstance(m_var, ROOT.RooAbsReal):
        bmass = m_var
    else:
        m_min, m_max = m_range
        bmass = ROOT.RooRealVar(m_var, m_title + " (MeV/c^{2})", m_min, m_max)

    sigmodel = models.dscb(ws, bmass, "B_sig", "B_sig", prefix="B_sig_", calib=True)
    ws.Import(sigmodel)

    shift = ws["B_sig_shift"]
    shift.setVal(0.0)
    scale = ws["B_sig_scale"]
    scale.setVal(1.0)

    b0gmodel = models.ege(
        ws,
        bmass,
        "B_b0g",
        "B_b0g",
        prefix="B_b0g_",
        calib=True,
        shift=shift,
        scale=scale,
    )
    ws.Import(b0gmodel)

    b1gmodel = models.ege(
        ws,
        bmass,
        "B_b1g",
        "B_b1g",
        prefix="B_b1g_",
        calib=True,
        shift=shift,
        scale=scale,
    )
    ws.Import(b1gmodel)

    # Signal + background model
    # Define fractions of signal and background
    N = ROOT.RooRealVar("B_peak_N", "B_peak_N", 1000, 0.0, 1.0e6)
    Fsig = ROOT.RooRealVar("B_peak_Fsig", "Fsig", 1000, 0.0, 1.0e6)
    Fb0g = ROOT.RooRealVar("B_peak_Fb0g", "Fb0g", 1000, 0.0, 1.0e6)
    Fb1g = ROOT.RooRealVar("B_peak_Fb1g", "Fb1g", 1000, 0.0, 1.0e6)
    S = ROOT.RooLinearVar(
        "B_peak_Nsig", "B_peak_Nsig", Fsig, N, ROOT.RooFit.RooConst(0.0)
    )
    B0g = ROOT.RooLinearVar(
        "B_peak_Nb0g", "B_peak_Nb0g", Fb0g, N, ROOT.RooFit.RooConst(0.0)
    )
    B1g = ROOT.RooLinearVar(
        "B_peak_Nb1g", "B_peak_Nb1g", Fb1g, N, ROOT.RooFit.RooConst(0.0)
    )
    # ws.Import(S)
    # ws.Import(B0g)
    # ws.Import(B1g)

    model = ROOT.RooAddPdf(
        "B_peak", "B_peak", [sigmodel, b0gmodel, b1gmodel], [S, B0g, B1g]
    )
    ws.Import(model)

    sigparams = sigmodel.getParameters({bmass})
    b0gparams = b0gmodel.getParameters({bmass})
    b1gparams = b1gmodel.getParameters({bmass})
    peakparams = model.getParameters({bmass})

    ws.defineSet("sigparams", sigparams)
    ws.defineSet("b0gparams", b0gparams)
    ws.defineSet("b1gparams", b1gparams)
    ws.defineSet("peakparams", peakparams)

    components = [sigmodel, b0gmodel, b1gmodel]
    yields = [S, B0g, B1g]
    yield_frac = [Fsig, Fb0g, Fb1g]

    ws.defineSet("peak_yields", ROOT.RooArgList(*yields))
    ws.defineSet("peak_frac", ROOT.RooArgList(*yield_frac))
    ws.defineSet("peak_components", ROOT.RooArgList(*components))

    ws.Print()  # Important for some reason to save to file

    return ws


def add_secondary_peak(ws, massvar, xregion):
    """
    Adds a secondary peak to the workspace.
    Parameters:
    - ws (RooWorkspace): The RooWorkspace object to add the peak to.
    - masvar (str): The name of the mass variable.
    - xregion (str): The name of the region.
    Returns:
    None
    """

    bmass = ws.var(massvar)
    peakpars = ws.set("peakparams")
    deltaM = {
        "eta": -87.37,
        "omega": 87.37,
        "phi": -87.37,
    }

    sig_s_model = models.dscb(
        ws,
        bmass,
        "B_s_sig",
        "B_s_sig",
        prefix="B_s_sig_",
        calib=True,
        alphaL=peakpars["B_sig_aL"],
        alphaR=peakpars["B_sig_aR"],
        nL=peakpars["B_sig_nL"],
        nR=peakpars["B_sig_nR"],
        scale=peakpars["B_sig_scale"],
        shift=peakpars["B_sig_shift"],
        sigma=peakpars["B_sig_sigma"],
    )

    # try:
    b0g_s_model = models.ege(
        ws,
        bmass,
        "B_s_b0g",
        "B_s_b0g",
        prefix="B_s_b0g_",
        calib=True,
        alphaL=peakpars["B_b0g_aL"],
        alphaR=peakpars["B_b0g_aR"],
        scale=peakpars["B_sig_scale"],
        shift=peakpars["B_sig_shift"],
        sigma=peakpars["B_b0g_sigma"],
    )
    b1g_s_model = models.ege(
        ws,
        bmass,
        "B_s_b1g",
        "B_s_b1g",
        prefix="B_s_b1g_",
        calib=True,
        alphaL=peakpars["B_b1g_aL"],
        alphaR=peakpars["B_b1g_aR"],
        scale=peakpars["B_sig_scale"],
        shift=peakpars["B_sig_shift"],
        sigma=peakpars["B_b1g_sigma"],
    )

    ws["B_s_sig_mean"].setVal(peakpars["B_sig_mean"].getVal() + deltaM[xregion])
    ws["B_s_b0g_mean"].setVal(peakpars["B_b0g_mean"].getVal() + deltaM[xregion])
    ws["B_s_b1g_mean"].setVal(peakpars["B_b1g_mean"].getVal() + deltaM[xregion])

    ws["B_s_sig_mean"].setConstant(True)
    ws["B_s_b0g_mean"].setConstant(True)
    ws["B_s_b1g_mean"].setConstant(True)

    Ns = ROOT.RooRealVar("B_s_peak_N", "B_s_peak_N", 1000, 0.0, 1.0e6)
    Fsig = peakpars["B_peak_Fsig"]
    Fb0g = peakpars["B_peak_Fb0g"]
    Fb1g = peakpars["B_peak_Fb1g"]

    Ss = ROOT.RooLinearVar(
        "B_s_peak_Nsig", "B_s_peak_Nsig", Fsig, Ns, ROOT.RooFit.RooConst(0.0)
    )
    B0gs = ROOT.RooLinearVar(
        "B_s_peak_Nb0g", "B_s_peak_Nb0g", Fb0g, Ns, ROOT.RooFit.RooConst(0.0)
    )
    B1gs = ROOT.RooLinearVar(
        "B_s_peak_Nb1g", "B_s_peak_Nb1g", Fb1g, Ns, ROOT.RooFit.RooConst(0.0)
    )

    peak_s_components = [sig_s_model, b0g_s_model, b1g_s_model]
    peak_s_yields = [Ss, B0gs, B1gs]
    peak_s_frac = [Fsig, Fb0g, Fb1g]

    peak_s_model = ROOT.RooAddPdf(
        "B_s_peak", "B_s_peak", peak_s_components, peak_s_yields
    )

    #    except Exception as e:
    #        logging.error(f"An error occurred while attemping to build the small peak model: {e}")
    #        gbkg_s_model = models.ege(ws, bmass, 'B_s_gbkg', 'B_s_gbkg', prefix='B_s_gbkg_', calib=True,
    #                                alphaL=peakpars['B_gbkg_aL'],
    #                                alphaR=peakpars['B_gbkg_aR'],
    #                                scale=peakpars['B_sig_scale'],
    #                                shift=peakpars['B_sig_shift'],
    #                                sigma=peakpars['B_gbkg_sigma'])
    #
    #        ws['B_s_sig_mean'].setVal(peakpars['B_sig_mean'].getVal() + deltaM[xregion])
    #        ws['B_s_gbkg_mean'].setVal(peakpars['B_gbkg_mean'].getVal() + deltaM[xregion])
    #
    #        ws['B_s_sig_mean'].setConstant(True)
    #        ws['B_s_gbkg_mean'].setConstant(True)
    #
    #        Ns = ROOT.RooRealVar('B_s_peak_N', 'B_s_peak_N', 1000, 0., 1.e6)
    #        Fsig = peakpars['B_peak_Fsig']
    #        Fbkg = peakpars['B_peak_Fbkg']
    #
    #        Ss = ROOT.RooLinearVar('B_s_peak_Nsig', 'B_s_peak_Nsig', Fsig, Ns, ROOT.RooFit.RooConst(0.))
    #        Bs = ROOT.RooLinearVar('B_s_peak_Nbkg', 'B_s_peak_Nbkg', Fbkg, Ns, ROOT.RooFit.RooConst(0.))
    #
    #        peak_s_components = [sig_s_model, gbkg_s_model]
    #        peak_s_yields = [Ss, Bs]
    #        peak_s_frac = [Fsig, Fbkg]
    #
    #        peak_s_model = ROOT.RooAddPdf('B_s_peak', 'B_s_peak',
    #                            [sig_s_model, gbkg_s_model],
    #                            [Ss, Bs])

    ws.Import(peak_s_model)

    ws.defineSet("peak_s_params", peak_s_model.getParameters({bmass}))
    ws.defineSet("peak_s_yields", ROOT.RooArgList(*peak_s_yields))
    ws.defineSet("peak_s_frac", ROOT.RooArgList(*peak_s_frac))
    ws.defineSet("peak_s_components", ROOT.RooArgList(*peak_s_components))


def build_combined_model(ws, masvar, kde_pdf=None):
    """
    Adds a combined model (signal + combinatorial) to the workspace.
    Parameters:
    - ws (RooWorkspace): The RooWorkspace object to add the model to.
    - masvar (str): The name of the mass variable.
    Returns:
    None
    """

    bmass = ws.var(masvar)

    # Construct the combinatorial background model
    Nb = ROOT.RooRealVar("B_combkg_N", "B_combkg_N", 1000, 0.0, 1.0e6)
    combkg_model = models.exp(ws, bmass, "B_combkg", "B_combkg", prefix="B_combkg_")

    # Retrieve the signal and gamma background models
    # sigmodel = ws.pdf('B_sig')
    # gbkgmodel = ws.pdf('B_gbkg')
    # S = ws['B_peak_Nsig']
    # B = ws['B_peak_Nbkg']

    peak_components = ROOT.RooArgList(ws.set("peak_components"))
    peak_yields = ROOT.RooArgList(ws.set("peak_yields"))

    model_name = "model"

    # Attempt to retrieve the secondary peak model

    if ws.set("peak_s_components") and ws.set("peak_s_yields"):
        print("Secondary peak model found")
        # sig_s_model = ws.pdf('B_s_sig')peak_s_model.getParameters({bmass})
        # gbkg_s_model = ws.pdf('B_s_gbkg')
        # Ss = ws['B_s_peak_Nsig']
        # Bs = ws['B_s_peak_Nbkg']
        peak_s_components = ROOT.RooArgList(ws.set("peak_s_components"))
        peak_s_yields = ROOT.RooArgList(ws.set("peak_s_yields"))

        model_components = ROOT.RooArgList(peak_components)
        model_components.add(peak_s_components)
        model_yields = ROOT.RooArgList(peak_yields)
        model_yields.add(peak_s_yields)

        model_components.add(combkg_model)
        model_yields.add(Nb)

        # components = [sigmodel, gbkgmodel, sig_s_model, gbkg_s_model, combkg_model]
        # yields = [S, B, Ss, Bs, Nb]

        if kde_pdf:
            # Attempt to retrieve the KDE model
            kde_model = ws.pdf(kde_pdf)
            Nkde = ROOT.RooRealVar("B_bkg_pr_N", "B_bkg_pr_N", 1000, 0.0, 1.0e6)
            model_components.add(kde_model)
            model_yields.add(Nkde)

        # Construct the combined model with the small peak
        model = ROOT.RooAddPdf(
            f"B_{model_name}", f"B_{model_name}", model_components, model_yields
        )
        ws.Import(model)

        # yields_set = ROOT.RooArgSet(*yields)
        ws.defineSet(f"{model_name}_yields", model_yields)

        # components_set = ROOT.RooArgSet(*components)
        ws.defineSet(f"{model_name}_components", model_components)

        model_name = "fallback_model"
        # except Exception as e:
        # logging.error(f"An error occurred while attemping to retrieve the small peak model: {e}")

    print(f"Building fallback model: {model_name}")
    # Construct the fallback combined model without the small peak
    # fallback_components = [sigmodel, gbkgmodel, combkg_model]
    # fallback_yields = [S, B, Nb]
    fallback_components = ROOT.RooArgList(peak_components)
    fallback_yields = ROOT.RooArgList(peak_yields)

    fallback_components.add(combkg_model)
    fallback_yields.add(Nb)

    if kde_pdf:
        kde_model = ws.pdf(kde_pdf)
        Nkde = ROOT.RooRealVar("B_bkg_pr_N", "B_bkg_pr_N", 1000, 0.0, 1.0e6)
        fallback_components.add(kde_model)
        fallback_yields.add(Nkde)

    fbmodel = ROOT.RooAddPdf(
        f"B_{model_name}", f"B_{model_name}", fallback_components, fallback_yields
    )
    ws.Import(fbmodel)

    # yields_set = ROOT.RooArgSet(*fallback_yields)
    ws.defineSet(f"{model_name}_yields", fallback_yields)

    # components_set = ROOT.RooArgSet(*fallback_components)
    ws.defineSet(f"{model_name}_components", fallback_components)
