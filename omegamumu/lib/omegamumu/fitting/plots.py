import os

from cycler import cycler

import ROOT
from sympy import Range

mpath = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
lbstyle = os.path.join(mpath, "common", "plotting", "lhcbStyle.C")
ROOT.gROOT.ProcessLine(f".L {lbstyle}")
ROOT.lhcbStyle()


def draw_frame(
    xvar,
    data,
    model=None,
    components=None,
    legend=False,
    params=False,
    pull_hist=False,
    **kwargs,
):
    """
    Draws a frame for plotting data and model components.
    Args:
        xvar (ROOT.RooRealVar): The variable to be plotted on the x-axis.
        data (ROOT.RooAbsData): The data to be plotted.
        model (ROOT.RooAbsPdf, optional): The model to be plotted. Defaults to None.
        components (list, optional): List of tuples specifying the components of the model to be plotted. Each tuple should contain the component name, color, and line style. Defaults to None.
        params (bool, optional): Flag indicating whether to display the model parameters on the plot. Defaults to False.
        pull_hist (bool, optional): Flag indicating whether to display the pull histogram on a separate frame. Defaults to False.
        **kwargs: Additional keyword arguments to customize the plot.
    Returns:
        tuple: A tuple containing the main frame and the pull frame (if pull_hist is True), or just the main frame and None.
    """

    range = kwargs.get("range", "")

    frame = xvar.frame(Range=range) if range else xvar.frame()

    data.plotOn(frame)
    # data.plotOn(frame, ROOT.RooFit.Range(range_min, range_max))

    if model:
        model.plotOn(frame, LineColor=(ROOT.kMagenta + 2), Range=range)
        # model.plotOn(frame, ROOT.RooFit.Range(range_min, range_max), LineColor=ROOT.kMagenta)

        pullframe = None
        if pull_hist:
            pulls = frame.pullHist()
            pullframe = xvar.frame(Range=range) if range else xvar.frame()
            pullframe.GetYaxis().SetTitle("Pulls")
            pullframe.addPlotable(pulls, "P")

        if params:
            layout = kwargs.get("layout", (0.62, 0.9, 0.8))
            precision = kwargs.get("precision", 2)
            model.paramOn(
                frame,
                Layout=layout,
                Format=("NEU", ROOT.RooFit.AutoPrecision(precision)),
            )

        if components:
            for comp, color, line in components:
                model.plotOn(frame, Components={comp}, LineColor=color, LineStyle=line)

        return frame, pullframe

    if legend:
        legend = ROOT.TLegend(0.7, 0.7, 0.9, 0.9)
        legend.AddEntry(data, "Data", "lep")
        if model:
            legend.AddEntry(model, "Model", "l")
        if components:
            for comp in components:
                comp_name, comp_color, comp_style = comp
                legend.AddEntry(comp_name, comp_name, "l")
        frame.addObject(legend)

    return frame, None


def styled_components(components, colors=None, styles=None):
    """
    Helper function to assign a style to each component to plot on the same frame.

    Args:
        components: RooArgSet containing the components to style.
        colors: Optional list of colors to use.
        styles: Optional list of line styles to use.

    Returns:
        list: List of tuples containing component name, color, and line style.
    """
    if colors is None:
        colors = ["b", "r", "g", "c", "k"]
    if styles is None:
        styles = ["-", "--", ":", "-.", "-"]

    style_cycler = cycler(color=colors) + cycler(linestyle=styles)
    pdf_components = [p.GetName() for p in components]
    plt_components = [
        (comp, style["color"], style["linestyle"])
        for comp, style in zip(pdf_components, style_cycler)
    ]

    return plt_components


def plot_frame(frame):

    canvas = ROOT.TCanvas(frame.GetName(), frame.GetName(), 800, 600)
    # canvas.SetBottomMargin(0)
    # canvas.SetLeftMargin(0.25)
    # canvas.SetRightMargin(0.05)

    canvas.cd()
    frame.Draw()

    return canvas


def plot_frame_with_pulls(frame, pulls, logy=False):

    canvas = ROOT.TCanvas(frame.GetName(), frame.GetName(), 800, 600)
    canvas.SetBottomMargin(0)
    canvas.SetLeftMargin(0.25)
    canvas.SetRightMargin(0.05)
    canvas.Clear()

    # Create upper and lower pads
    upper = ROOT.TPad("upper", "upper", 0.0, 0.25, 1.0, 1.0)
    lower = ROOT.TPad("lower", "lower", 0.0, 0.0, 1.0, 0.25)

    # Apply current style to both TPads
    upper.UseCurrentStyle()
    lower.UseCurrentStyle()

    # Draw the pads
    upper.Draw()
    lower.Draw()

    upper.SetLeftMargin(0.15)
    upper.SetBottomMargin(0.02)
    upper.SetTopMargin(0.06)

    lower.SetLeftMargin(0.15)
    lower.SetBottomMargin(0.4)
    lower.SetTopMargin(0.01)

    upper.cd()
    frame.Draw()
    if logy:
        upper.SetLogy()

    canvas.cd()

    tsize = 3 * 0.06
    xaxis = pulls.GetXaxis()
    xaxis.SetLabelSize(tsize)
    xaxis.SetTitleSize(1.05 * tsize)

    yaxis = pulls.GetYaxis()
    yaxis.SetLabelSize(tsize)
    yaxis.SetTitleSize(1.05 * tsize)
    yaxis.SetNdivisions(505)
    yaxis.SetRangeUser(-8.5, 8.5)

    xmin = xaxis.GetXmin()
    xmax = xaxis.GetXmax()
    line1 = ROOT.TLine(xmin, 5, xmax, 5)
    line2 = ROOT.TLine(xmin, 2, xmax, 2)
    line3 = ROOT.TLine(xmin, -2, xmax, -2)
    line4 = ROOT.TLine(xmin, -5, xmax, -5)

    line1.SetLineColor(ROOT.kMagenta + 3)
    line2.SetLineColor(ROOT.kBlue)
    line3.SetLineColor(ROOT.kBlue)
    line4.SetLineColor(ROOT.kMagenta + 3)

    line1.SetLineWidth(2)
    line2.SetLineWidth(2)
    line3.SetLineWidth(2)
    line4.SetLineWidth(2)

    line1.SetDrawOption("L")
    line2.SetDrawOption("L")
    line3.SetDrawOption("L")
    line4.SetDrawOption("L")

    pulls.addObject(line1)
    # pulls.addObject(line2)
    # pulls.addObject(line3)
    pulls.addObject(line4)

    lower.cd()
    pulls.Draw()

    return canvas
