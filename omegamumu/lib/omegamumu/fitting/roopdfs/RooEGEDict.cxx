// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME RooEGEDict
#define R__NO_DEPRECATION

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "ROOT/RConfig.hxx"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Header files passed as explicit arguments
#include "RooEGE.h"

// Header files passed via #pragma extra_include

// The generated code does not explicitly qualify STL entities
namespace std {} using namespace std;

namespace ROOT {
   static void *new_RooEGE(void *p = nullptr);
   static void *newArray_RooEGE(Long_t size, void *p);
   static void delete_RooEGE(void *p);
   static void deleteArray_RooEGE(void *p);
   static void destruct_RooEGE(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RooEGE*)
   {
      ::RooEGE *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RooEGE >(nullptr);
      static ::ROOT::TGenericClassInfo 
         instance("RooEGE", ::RooEGE::Class_Version(), "RooEGE.h", 18,
                  typeid(::RooEGE), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::RooEGE::Dictionary, isa_proxy, 4,
                  sizeof(::RooEGE) );
      instance.SetNew(&new_RooEGE);
      instance.SetNewArray(&newArray_RooEGE);
      instance.SetDelete(&delete_RooEGE);
      instance.SetDeleteArray(&deleteArray_RooEGE);
      instance.SetDestructor(&destruct_RooEGE);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RooEGE*)
   {
      return GenerateInitInstanceLocal(static_cast<::RooEGE*>(nullptr));
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const ::RooEGE*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr RooEGE::fgIsA(nullptr);  // static to hold class pointer

//______________________________________________________________________________
const char *RooEGE::Class_Name()
{
   return "RooEGE";
}

//______________________________________________________________________________
const char *RooEGE::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooEGE*)nullptr)->GetImplFileName();
}

//______________________________________________________________________________
int RooEGE::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooEGE*)nullptr)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RooEGE::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooEGE*)nullptr)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RooEGE::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooEGE*)nullptr)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void RooEGE::Streamer(TBuffer &R__b)
{
   // Stream an object of class RooEGE.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(RooEGE::Class(),this);
   } else {
      R__b.WriteClassBuffer(RooEGE::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_RooEGE(void *p) {
      return  p ? new(p) ::RooEGE : new ::RooEGE;
   }
   static void *newArray_RooEGE(Long_t nElements, void *p) {
      return p ? new(p) ::RooEGE[nElements] : new ::RooEGE[nElements];
   }
   // Wrapper around operator delete
   static void delete_RooEGE(void *p) {
      delete (static_cast<::RooEGE*>(p));
   }
   static void deleteArray_RooEGE(void *p) {
      delete [] (static_cast<::RooEGE*>(p));
   }
   static void destruct_RooEGE(void *p) {
      typedef ::RooEGE current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class ::RooEGE

namespace {
  void TriggerDictionaryInitialization_RooEGEDict_Impl() {
    static const char* headers[] = {
"RooEGE.h",
nullptr
    };
    static const char* includePaths[] = {
"/cvmfs/lhcbdev.cern.ch/conda/envs/default/2024-07-10_13-01/linux-64/include/",
"/home/epp/phsdpd/work/B2omegaMuMu/omegamumu/lib/omegamumu/fitting/roopdfs/",
nullptr
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "RooEGEDict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_AutoLoading_Map;
class __attribute__((annotate("$clingAutoload$RooEGE.h")))  RooEGE;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "RooEGEDict dictionary payload"


#define _BACKWARD_BACKWARD_WARNING_H
// Inline headers
#include "RooEGE.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[] = {
"RooEGE", payloadCode, "@",
nullptr
};
    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("RooEGEDict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_RooEGEDict_Impl, {}, classesHeaders, /*hasCxxModule*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_RooEGEDict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_RooEGEDict() {
  TriggerDictionaryInitialization_RooEGEDict_Impl();
}
