/*****************************************************************************
 * Project: RooFit                                                           *
 *                                                                           *
 * This code was autogenerated by RooClassFactory                            *
 *****************************************************************************/

#define ROOEGE_cxx
// Your description goes here...

#include "Riostream.h"

#include "RooAbsCategory.h"
#include "RooAbsReal.h"
#include "RooEGE.h"
#include "TMath.h"
#include <math.h>

// ClassImp(RooEGE)

RooEGE::RooEGE( const char* name, const char* title, RooAbsReal& _x, RooAbsReal& _x0, RooAbsReal& _sigma,
                RooAbsReal& _alpha1, RooAbsReal& _alpha2 )
    : RooAbsPdf( name, title )
    , x( "x", "x", this, _x )
    , x0( "x0", "x0", this, _x0 )
    , sigma( "sigma", "sigma", this, _sigma )
    , alpha1( "alpha1", "alpha1", this, _alpha1 )
    , alpha2( "alpha2", "alpha2", this, _alpha2 ) {}

RooEGE::RooEGE( const RooEGE& other, const char* name )
    : RooAbsPdf( other, name )
    , x( "x", this, other.x )
    , x0( "x0", this, other.x0 )
    , sigma( "sigma", this, other.sigma )
    , alpha1( "alpha1", this, other.alpha1 )
    , alpha2( "alpha2", this, other.alpha2 ) {}

Double_t RooEGE::evaluate() const {

  Double_t t = ( x - x0 ) / sigma;

  Double_t absAlpha1 = fabs( (Double_t)alpha1 );
  Double_t absAlpha2 = fabs( (Double_t)alpha2 );

  if ( t >= -alpha1 && t <= -alpha2 ) {
    return exp( -0.5 * t * t );
  } else if ( t < -alpha1 ) {
    return exp( alpha1 * ( 0.5 * alpha1 + t ) );
  } else {
    return exp( absAlpha2 * ( 0.5 * absAlpha2 - t ) );
  }
}

RooEGE::~RooEGE() {
  // close();
}
