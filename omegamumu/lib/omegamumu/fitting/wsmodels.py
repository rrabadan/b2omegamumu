import os

path = os.path.dirname(os.path.realpath(__file__))

import ROOT

ROOT.gSystem.Load(f"{path}/roopdfs/RooEGE.so")


def gauss(
    ws, x, name="gauss", title="gauss", prefix="", suffix="", calib=False, **kwargs
):
    """
    Auxiliary function to create a Gaussian PDF inside a workspace.

    Parameters
    ----------
    ws     : The workspace.
    x      : The variable of the pdf.
    name   : The name of the function.
    title  : The title of the function.
    prefix : The prefix of the parameters.
    """

    xvar = x if isinstance(x, ROOT.RooAbsReal) else ws[x]

    mean = kwargs.get("mean", (5300, xvar.getMin(), xvar.getMax()))
    shift = kwargs.get("shift", (0, -20, 20))

    sigma = kwargs.get("sigma", (0.0, 500))
    scale = kwargs.get("scale", (1, 0.0, 10))

    _mean = (
        mean
        if isinstance(mean, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}mean{suffix}", "mean", *mean)
    )
    _sigma = (
        sigma
        if isinstance(sigma, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}sigma{suffix}", "#sigma", *sigma)
    )

    if not calib:
        ws.Import(ROOT.RooGaussian(name, title, xvar, _mean, _sigma))
        return ws.pdf(name)

    _shift = (
        shift
        if isinstance(shift, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}shift{suffix}", "#Delta#mu", *shift)
    )
    _mu = ROOT.RooAddition(f"{prefix}mu{suffix}", "#mu", ROOT.RooArgList(_mean, _shift))

    _scale = (
        scale
        if isinstance(scale, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}scale{suffix}", "scale", *scale)
    )
    _width = ROOT.RooProduct(
        f"{prefix}width{suffix}", "#sigma", ROOT.RooArgList(_sigma, _scale)
    )

    # _mean.setConstant(True)
    # _sigma.setConstant(True)

    ws.Import(ROOT.RooGaussian(name, title, xvar, _mu, _width))
    return ws.pdf(name)


def adcb(ws, x, name="adcb", title="adcb", prefix="", suffix="", calib=False, **kwargs):
    """
    Auxiliary function to create an asymmetric double-sided Crystal Ball PDF inside a workspace.

    Parameters
    ----------
    ws     : The workspace.
    x      : The variable of the PDF.
    name   : The name of the function.
    title  : The title of the function.
    prefix : The prefix of the parameters.
    suffix : The suffix of the parameters.
    """

    xvar = x if isinstance(x, ROOT.RooAbsReal) else ws[x]

    mean = kwargs.get("mean", (5300, xvar.getMin(), xvar.getMax()))
    shift = kwargs.get("shift", (0, -20, 20))

    sigmaL = kwargs.get("sigmaL", (0.0, 500))
    sigmaR = kwargs.get("sigmaR", (0.0, 500))
    scale = kwargs.get("scale", (1, 0.0, 10))

    alphaL = kwargs.get("alphaL", (0, 20))
    alphaR = kwargs.get("alphaR", (0, 20))
    nL = kwargs.get("nL", (5, 0, 20))
    nR = kwargs.get("nR", (5, 0, 20))

    _mean = (
        mean
        if isinstance(mean, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}mean{suffix}", "mean", *mean)
    )
    _sigmaL = (
        sigmaL
        if isinstance(sigmaL, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}sigmaL{suffix}", "#sigma_{L}", *sigmaL)
    )
    _sigmaR = (
        sigmaR
        if isinstance(sigmaR, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}sigmaR{suffix}", "#sigma_{R}", *sigmaR)
    )

    _aL = (
        alphaL
        if isinstance(alphaL, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}aL{suffix}", "#alpha_{L}", *alphaL)
    )
    _aR = (
        alphaR
        if isinstance(alphaR, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}aR{suffix}", "#alpha_{R}", *alphaR)
    )
    _nL = (
        nL
        if isinstance(nL, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}nL{suffix}", "n_{L}", *nL)
    )
    _nR = (
        nR
        if isinstance(nR, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}nR{suffix}", "n_{R}", *nR)
    )

    if not calib:
        ws.Import(
            ROOT.RooCrystalBall(
                name, title, xvar, _mean, _sigmaL, _sigmaR, _aL, _nL, _aR, _nR
            )
        )
        return ws.pdf(name)

    _shift = (
        shift
        if isinstance(shift, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}shift{suffix}", "#Delta#mu", *shift)
    )
    _mu = ROOT.RooAddition(f"{prefix}mu{suffix}", "#mu", ROOT.RooArgList(_mean, _shift))

    _scale = (
        scale
        if isinstance(scale, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}scale{suffix}", "scale", *scale)
    )
    _widthL = ROOT.RooProduct(
        f"{prefix}widthL{suffix}", "#sigma_{L}", ROOT.RooArgList(_sigmaL, _scale)
    )
    _widthR = ROOT.RooProduct(
        f"{prefix}widthR{suffix}", "#sigma_{R}", ROOT.RooArgList(_sigmaR, _scale)
    )

    # _mean.setConstant(True)
    # _sigmaR.setConstant(True)
    # _sigmaL.setConstant(True)

    ws.Import(
        ROOT.RooCrystalBall(
            name, title, xvar, _mu, _widthL, _widthR, _aL, _nL, _aR, _nR
        )
    )
    return ws.pdf(name)


def dscb(ws, x, name="dscb", title="dscb", prefix="", suffix="", calib=False, **kwargs):
    """
    Auxiliary function to create a double-sided Crystal Ball PDF inside a workspace.

    Parameters
    ----------
    ws     : The workspace.
    x      : The variable of the pdf.
    name   : The name of the function.
    title  : The title of the function.
    prefix : The prefix of the parameters.
    suffix : The suffix of the parameters.
    """

    xvar = x if isinstance(x, ROOT.RooAbsReal) else ws[x]

    mean = kwargs.get("mean", (5300, xvar.getMin(), xvar.getMax()))
    shift = kwargs.get("shift", (0, -20, 20))

    sigma = kwargs.get("sigma", (0.0, 500))
    scale = kwargs.get("scale", (1, 0.0, 10))

    alphaL = kwargs.get("alphaL", (0, 20))
    alphaR = kwargs.get("alphaR", (0, 20))
    nL = kwargs.get("nL", (5, 0, 20))
    nR = kwargs.get("nR", (5, 0, 20))

    _mean = (
        mean
        if isinstance(mean, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}mean{suffix}", "mean", *mean)
    )
    _sigma = (
        sigma
        if isinstance(sigma, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}sigma{suffix}", "#sigma", *sigma)
    )

    _aL = (
        alphaL
        if isinstance(alphaL, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}aL{suffix}", "#alpha_{L}", *alphaL)
    )
    _aR = (
        alphaR
        if isinstance(alphaR, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}aR{suffix}", "#alpha_{R}", *alphaR)
    )
    _nL = (
        nL
        if isinstance(nL, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}nL{suffix}", "n_{L}", *nL)
    )
    _nR = (
        nR
        if isinstance(nR, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}nR{suffix}", "n_{R}", *nR)
    )

    if not calib:
        ws.Import(
            ROOT.RooCrystalBall(name, title, xvar, _mean, _sigma, _aL, _nL, _aR, _nR)
        )
        return ws.pdf(name)

    _shift = (
        shift
        if isinstance(shift, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}shift{suffix}", "#Delta#mu", *shift)
    )
    _mu = ROOT.RooAddition(f"{prefix}mu{suffix}", "#mu", ROOT.RooArgList(_mean, _shift))

    _scale = (
        scale
        if isinstance(scale, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}scale{suffix}", "scale", *scale)
    )
    _width = ROOT.RooProduct(
        f"{prefix}width{suffix}", "#sigma", ROOT.RooArgList(_sigma, _scale)
    )

    # _mean.setConstant(True)
    # _sigma.setConstant(True)

    ws.Import(ROOT.RooCrystalBall(name, title, xvar, _mu, _width, _aL, _nL, _aR, _nR))
    return ws.pdf(name)


def cbs(ws, x, name="cbs", title="cbs", prefix="", suffix="", calib=False, **kwargs):
    """
    Auxiliary function to create a Crystal Ball PDF inside a workspace.

    Parameters
    ----------
    ws     : The workspace.
    x      : The variable of the pdf.
    name   : The name of the function.
    title  : The title of the function.
    prefix : The prefix of the parameters.
    suffix : The suffix of the parameters.
    """

    xvar = x if isinstance(x, ROOT.RooAbsReal) else ws[x]

    mean = kwargs.get(
        "mean", ((xvar.getMax() - xvar.getMin()) / 2.0, xvar.getMin(), xvar.getMax())
    )
    shift = kwargs.get("shift", (0, -20, 20))

    sigma = kwargs.get("sigma", (0.0, 500))
    scale = kwargs.get("scale", (1, 0.0, 10))

    alpha = kwargs.get("alpha", (0, 20))
    n = kwargs.get("n", (5, 0, 20))

    _mean = (
        mean
        if isinstance(mean, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}mean{suffix}", "mean", *mean)
    )
    _sigma = (
        sigma
        if isinstance(sigma, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}sigma{suffix}", "#sigma", *sigma)
    )

    _a = (
        alpha
        if isinstance(alpha, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}a{suffix}", "#alpha", *alpha)
    )
    _n = (
        n
        if isinstance(n, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}n{suffix}", "n", *n)
    )

    if not calib:
        ws.Import(ROOT.RooCrystalBall(name, title, xvar, _mean, _sigma, _a, _n))
        return ws.pdf(name)

    _shift = (
        shift
        if isinstance(shift, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}shift{suffix}", "#Delta#mu", *shift)
    )
    _mu = ROOT.RooAddition(f"{prefix}mu{suffix}", "#mu", ROOT.RooArgList(_mean, _shift))

    _scale = (
        scale
        if isinstance(scale, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}scale{suffix}", "scale", *scale)
    )
    _width = ROOT.RooProduct(
        f"{prefix}width{suffix}", "#sigma", ROOT.RooArgList(_sigma, _scale)
    )

    # _mean.setConstant(True)
    # _sigma.setConstant(True)

    ws.Import(ROOT.RooCrystalBall(name, title, xvar, _mu, _width, _a, _n))
    return ws.pdf(name)


def ege(ws, x, name="ege", title="ege", prefix="", suffix="", calib=False, **kwargs):
    """
    Auxiliary function to create an EGE PDF inside a workspace.

    Parameters
    ----------
    ws     : The workspace.
    x      : The variable of the pdf.
    name   : The name of the function.
    title  : The title of the function.
    prefix : The prefix of the parameters.
    suffix : The suffix of the parameters.
    """

    xvar = x if isinstance(x, ROOT.RooAbsReal) else ws[x]

    mean = kwargs.get("mean", (xvar.getMin(), xvar.getMax()))
    shift = kwargs.get("shift", (0, -20, 20))

    sigma = kwargs.get("sigma", (0.1, 500))
    scale = kwargs.get("scale", (1, 0.0, 10))

    alphaL = kwargs.get("alphaL", (-10, 10))
    alphaR = kwargs.get("alphaR", (-10, 10))

    _mean = (
        mean
        if isinstance(mean, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}mean{suffix}", "mean", *mean)
    )
    _sigma = (
        sigma
        if isinstance(sigma, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}sigma{suffix}", "#sigma", *sigma)
    )
    _aL = (
        alphaL
        if isinstance(alphaL, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}aL{suffix}", "#alpha_{L}", *alphaL)
    )
    _aR = (
        alphaR
        if isinstance(alphaR, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}aR{suffix}", "#alpha_{R}", *alphaR)
    )

    if not calib:
        ws.Import(ROOT.RooEGE(name, title, xvar, _mean, _sigma, _aL, _aR))
        return ws.pdf(name)

    _shift = (
        shift
        if isinstance(shift, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}shift{suffix}", "#Delta#mu", *shift)
    )
    _mu = ROOT.RooAddition(f"{prefix}mu{suffix}", "#mu", ROOT.RooArgList(_mean, _shift))

    _scale = (
        scale
        if isinstance(scale, ROOT.RooAbsReal)
        else ROOT.RooRealVar(f"{prefix}scale{suffix}", "scale", *scale)
    )
    _width = ROOT.RooProduct(
        f"{prefix}width{suffix}", "#sigma", ROOT.RooArgList(_sigma, _scale)
    )

    # _mean.setConstant(True)
    # _sigma.setConstant(True)

    ws.Import(ROOT.RooEGE(name, title, xvar, _mu, _width, _aL, _aR))
    return ws.pdf(name)


def ege_gauss(
    ws,
    x,
    name="ege_gauss",
    title="ege_gauss",
    prefix="",
    suffix="",
    calib=False,
    **kwargs,
):
    """
    Auxiliary function to create an EGE + Gaussian PDF inside a workspace.

    Parameters
    ----------
    ws    : The workspace.
    x     : The variable of the pdf.
    name  : The name of the function.
    title : The title of the function.
    suffix: The suffix of the function.
    prefix: The prefix of the function.
    calib : Whether to calibrate the parameters.
    kwargs: Additional parameters for the model.
    """

    xvar = x if isinstance(x, ROOT.RooRealVar) else ws[x]

    share_mean = kwargs.get("share_mean", False)
    share_sigma = kwargs.get("share_sigma", False)

    if share_mean:
        kwargs["mean"] = ROOT.RooRealVar(
            f"{prefix}mean{suffix}",
            f"{prefix}mean{suffix}",
            xvar.getMin(),
            xvar.getMax(),
        )
    if share_sigma:
        kwargs["sigma"] = ROOT.RooRealVar(
            f"{prefix}sigma{suffix}", f"{prefix}sigma{suffix}", 0.1, 500
        )
        # mean = ROOT.RooRealVar(f'{prefix}mean{suffix}', f'{prefix}mean{suffix}', 5400., xvar.getMin(), xvar.getMax())
        # #kwargs['mean'] = mean

    ege_model = ege(
        ws,
        xvar,
        f"{prefix}ege{suffix}",
        f"{prefix}ege{suffix} component",
        prefix=prefix + "ege_",
        suffix=suffix,
        calib=calib,
        **kwargs,
    )
    gauss_model = gauss(
        ws,
        xvar,
        f"{prefix}gauss{suffix}",
        f"{prefix}gauss{suffix} component",
        prefix=prefix + "gauss_",
        suffix=suffix,
        calib=calib,
        **kwargs,
    )

    fege = ROOT.RooRealVar(
        f"{prefix}ege_frac{suffix}", f"{prefix}ege_frac{suffix}", 0.5, 0.0, 1.0
    )
    bkg_model = ROOT.RooAddPdf(name, title, [ege_model, gauss_model], [fege])
    ws.Import(bkg_model)
    return ws.pdf(name)


def cbs_gauss(
    ws,
    x,
    name="cbs_gauss",
    title="cbs_gauss",
    prefix="",
    suffix="",
    calib=False,
    **kwargs,
):
    """
    Auxiliary function to create an CBS + Gaussian PDF inside a workspace.

    Parameters
    ----------
    ws    : The workspace.
    x     : The variable of the pdf.
    name  : The name of the function.
    title : The title of the function.
    suffix: The suffix of the function.
    prefix: The prefix of the function.
    calib : Whether to calibrate the parameters.
    kwargs: Additional parameters for the model.
    """

    xvar = x if isinstance(x, ROOT.RooRealVar) else ws[x]

    share_mean = kwargs.get("share_mean", False)
    share_sigma = kwargs.get("share_sigma", False)

    if share_mean:
        kwargs["mean"] = ROOT.RooRealVar(
            f"{prefix}mean{suffix}",
            f"{prefix}mean{suffix}",
            xvar.getMin(),
            xvar.getMax(),
        )
    if share_sigma:
        kwargs["sigma"] = ROOT.RooRealVar(
            f"{prefix}sigma{suffix}", f"{prefix}sigma{suffix}", 0.1, 500
        )
        # mean = ROOT.RooRealVar(f'{prefix}mean{suffix}', f'{prefix}mean{suffix}', 5400., xvar.getMin(), xvar.getMax())
        # #kwargs['mean'] = mean

    # ege_model = ege(ws, xvar, f'{prefix}ege{suffix}', f'{prefix}ege{suffix} component', prefix=prefix+'ege_', suffix=suffix, calib=calib, **kwargs)
    cbs_model = cbs(
        ws,
        xvar,
        f"{prefix}cbs{suffix}",
        f"{prefix}cbs{suffix} component",
        prefix=prefix + "cbs_",
        suffix=suffix,
        calib=calib,
        **kwargs,
    )
    gauss_model = gauss(
        ws,
        xvar,
        f"{prefix}gauss{suffix}",
        f"{prefix}gauss{suffix} component",
        prefix=prefix + "gauss_",
        suffix=suffix,
        calib=calib,
        **kwargs,
    )

    fcbs = ROOT.RooRealVar(
        f"{prefix}cbs_frac{suffix}", f"{prefix}cbs_frac{suffix}", 0.5, 0.0, 1.0
    )
    bkg_model = ROOT.RooAddPdf(name, title, [cbs_model, gauss_model], [fcbs])
    ws.Import(bkg_model)
    return ws.pdf(name)


def exp(ws, x, name="exp", title="exp", prefix="", suffix="", **kwargs):
    """
    Auxiliary function to create an exponential PDF inside a workspace.

    Parameters
    ----------
    ws     : The workspace.
    x      : The variable of the pdf.
    name   : The name of the function.
    title  : The title of the function.
    prefix : The prefix of the parameters.
    suffix : The suffix of the parameters.
    """

    xvar = x if isinstance(x, ROOT.RooAbsReal) else ws[x]

    slope = kwargs.get("slope", (-100, 100))

    _slope = ROOT.RooRealVar(f"{prefix}slope{suffix}", "#lambda", *slope)

    ws.Import(ROOT.RooExponential(name, title, xvar, _slope))
    return ws.pdf(name)
