import os
import logging
import argparse
import pandas as pd

from omegamumu.common.utils import KeyValMap
from omegamumu.datasets.config import get_eventtypes
from omegamumu.corrections import (
    load_reweighter,
    gammapid_reweighter_inputs,
)
from locations import Locations


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("mcsample", help='Event type or "name" of the MC sample')
    parser.add_argument(
        "decay", choices=["omegamumu", "kstplusmumu"], help="Decay channel"
    )
    parser.add_argument(
        "--reweighter",
        default="Bplus2KstPlusJpsi",
        help="Reweighter file",
    )
    return parser.parse_args()


def load_dataframe(inputf):
    if not os.path.exists(inputf):
        logging.error(f"File {inputf} does not exist")
        exit(1)
    return pd.read_pickle(inputf)


def calculate_weights(df, reweighter):
    oweights = df["kin_weight"] * df["tistos_weight"] * df["trk_weight"]
    inputs = gammapid_reweighter_inputs(df)
    w1 = reweighter.predict_weights(inputs)
    w2 = reweighter.predict_weights(inputs, original_weight=oweights)
    return w1, w2


def create_output_dataframe(df, w1, w2):
    ndf = df.copy().loc[:, ["runNumber", "eventNumber", "candNumber"]]
    ndf["gammapid_weight"] = w1
    ndf["total_gammapid_weight"] = w2
    return ndf


def main():

    args = parse_arguments()

    logging.basicConfig(level=logging.INFO)

    mcsamples = KeyValMap(data=get_eventtypes())
    try:
        _, samplename = mcsamples.get_pair(args.mcsample)
    except Exception as e:
        print(e)
        exit(1)
    nameid = f"{samplename}_{args.decay}"

    reweighter = load_reweighter(
        os.path.join(Locations.get_conditions_corrections_dir(), "gammapid"),
        "_".join([args.reweighter, "gbreweighter"]),
    )

    inputf = os.path.join(Locations.get_df_dir(), f"{nameid}.pkl")
    df = load_dataframe(inputf)

    w1, w2 = calculate_weights(df, reweighter)
    ndf = create_output_dataframe(df, w1, w2)

    outputf = os.path.join(
        Locations.get_conditions_weights_dir(), f"{nameid}_gammapid_weights.pkl"
    )
    ndf.to_pickle(outputf)
    logging.info(f"Saved weights to {outputf}")


if __name__ == "__main__":
    main()
