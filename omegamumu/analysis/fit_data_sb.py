from math import log
import os
import argparse
import importlib
import logging
import json

from numpy import cov
import pandas as pd

import ROOT

import omegamumu.fitting.wsmodels as wsmodels
import omegamumu.fitting.plots as plotter

from locations import Locations
from processing import filter_dataframe

DECAY_TO_CANDIDATE = {
    "omegamumu": ("B", "omega", "#pi^{+}#pi^{-}"),
    "kstplusmumu": ("Bplus", "kstplus", "K^{+}"),
    "rhoplusmumu": ("Bplus", "rhoplus", "#pi^{+}"),
}
BLINDRANGE = {"omegamumu": (5200, 5500), "kstplusmumu": (5100, 5400)}
SIGRANGE = {"omegamumu": (5200, 5360), "kstplusmumu": (5100, 5400)}


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "decay", choices=["omegamumu", "kstplusmumu"], help="Decay channel"
    )
    parser.add_argument("mode", choices=["jpsi", "psi2s", "rare"], help="Dimuon mode")
    parser.add_argument(
        "xrange",
        choices=["eta", "omega", "kst", "rho", "phi"],
        help=" x meson mass window",
    )
    parser.add_argument(
        "massvar", choices=["bmass", "bmass_piz", "bmass_jpsi"], help="Mass variable"
    )
    return parser.parse_args()


def setup_logging():
    logging.basicConfig(level=logging.INFO)


def get_input_file(args):
    file = f"{args.decay}_{args.mode}.pkl"
    return os.path.join(Locations.get_df_dir(), file)


def dynamic_massvar_import(decay, massvar):
    module = importlib.import_module(f"omegamumu.variables.{decay}")
    return getattr(module, massvar)


def configure_dataframe(args):

    file = get_input_file(args)

    df = pd.read_pickle(file)
    logging.info(f"Loaded {file}")
    logging.info(f"Entries: {df.shape[0]}")

    massvar = dynamic_massvar_import(args.decay, args.massvar)

    # b_cand = DECAY_TO_CANDIDATE[args.decay][0]
    x_cand = DECAY_TO_CANDIDATE[args.decay][1]

    df = filter_dataframe(
        df, args.decay, x_cand=x_cand, xwindow=args.xrange, ismc=False
    )

    # Select columns
    cols = [
        "runNumber",
        "eventNumber",
        "candNumber",
    ]
    cols += massvar.branch
    df = df.loc[:, cols].copy()

    # rename columns
    df.rename(
        columns={
            massvar.branch[0]: massvar.name,
        },
        inplace=True,
    )

    return df


def fill_dataset(df, name, massvar):
    m_name = massvar.GetName()
    ds = ROOT.RooDataSet.from_numpy(
        {m_name: df[m_name].to_numpy()}, [massvar], name=name
    )
    return ds


def configure_workspace(args):
    massvar = dynamic_massvar_import(args.decay, args.massvar)
    ws = ROOT.RooWorkspace(f"{massvar.name}_sb_fit")
    roomass = ROOT.RooRealVar(
        massvar.name, massvar.x_title_root, massvar.x_min, massvar.x_max
    )

    roomass.setRange("full", massvar.x_min, massvar.x_max)
    roomass.setRange("left", massvar.x_min, BLINDRANGE[args.decay][0])
    roomass.setRange("right", BLINDRANGE[args.decay][1], massvar.x_max)
    roomass.setRange("signal", SIGRANGE[args.decay][0], SIGRANGE[args.decay][1])
    roomass.setRange("blind", BLINDRANGE[args.decay][0], BLINDRANGE[args.decay][1])

    ws.Import(roomass)

    wsmodels.exp(ws, roomass, name="exp")
    B = ROOT.RooRealVar("B", "B", 1000, 0, 1e6)
    ws.Import(B)
    ## Extend model
    B_model = ROOT.RooExtendPdf("B_model", "B_model", ws.pdf("exp"), B)
    ws.Import(B_model)
    return ws


def plot_fit(mass, data, model, name, extension="png"):

    frame = mass.frame()

    data.plotOn(frame)

    model.plotOn(
        frame,
        LineColor=(ROOT.kMagenta + 2),
        Range="full",
        NormRange="left,right",
        # Normalization=dict(scaleFactor=1.0, scaleType=ROOT.RooAbsReal.RelativeExpected),
    )

    pulls = frame.pullHist()
    pullframe = mass.frame()
    pullframe.GetYaxis().SetTitle("Pulls")
    pullframe.addPlotable(pulls, "P")

    layout = (0.62, 0.9, 0.8)
    precision = 2
    model.paramOn(
        frame,
        Layout=layout,
        Format=("NEU", ROOT.RooFit.AutoPrecision(precision)),
    )
    canvas = plotter.plot_frame_with_pulls(frame, pullframe)
    canvas.SaveAs(os.path.join(Locations.get_plots_dir(), f"{name}.{extension}"))


def fit_sb(model, data):
    fit_result = model.fitTo(data, Range="left,right", Extended=True, Save=True)
    model.removeStringAttribute("fitrange")
    return fit_result


def extrapolate_yield(model, data, mass, fit_result, range="signal"):

    integral_full = model.createIntegral(ROOT.RooArgSet(mass), Range="full")
    integral_range = model.createIntegral(ROOT.RooArgSet(mass), Range=range)

    yield_range = integral_range.getVal()
    yield_full = integral_full.getVal()

    logging.info(
        f"Yield in sidebands: {yield_range}, yield in full range: {yield_full}"
    )

    scale_factor = yield_range / yield_full
    entries_range = data.sumEntries() * scale_factor

    logging.info(f"Scale factor: {scale_factor}")

    # Propagate the error
    # cov_matrix = fit_result.covarianceMatrix()
    error_range = integral_range.getPropagatedError(fit_result)
    error_full = integral_full.getPropagatedError(fit_result)
    logging.info(f"Error in range: {error_range}, error in full range: {error_full}")

    scale_factor_error = (
        scale_factor
        * ((error_range / yield_range) ** 2 + (error_full / yield_full) ** 2) ** 0.5
    )
    entries_range_error = scale_factor_error * entries_range
    return entries_range, entries_range_error


def main():

    setup_logging()

    args = parse_arguments()

    ws = configure_workspace(args)
    ws.Print()

    bmass = ws.var(args.massvar)
    model = ws.pdf("B_model")

    df = configure_dataframe(args)
    df.query(
        f"`{args.massvar}` > {bmass.getMin()} & `{args.massvar}` < {bmass.getMax()}",
        inplace=True,
    )
    logging.info(f"Configured dataframe: {df.shape}")

    ds = fill_dataset(df, "data", bmass)
    blinded_ds = ds.reduce(CutRange="left,right")
    ws.Import(ds)

    fit_result = fit_sb(model, blinded_ds)
    fit_result.Print()

    m_range = "signal"
    yield_in_range, yield_unc = extrapolate_yield(
        model, ds, bmass, fit_result, range=m_range
    )

    logging.info(f"Full dataset: {ds.numEntries()}")
    logging.info(f"Blinded dataset: {blinded_ds.numEntries()}")
    logging.info(
        f"Extrapolated yield in the {m_range} region: {yield_in_range} +/- {yield_unc}"
    )

    plot_fit(bmass, blinded_ds, model, "test_sb")

    # Save the extrapolated yield to a json file
    yield_file = os.path.join(
        Locations.get_tables_dir(), f"{args.decay}_{args.mode}_extbkgyield.json"
    )
    with open(yield_file, "w") as f:
        json.dump({"yield": [yield_in_range, yield_unc]}, f, indent=4)


if __name__ == "__main__":
    main()
