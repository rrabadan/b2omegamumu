import os
import argparse
import logging

import pandas as pd

import ROOT

import omegamumu.fitting.workspaces as workspaces
import omegamumu.fitting.plots as plotter

from omegamumu.common.utils import KeyValMap
from omegamumu.datasets.config import get_eventtypes

from locations import Locations
from processing import filter_dataframe

DECAY_TO_CANDIDATE = {
    "omegamumu": ("B", "omega", "#pi^{+}#pi^{-}"),
    "kstplusmumu": ("Bplus", "kstplus", "K^{+}"),
    "rhoplusmumu": ("Bplus", "rhoplus", "#pi^{+}"),
}
SIGRANGE = {"omegamumu": (5000, 6000), "kstplusmumu": (4800, 6000)}


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "decay", choices=["omegamumu", "kstplusmumu"], help="Decay channel"
    )
    parser.add_argument(
        "xrange",
        choices=["eta", "omega", "kst", "rho", "phi"],
        help=" x meson mass window",
    )
    parser.add_argument(
        "massvar", choices=["bmass", "bmass-piz", "bmass-psi"], help="Mass variable"
    )
    parser.add_argument("sigshape", help='Event type or "name" of the MC sample')
    parser.add_argument(
        "--bkgshape",
        nargs="?",
        default=None,
        help='Event type or "name" of the bkg sample',
    )
    return parser.parse_args()


def setup_logging():
    logging.basicConfig(level=logging.INFO)


def get_input_file(args):
    file = f"{args.decay}_jpsi.pkl"
    return os.path.join(Locations.get_df_dir(), file)


def get_sigshape_file(args):
    mcsamples = KeyValMap(data=get_eventtypes())
    try:
        eventtype, samplename = mcsamples.get_pair(args.sigshape)
    except Exception as e:
        print(e)
        exit(1)

    file = f"{samplename}_{args.decay}_{args.massvar}_fit_ws.root"
    return os.path.join(Locations.get_conditions_fitws_dir(), file)


def get_bkgshape_file(args):
    mcsamples = KeyValMap(data=get_eventtypes())
    try:
        eventtype, samplename = mcsamples.get_pair(args.bkgshape)
    except Exception as e:
        print(e)
        exit(1)

    file = f"{samplename}_{args.decay}_{args.xrange}_kde_ws.root"
    return os.path.join(Locations.get_conditions_fitws_dir(), file)


def read_ws_file(file, wsname):
    with ROOT.TFile(file) as f:
        ws = f.Get(wsname)
    return ws


def massvar_titles(massvar, decay):
    titles = {
        "omegamumu": "#pi^{+}#pi^{-}",
        "kstplusmumu": "K^{+}{1}",
        "rhoplusmumu": "#pi^{+}",
    }

    decay_title = titles[decay]
    if massvar == "bmass":
        return "".join(["m(", "#mu^{+}#mu^{-}", decay_title, "#gamma#gamma", ")"])
    if massvar == "bmass-piz":
        return "".join(
            ["m(", "#mu^{+}#mu^{-}", decay_title, "#gamma#gamma", "#pi^{0}", ")"]
        )
    if massvar == "bmass-psi":
        return "".join(["m(", "J/#psi", decay_title, "#pi^{0}", ")"])


def configure_dataframe(file, decay, xwindow=None):

    df = pd.read_pickle(file)
    logging.info(f"Loaded {file}")
    logging.info(f"Entries: {df.shape[0]}")

    b_cand = DECAY_TO_CANDIDATE[decay][0]
    x_cand = DECAY_TO_CANDIDATE[decay][1]

    df = filter_dataframe(df, decay, x_cand=x_cand, xwindow=xwindow, ismc=False)

    # Select columns
    cols = [
        f"{b_cand}_M",
        f"{b_cand}_piz_DTF_M",
        f"{b_cand}_Jpsi_DTF_M",
        f"{x_cand}_M",
        "pi0_M",
        "runNumber",
        "eventNumber",
        "candNumber",
    ]
    df = df.loc[:, cols].copy()

    # rename columns
    df.rename(
        columns={
            f"{b_cand}_M": "bmass",
            f"{b_cand}_piz_DTF_M": "bmass-piz",
            f"{b_cand}_Jpsi_DTF_M": "bmass-psi",
            f"{x_cand}_M": "xmass",
            "pi0_M": "pimass",
        },
        inplace=True,
    )

    return df


def fill_dataset(df, name, massvar):
    m_name = massvar.GetName()
    ds = ROOT.RooDataSet.from_numpy(
        {m_name: df[m_name].to_numpy()}, [massvar], name=name
    )
    return ds


def configure_workspace(
    name,
    massvar,
    masstitle,
    range,
    sigshape,
    bkgshape=None,
    xregion=None,
    secondary_peak=False,
):

    ws_sig = read_ws_file(sigshape, "B_peak")
    ws_bkg = read_ws_file(bkgshape, "B_bkg") if bkgshape else None

    if ws_bkg:
        ws = workspaces.bpeak_workspace(name, name, m_var=ws_bkg.var(massvar))
    else:
        ws = workspaces.bpeak_workspace(
            name, name, m_var=massvar, m_title=masstitle, m_range=range
        )

    peakparams_sig = ws_sig.set("peakparams")
    peakparams = ws.set("peakparams")

    for p in peakparams_sig:
        p_name = p.GetName()
        peakparams[p_name].setVal(p.getVal())
        peakparams[p_name].setError(p.getError())
        peakparams[p_name].setConstant(True)

    peakparams["B_peak_N"].setConstant(False)

    if secondary_peak:
        assert xregion, "Secondary peak requires xregion"
        print(f"Adding secondary peak for {xregion}")
        workspaces.add_secondary_peak(ws, massvar, xregion)

    name_kde_pdf = None
    if ws_bkg:
        ws.Import(ws_bkg.pdf(f"kde_{massvar}"))
        # kde_pdf.Print(MINUIT status code)
        name_kde_pdf = f"kde_{massvar}"

    workspaces.build_combined_model(ws, massvar, kde_pdf=name_kde_pdf)

    return ws


def fit_combined_model(model, data, peakpars):
    r = model.fitTo(data, Save=True, PrintLevel=-1)
    r.Print("v")

    peakpars["B_sig_shift"].setConstant(False)
    r = model.fitTo(data, Save=True, PrintLevel=-1)
    r.Print("v")

    peakpars["B_sig_shift"].setConstant(True)
    peakpars["B_sig_scale"].setConstant(False)
    r = model.fitTo(data, Save=True, PrintLevel=-1)
    r.Print("v")

    peakpars["B_sig_shift"].setConstant(False)
    peakpars["B_sig_scale"].setConstant(False)
    r = model.fitTo(data, Save=True, PrintLevel=-1)
    r.Print("v")

    return r


def compute_sweights(data, model, yields):
    for p in model.getParameters(data):
        p.setConstant(True)

    for y in yields:
        y.setConstant(False)

    sData = ROOT.RooStats.SPlot("sData", "An sPlot", data, model, yields)
    sData.AddSWeight(model, yields)

    # sData.GetSWeightVars().Print("v")
    return sData.GetNumSWeightVars() > 0


def add_sweights(data, df, massvar):
    data_dict = data.to_numpy()
    data_dict.pop(massvar)
    sw_df = pd.DataFrame.from_dict(data_dict)
    assert (
        sw_df.shape[0] == df.shape[0]
    ), "Dataframe and dataset have different number of entries"
    df.reset_index(drop=True, inplace=True)
    sw_df.reset_index(drop=True, inplace=True)
    return pd.concat([df, sw_df], axis=1)


def plot_fit(mass, data, model, name, components=None, extension="png"):
    mframe, pulls = plotter.draw_frame(
        mass,
        data,
        model,
        pull_hist=True,
        params=True,
        components=components,
    )
    # layout=(0.4, 0.7, 0.4))
    canvas = plotter.plot_frame_with_pulls(mframe, pulls)
    canvas.SaveAs(os.path.join(Locations.get_plots_dir(), f"{name}.{extension}"))


def main():
    setup_logging()

    args = parse_arguments()

    file = get_input_file(args)

    sigfile = get_sigshape_file(args)
    bkgfile = get_bkgshape_file(args) if args.bkgshape else None

    logging.info(f"Signal file: {sigfile}")
    if bkgfile:
        logging.info(f"Background file: {bkgfile}")

    secondary_peak = True if args.decay == "omegamumu" else False

    ws = configure_workspace(
        "B_fit",
        args.massvar,
        massvar_titles(args.massvar, args.decay),
        SIGRANGE[args.decay],
        sigfile,
        bkgfile,
        xregion=args.xrange,
        secondary_peak=secondary_peak,
    )
    # ws.Print()

    bmass = ws.var(args.massvar)
    model = ws.pdf("B_model")
    yields = ws.set("model_yields")
    components = ws.set("model_components")

    df = configure_dataframe(file, args.decay, args.xrange)
    df.query(
        f"`{args.massvar}` > {bmass.getMin()} & `{args.massvar}` < {bmass.getMax()}",
        inplace=True,
    )
    logging.info(f"Configured dataframe: {df.shape}")

    ds = fill_dataset(df, "data", bmass)
    logging.info(f"Filled dataset: {ds.numEntries()}")
    ws.Import(ds)

    peakparams = ws.set("peakparams")
    ws.saveSnapshot("init", peakparams)

    r = fit_combined_model(model, ds, peakparams)
    ws.saveSnapshot("model_fit", peakparams)
    plot_fit(
        bmass,
        ds,
        model,
        f"{args.decay}_{args.xrange}_{args.massvar}_fit",
        components=plotter.styled_components(components),
    )

    if compute_sweights(ds, model, yields):
        logging.info(f"Fitted model status: {r.status()}, covQual: {r.covQual()}")
        logging.info(f"Computed sWeights")
        df_sw = add_sweights(ds, df, args.massvar)
        df_sw.to_pickle(
            os.path.join(
                Locations.get_conditions_weights_dir(),
                f"{args.decay}_{args.xrange}_{args.massvar}_sweights.pkl",
            )
        )
        ws.writeToFile(
            os.path.join(
                Locations.get_conditions_fitws_dir(),
                f"{args.decay}_{args.xrange}_{args.massvar}_fit_ws.root",
            )
        )
        # print(df_sw)
        return

    try:
        fbmodel = ws.pdf("B_fallback_model")
        yields = ws.set("fallback_model_yields")
        components = ws.set("fallback_model_components")
        components.Print()

        ws.loadSnapshot("init")
        r_fb = fit_combined_model(fbmodel, ds, ws.set("peakparams"))
        ws.saveSnapshot("fallback_model_fit", peakparams)

        plot_fit(
            bmass,
            ds,
            fbmodel,
            f"{args.decay}_{args.xrange}_{args.massvar}_fit_fallback",
            components=plotter.styled_components(components),
        )

        if compute_sweights(ds, fbmodel, yields):
            logging.info(
                f"Fitted model status: {r_fb.status()}, covQual: {r_fb.covQual()}"
            )
            logging.info(f"Computed sWeights")
            df_sw = add_sweights(ds, df, args.massvar)
            df_sw.to_pickle(
                os.path.join(
                    Locations.get_conditions_weights_dir(),
                    f"{args.decay}_{args.xrange}_{args.massvar}_sweights.pkl",
                )
            )
            ws.writeToFile(
                os.path.join(
                    Locations.get_conditions_fitws_dir(),
                    f"{args.decay}_{args.xrange}_{args.massvar}_fit_ws.root",
                )
            )
            return
    except Exception as e:
        logging.error(f"Failed to compute sWeights: {e}")
    finally:
        ws.writeToFile(
            os.path.join(
                Locations.get_conditions_fitws_dir(),
                f"{args.decay}_{args.xrange}_{args.massvar}_fit_ws.root",
            )
        )


if __name__ == "__main__":
    main()
