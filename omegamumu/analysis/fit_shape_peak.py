import os
import argparse
import logging

import numpy as np
import pandas as pd

import ROOT

import omegamumu.fitting.plots as plotter
import omegamumu.fitting.workspaces as workspaces

from omegamumu.common.utils import KeyValMap
from omegamumu.datasets.config import get_eventtypes

from locations import Locations
from processing import filter_dataframe

XRANGE = (400, 1100)

BRANGE = {"omegamumu": (4800, 6000), "kstplusmumu": (4800, 7000)}

SIGRANGE = {"omegamumu": (5000, 5800), "kstplusmumu": (4800, 6000)}

DECAY_TO_CANDIDATE = {
    "omegamumu": ("B", "omega", "#pi^{+}#pi^{-}"),
    "kstplusmumu": ("Bplus", "kstplus", "K^{+}"),
    "rhoplusmumu": ("Bplus", "rhoplus", "#pi^{+}"),
}


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("mcsample", help='Event type or "name" of the MC sample')
    parser.add_argument(
        "decay", choices=["omegamumu", "kstplusmumu"], help="Decay channel"
    )
    parser.add_argument(
        "massvar", choices=["bmass", "bmass-piz", "bmass-psi"], help="Mass variable"
    )
    parser.add_argument(
        "--xwindow",
        nargs="?",
        default=None,
        choices=["eta", "omega", "kst", "rho", "phi"],
        help=" x meson mass window",
    )
    parser.add_argument(
        "--nosweights", action="store_true", help="Fit sWeighted gamma bkg shapes"
    )
    return parser.parse_args()


def setup_logging():
    logging.basicConfig(level=logging.INFO)


def get_sample_name(args):
    mcsamples = KeyValMap(data=get_eventtypes())
    try:
        eventtype, samplename = mcsamples.get_pair(args.mcsample)
    except Exception as e:
        print(e)
        exit(1)

    nameid = f"{samplename}_{args.decay}"
    return nameid


def load_workspace_pizero(nameid):
    filename = os.path.join(
        Locations.get_conditions_fitws_dir(), f"{nameid}_Pi0_fit_ws.root"
    )
    logging.info(f"Loading {filename}")
    f = ROOT.TFile(filename)
    wspiz = f.Get("Pi0_fit")
    return wspiz


def configure_workspace(name, title, bmass, decay):
    ws = workspaces.bpeak_workspace(name, title, m_var=bmass)
    mass_name = bmass.GetName() if isinstance(bmass, ROOT.RooAbsReal) else bmass
    ws.var(mass_name).setRange("sigrange", SIGRANGE[decay][0], SIGRANGE[decay][1])
    return ws


def create_weigthed_dataset(data):
    return ROOT.RooDataSet(
        data.GetName(), data.GetTitle(), data, data.get(), "", "Pi0_nbkg_sw"
    )


def load_mass_and_datasets(nameid, mass_name):
    wspiz = load_workspace_pizero(nameid)
    mass = wspiz[mass_name]
    mass.Print()
    datasets = {
        "ds": wspiz.data("data"),
        "ds_sig": wspiz.data("data_sig"),
        "ds_bkg": wspiz.data("data_bkg"),
        "ds_b1g": wspiz.data("data_b1g"),
        "ds_b0g": wspiz.data("data_b0g"),
    }
    return mass, datasets


def configure_dataframe(file, decay, xwindow=None):

    df = pd.read_pickle(file)
    logging.info(f"Loaded {file}")
    logging.info(df.shape)

    b_cand = DECAY_TO_CANDIDATE[decay][0]
    x_cand = DECAY_TO_CANDIDATE[decay][1]

    df = filter_dataframe(df, decay, x_cand=x_cand, xwindow=xwindow)

    # Select columns
    cols = [
        f"{b_cand}_M",
        f"{b_cand}_piz_DTF_M",
        f"{b_cand}_Jpsi_DTF_M",
        f"{x_cand}_M",
        "pi0_M",
        "true_b2g",
        "true_b1g",
        "true_b0g",
    ]
    # df = df[cols]
    df = df.loc[:, cols].copy()

    # rename columns
    df.rename(
        columns={
            f"{b_cand}_M": "bmass",
            f"{b_cand}_piz_DTF_M": "bmass-piz",
            f"{b_cand}_Jpsi_DTF_M": "bmass-psi",
            f"{x_cand}_M": "xmass",
            #'pi0_M': 'pimass',
        },
        inplace=True,
    )

    return df


def configure_variables(decay):
    xmin, xmax = XRANGE
    bmin, bmax = BRANGE[decay]

    x_title = DECAY_TO_CANDIDATE[decay][2]

    xmass = ROOT.RooRealVar(
        "xmass", f"m({x_title} #gamma #gamma)" + " (MeV/c^{2})", xmin, xmax
    )
    bmass = ROOT.RooRealVar(
        "bmass",
        "m(#mu^{+} #mu^{-} " + x_title + " #gamma #gamma) (MeV/c^{2})",
        bmin,
        bmax,
    )
    bmasspiz = ROOT.RooRealVar(
        "bmass-piz", "m(#mu^{+}#mu^{-}" + x_title + "#pi^{0}) (MeV/c^{2})", bmin, bmax
    )
    bmasspsi = ROOT.RooRealVar(
        "bmass-psi", "m(J/#psi " + x_title + " #pi^{0}) (MeV/c^{2})", bmin, bmax
    )

    return xmass, bmass, bmasspiz, bmasspsi


def fill_dataset(df, name, xmass, bmass, bmasspiz, bmasspsi):
    ds = ROOT.RooDataSet.from_numpy(
        {
            "xmass": df["xmass"].to_numpy(),
            "bmass": df["bmass"].to_numpy(),
            "bmass-piz": df["bmass-piz"].to_numpy(),
            "bmass-psi": df["bmass-psi"].to_numpy(),
        },
        [xmass, bmass, bmasspiz, bmasspsi],
        name=name,
    )
    return ds


def fit_signal_shape(model, data, sigparams):
    sigparams["B_sig_shift"].setConstant(True)
    sigparams["B_sig_scale"].setConstant(True)
    # sigparams["B_sig_mean"].setVal(5310)
    r_sig = model.fitTo(data, Save=True, Range="sigrange", PrintLevel=-1)
    r_sig.Print("v")
    return r_sig


def fit_gbkg_shape(model, data, params, label="gbkg", decay="omegamumu", sumw2=True):
    params["B_sig_shift"].setConstant(True)
    params["B_sig_scale"].setConstant(True)

    r_gbkg = model.fitTo(data, Save=True, PrintLevel=-1, SumW2Error=sumw2)
    r_gbkg.Print("v")

    params[f"B_{label}_mean"].setConstant(True)
    params[f"B_{label}_sigma"].setConstant(True)
    r_gbkg = model.fitTo(data, Save=True, PrintLevel=-1, SumW2Error=sumw2)
    r_gbkg.Print("v")

    params[f"B_{label}_mean"].setConstant(False)
    params[f"B_{label}_sigma"].setConstant(False)
    params[f"B_{label}_aL"].setConstant(True)
    params[f"B_{label}_aR"].setConstant(True)
    r_gbkg = model.fitTo(data, Save=True, PrintLevel=-1, SumW2Error=sumw2)
    r_gbkg.Print("v")

    # if not decay == 'omegamumu':
    #    return r_gbkg

    params[f"B_{label}_aL"].setConstant(False)
    params[f"B_{label}_aR"].setConstant(False)
    r_gbkg = model.fitTo(data, Save=True, PrintLevel=-1, SumW2Error=sumw2)
    r_gbkg.Print("v")

    return r_gbkg


def fit_gbkgsig_shape(model, data, gbkgparams, sigparams):

    for p in gbkgparams:
        p.setConstant(False)

    for p in sigparams:
        p.setConstant(True)

    r_bkg = model.fitTo(data, Save=True, PrintLevel=-1)
    r_bkg.Print("v")

    return r_bkg


def fit_peak_shape(model, data, gbkgparams, sigparams):
    for p in gbkgparams:
        p.setConstant(True)
    for p in sigparams:
        p.setConstant(True)
    r = model.fitTo(data, Save=True, Range="sigrange", PrintLevel=-1)
    # r = model.fitTo(data, Save=True, PrintLevel=-1)
    r.Print("v")
    return r


def plot_fit(
    mass,
    data,
    model,
    name,
    range=None,
    components=None,
    params=False,
    logy=False,
    extension="png",
):
    mframe, pulls = plotter.draw_frame(
        mass,
        data,
        model,
        pull_hist=True,
        params=params,
        components=components,
        range=range,
    )
    # layout=(0.4, 0.7, 0.4))
    canvas = plotter.plot_frame_with_pulls(mframe, pulls, logy=logy)
    canvas.SaveAs(os.path.join(Locations.get_plots_dir(), f"{name}.{extension}"))


def sweights(model, ds, weight, S, B):
    # Fsig.setConstant(True)
    # Fbkg.setConstant(True)

    yields = [S, B]
    sData = ROOT.RooStats.SPlot("sData", "sData", ds, model, yields)

    sData.AddSWeight(model, yields)

    ds.Print("v")

    sweights = ds.to_numpy()
    print(sweights)


def run_fit_peak_shape(args):

    nameid = get_sample_name(args)
    massvar = args.massvar

    inputf = os.path.join(Locations.get_dataframes_dir(), f"{nameid}_v2.pkl")

    df = configure_dataframe(inputf, args.decay, args.xwindow)
    logging.info(f"Dataframe  with {df.shape[0]} entries")

    masses = configure_variables(args.decay)

    bmass = next(m for m in masses if m.GetName() == massvar)

    ws = configure_workspace("B_peak", "B_peak", bmass, args.decay)

    ds_sig = fill_dataset(
        df.query("true_b2g"), "data_sig", masses[0], masses[1], masses[2], masses[3]
    )
    ds_bkg = fill_dataset(
        df.query("true_b0g | true_b1g"),
        "data_bkg",
        masses[0],
        masses[1],
        masses[2],
        masses[3],
    )
    ds_b1g = fill_dataset(
        df.query("true_b1g"), "data_b1g", masses[0], masses[1], masses[2], masses[3]
    )
    ds_b0g = fill_dataset(
        df.query("true_b0g"), "data_b0g", masses[0], masses[1], masses[2], masses[3]
    )

    ds = ds_sig.Clone("data")
    ds.append(ds_bkg)

    sigmodel = ws.pdf("B_sig")
    gbkgmodel = ws.pdf("B_gbkg")
    model = ws.pdf("B_peak")

    sigparams = ws.set("sigparams")
    gbkgparams = ws.set("gbkgparams")
    peakparams = ws.set("peakparams")

    ws.saveSnapshot("init_sig", sigparams, True)
    ws.saveSnapshot("init_gbkg", gbkgparams, True)

    r_sig = fit_signal_shape(sigmodel, ds_sig, sigparams)
    plot_fit(
        bmass, ds_sig, sigmodel, f"{nameid}_{massvar}_fit_signal", range="sigrange"
    )
    plot_fit(
        bmass,
        ds_sig,
        sigmodel,
        f"{nameid}_{massvar}_fit_signal_logy",
        range="sigrange",
        logy=True,
    )
    ws.saveSnapshot("sig_fit", sigparams, True)

    ws.loadSnapshot("init_gbkg")
    # mframe, _ = plotter.draw_frame(bmass, ds_bkg)
    # canvas = plotter.plot_frame(mframe)
    # canvas.SaveAs(os.path.join(Locations.get_plots_dir(), f'{nameid}_{massvar}_gb0g.png'))
    r_b0g = fit_gbkg_shape(gbkgmodel, ds_b0g, gbkgparams, decay=args.decay)
    plot_fit(bmass, ds_b0g, gbkgmodel, f"{nameid}_{massvar}_fit_gb0g")
    ws.saveSnapshot("gb0g_fit", gbkgparams, True)

    ws.loadSnapshot("init_gbkg")
    r_b1g = fit_gbkg_shape(gbkgmodel, ds_b1g, gbkgparams, decay=args.decay)
    plot_fit(bmass, ds_b1g, gbkgmodel, f"{nameid}_{massvar}_fit_gb1g")
    ws.saveSnapshot("gb1g_fit", gbkgparams, True)

    ws.loadSnapshot("init_gbkg")
    r_bkg = fit_gbkg_shape(gbkgmodel, ds_bkg, gbkgparams, decay=args.decay)
    plot_fit(bmass, ds_bkg, gbkgmodel, f"{nameid}_{massvar}_fit_gbkg")
    ws.saveSnapshot("gbkg_fit", gbkgparams, True)

    # Fit peak shape
    ws.loadSnapshot("sig_fit")
    ws.loadSnapshot("gbkg_fit")

    r = fit_peak_shape(model, ds, gbkgparams, sigparams)
    components = [(sigmodel.GetName(), "b", "--"), (gbkgmodel.GetName(), "c", "--")]
    plot_fit(
        bmass,
        ds,
        model,
        f"{nameid}_{massvar}_fit_peak",
        components=components,
        range="sigrange",
    )
    ws.saveSnapshot("peak_fit", peakparams, True)

    logging.info(f"Signal fit. Status: {r_sig.status()}, covQual: {r_sig.covQual()}")
    logging.info(
        f"0 true gamma bkg shape. Status: {r_b0g.status()}, covQual: {r_b0g.covQual()}"
    )
    logging.info(
        f"1 true gamma bkg shape. Status: {r_b1g.status()}, covQual: {r_b1g.covQual()}"
    )
    logging.info(
        f"gamma bkg shape. Status: {r_bkg.status()}, covQual: {r_bkg.covQual()}"
    )
    logging.info(f"Peak shape. Status: {r.status()}, covQual: {r.covQual()}")

    ws.writeToFile(
        os.path.join(
            Locations.get_conditions_fitws_dir(), f"{nameid}_{massvar}_fit_ws.root"
        )
    )


def run_fit_peak_shape_sweights(args):
    nameid = get_sample_name(args)
    massvar = args.massvar

    bmass, datasets = load_mass_and_datasets(nameid, massvar)

    ws = configure_workspace("B_peak", "B_peak", bmass, args.decay)

    sigmodel = ws.pdf("B_sig")
    gbkgmodel = ws.pdf("B_gbkg")
    # bkgmodel = ws.pdf('B_bkg')
    b0gmodel = ws.pdf("B_b0g")
    b1gmodel = ws.pdf("B_b1g")
    model = ws.pdf("B_peak")

    sigparams = ws.set("sigparams")
    gbkgparams = ws.set("gbkgparams")
    # bkgparams = ws.set('bkgparams')
    b0gparams = ws.set("b0gparams")
    b1gparams = ws.set("b1gparams")
    peakparams = ws.set("peakparams")

    # ws.saveSnapshot('init', bkgparams, True)

    r_sig = fit_signal_shape(sigmodel, datasets["ds_sig"], sigparams)
    plot_fit(
        bmass,
        datasets["ds_sig"],
        sigmodel,
        f"{nameid}_{massvar}_fit_sig",
        range="sigrange",
    )
    plot_fit(
        bmass,
        datasets["ds_sig"],
        sigmodel,
        f"{nameid}_{massvar}_fit_sig_logy",
        range="sigrange",
        params=False,
        logy=True,
    )
    ws.saveSnapshot("sig_fit", sigparams, True)

    ds_bkg_sw = create_weigthed_dataset(datasets["ds_bkg"])
    ds_b1g_sw = create_weigthed_dataset(datasets["ds_b1g"])
    ds_b0g_sw = create_weigthed_dataset(datasets["ds_b0g"])

    # ws.loadSnapshot('init')
    r_b0g_sw = fit_gbkg_shape(
        # gbkgmodel, ds_b0g_sw, gbkgparams, decay=args.decay, sumw2=True)
        b0gmodel,
        ds_b0g_sw,
        b0gparams,
        label="b0g",
        decay=args.decay,
        sumw2=True,
    )
    plot_fit(bmass, ds_b0g_sw, b0gmodel, f"{nameid}_{massvar}_fit_gb0g_sw", params=True)
    ws.saveSnapshot("gb0g_fit_sw", b0gparams, True)

    # ws.loadSnapshot('init')
    r_b1g_sw = fit_gbkg_shape(
        # gbkgmodel, ds_b1g_sw, gbkgparams, decay=args.decay, sumw2=True)
        b1gmodel,
        ds_b1g_sw,
        b1gparams,
        label="b1g",
        decay=args.decay,
        sumw2=True,
    )
    plot_fit(bmass, ds_b1g_sw, b1gmodel, f"{nameid}_{massvar}_fit_gb1g_sw", params=True)
    ws.saveSnapshot("gb1g_fit_sw", b1gparams, True)

    # Fit combined 1 true gamma and 0 true gamma background
    # Model is a the sum of the fitted 2 true gamma signal and an ege function
    # ws.loadSnapshot('init')
    ws.loadSnapshot("sig_fit")

    # r_bkg = fit_gbkgsig_shape(bkgmodel, datasets['ds_bkg'], gbkgparams,
    #                          sigparams)
    # components = [(sigmodel.GetName(), "b", "--"),
    #              (gbkgmodel.GetName(), "c", "--")]
    # plot_fit(
    #    bmass,
    #    datasets['ds_bkg'],
    #    bkgmodel,
    #    f'{nameid}_{massvar}_fit_gbkgsig',
    #    components=components)
    # ws.saveSnapshot('gbkgsig_fit', gbkgparams, True)

    # Fit peak shape
    # ws.loadSnapshot('sig_fit')
    # ws.loadSnapshot('gbkg_fit')

    # r = fit_peak_shape(model, datasets['ds'], gbkgparams, sigparams)
    # components = [(sigmodel.GetName(), "b", "--"),
    #              (gbkgmodel.GetName(), "c", "--")]
    # plot_fit(bmass, datasets['ds'], model, f'{nameid}_{massvar}_fit_peak', components=components, range='sigrange')
    # ws.saveSnapshot('peak_fit', peakparams, True)

    ws.loadSnapshot("sig_fit")
    # ws.loadSnapshot('gbkg_fit_sw')
    ws.loadSnapshot("gb0g_fit_sw")
    ws.loadSnapshot("gb1g_fit_sw")

    b01gparams = ROOT.RooArgSet(b0gparams, b1gparams)

    r_sw = fit_peak_shape(model, datasets["ds"], b01gparams, sigparams)
    components = [
        (sigmodel.GetName(), "b", "--"),
        (b0gmodel.GetName(), "c", "--"),
        (b1gmodel.GetName(), "g", "--"),
    ]
    plot_fit(
        bmass,
        datasets["ds"],
        model,
        f"{nameid}_{massvar}_fit_peak_sw",
        components=components,
        params=True,
        range="sigrange",
    )
    plot_fit(
        bmass,
        datasets["ds"],
        model,
        f"{nameid}_{massvar}_fit_peak_sw_logy",
        components=components,
        range="sigrange",
        params=False,
        logy=True,
    )
    ws.saveSnapshot("peak_fit_sw", peakparams, True)

    logging.info(f"Signal fit. Status: {r_sig.status()}, covQual: {r_sig.covQual()}")
    logging.info(
        f"0 true gamma bkg sw shape. Status: {r_b0g_sw.status()}, covQual: {r_b0g_sw.covQual()}"
    )
    logging.info(
        f"1 true gamma bkg sw shape. Status: {r_b1g_sw.status()}, covQual: {r_b1g_sw.covQual()}"
    )
    # logging.info(
    #    f'gamma bkg shape. Status: {r_bkg.status()}, covQual: {r_bkg.covQual()}'
    # )
    logging.info(f"Peak shape (sw): {r_sw.status()}, covQual: {r_sw.covQual()}")
    # logging.info(f'Peak shape. Status: {r.status()}, covQual: {r.covQual()}')

    ws.writeToFile(
        os.path.join(
            Locations.get_conditions_fitws_dir(), f"{nameid}_{massvar}_fit_ws.root"
        )
    )


def main():

    args = parse_arguments()

    setup_logging()

    if args.nosweights:
        run_fit_peak_shape(args)
        return
    run_fit_peak_shape_sweights(args)


if __name__ == "__main__":
    main()
