import os
import argparse
import logging

import pandas as pd
import numpy as np

import ROOT

import omegamumu.fitting.workspaces as workspaces
import omegamumu.fitting.plots as plotter

from omegamumu.common.utils import KeyValMap
from omegamumu.datasets.config import get_eventtypes

from locations import Locations
from processing import filter_dataframe

BRANGE = {"omegamumu": (4700, 6000), "kstplusmumu": (4800, 7000)}
BRANGE = {"omegamumu": (4800, 6000), "kstplusmumu": (4800, 7000)}
XRANGE = (400, 1100)

DECAY_TO_CANDIDATE = {
    "omegamumu": ("B", "omega", "#pi^{+}#pi^{-}"),
    "kstplusmumu": ("Bplus", "kstplus", "K^{+}"),
    "rhoplusmumu": ("Bplus", "rhoplus", "#pi^{+}"),
}


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("mcsample", help='Event type or "name" of the MC sample')
    parser.add_argument(
        "decay", choices=["omegamumu", "kstplusmumu"], help="Decay channel"
    )
    parser.add_argument(
        "--xwindow",
        nargs="?",
        default=None,
        choices=["eta", "omega", "kst", "rho", "phi"],
        help=" x meson mass window",
    )
    return parser.parse_args()


def configure_dataframe(file, decay, pizero_range, xwindow=None):

    df = pd.read_pickle(file)
    logging.info(f"Loaded {file}")
    logging.info(df.shape)

    b_cand = DECAY_TO_CANDIDATE[decay][0]
    x_cand = DECAY_TO_CANDIDATE[decay][1]

    df = filter_dataframe(df, decay, x_cand=x_cand, xwindow=xwindow)

    # Select columns
    cols = [
        f"{b_cand}_M",
        f"{b_cand}_piz_DTF_M",
        f"{b_cand}_Jpsi_DTF_M",
        f"{x_cand}_M",
        "pi0_M",
        "true_b2g",
        "true_b1g",
        "true_b0g",
        "runNumber",
        "eventNumber",
        "candNumber",
    ]
    # df = df[cols]
    df = df.loc[:, cols].copy()
    df.astype({"runNumber": "int32", "eventNumber": "int32", "candNumber": "int32"})

    # rename columns
    df.rename(
        columns={
            f"{b_cand}_M": "bmass",
            f"{b_cand}_piz_DTF_M": "bmass-piz",
            f"{b_cand}_Jpsi_DTF_M": "bmass-psi",
            f"{x_cand}_M": "xmass",
            "pi0_M": "pimass",
        },
        inplace=True,
    )

    bmin, bmax = BRANGE[decay]
    pimin, pimax = pizero_range
    df = (
        df.query(f"bmass > {bmin} and bmass < {bmax}")
        .query(f"`bmass-piz` > {bmin} and `bmass-piz` < {bmax}")
        .query(f"`bmass-psi` > {bmin} and `bmass-psi` < {bmax}")
        .query(f"pimass > {pimin} and pimass < {pimax}")
        .reset_index(drop=True)
    )

    return df


def configure_variables(decay, xrange, brange):
    xmin, xmax = xrange
    bmin, bmax = brange

    x_title = DECAY_TO_CANDIDATE[decay][2]

    xmass = ROOT.RooRealVar(
        "xmass", f"m({x_title} #gamma #gamma)" + " (MeV/c^{2})", xmin, xmax
    )
    bmass = ROOT.RooRealVar(
        "bmass",
        "m(#mu^{+} #mu^{-} " + x_title + " #gamma #gamma) (MeV/c^{2})",
        bmin,
        bmax,
    )
    bmasspiz = ROOT.RooRealVar(
        "bmass-piz", "m(#mu^{+}#mu^{-}" + x_title + "#pi^{0}) (MeV/c^{2})", bmin, bmax
    )
    bmasspsi = ROOT.RooRealVar(
        "bmass-psi", "m(J/#psi " + x_title + " #pi^{0}) (MeV/c^{2})", bmin, bmax
    )

    return xmass, bmass, bmasspiz, bmasspsi


def fill_dataset(df, name, pimass, xmass, bmass, bmasspiz, bmasspsi):
    ds = ROOT.RooDataSet.from_numpy(
        {
            "pimass": df["pimass"].to_numpy(),
            "xmass": df["xmass"].to_numpy(),
            "bmass": df["bmass"].to_numpy(),
            "bmass-piz": df["bmass-piz"].to_numpy(),
            "bmass-psi": df["bmass-psi"].to_numpy(),
        },
        [pimass, xmass, bmass, bmasspiz, bmasspsi],
        name=name,
    )
    return ds


def configure_datasets(ws, df, decay, xrange, brange):

    pimass = ws.var("pimass")
    xmass, bmass, bmasspiz, bmasspsi = configure_variables(decay, xrange, brange)

    # Signal and background candidates
    b2g_mask = df.true_b2g
    b1g_mask = df.true_b1g
    b0g_mask = df.true_b0g

    b2g_count = b2g_mask.sum()
    bkg_count = b1g_mask.sum() + b0g_mask.sum()
    logging.info(f"Signal dataframe  with {b2g_count} entries")
    logging.info(f"Bkg dataframe  with {bkg_count} entries")

    # Create datasets from the dataframes
    ds_sig = fill_dataset(
        df[b2g_mask], "data_sig", pimass, xmass, bmass, bmasspiz, bmasspsi
    )
    ds_bkg = fill_dataset(
        df[b1g_mask | b0g_mask], "data_bkg", pimass, xmass, bmass, bmasspiz, bmasspsi
    )
    ds_b1g = fill_dataset(
        df[b1g_mask], "data_b1g", pimass, xmass, bmass, bmasspiz, bmasspsi
    )
    ds_b0g = fill_dataset(
        df[b0g_mask], "data_b0g", pimass, xmass, bmass, bmasspiz, bmasspsi
    )

    ds = ds_sig.Clone("data")
    ds.append(ds_bkg)

    ws.Import(ds)
    ws.Import(ds_sig)
    ws.Import(ds_bkg)
    ws.Import(ds_b1g)
    ws.Import(ds_b0g)

    return b2g_mask, b1g_mask, b0g_mask


def plot_fit(mass, data, model, name, logy=False, extension="png"):
    mframe, pulls = plotter.draw_frame(
        mass, data, model, pull_hist=True, params=True, layout=(0.4, 0.7, 0.4)
    )
    canvas = plotter.plot_frame_with_pulls(mframe, pulls, logy=logy)
    canvas.SaveAs(os.path.join(Locations.get_plots_dir(), f"{name}.{extension}"))


def fit_signal_shape(data, model, sigparams):

    # Fit signal shape
    # Set initial values for the signal model
    sigparams["Pi0_sig_mean"].setVal(135)
    sigparams["Pi0_sig_mean"].setConstant(True)
    sigparams["Pi0_sig_sigma"].setVal(2)
    # sigparams["Pi0_sig_sigma"].setConstant(True)
    r_sig = model.fitTo(data, Save=True, PrintLevel=-1)
    r_sig.Print("v")

    # Release the signal model parameters
    sigparams["Pi0_sig_sigma"].setVal(8)
    sigparams["Pi0_sig_mean"].setConstant(False)
    sigparams["Pi0_sig_sigma"].setConstant(False)
    r_sig = model.fitTo(data, Save=True, PrintLevel=-1)
    r_sig.Print("v")
    return r_sig


def fit_sigbkg_shape(data, model, sigparams):
    # Fix the signal model parameters for the background fit
    for p in sigparams:
        p.setConstant(True)
    r_bkg = model.fitTo(data, Save=True, PrintLevel=-1)
    r_bkg.Print("v")
    return r_bkg


def add_bkg_sweights(data, model, bkgparams):
    for p in bkgparams:
        p.setConstant(True)

    nsig = bkgparams["Pi0_nsig"]
    nbkg = bkgparams["Pi0_nbkg"]

    nsig.setConstant(False)
    nbkg.setConstant(False)

    sData = ROOT.RooStats.SPlot("sData", "An sPlot", data, model, [nsig, nbkg])
    sData.AddSWeight(model, [nsig, nbkg])


def save_sweights(ds, df, filename):
    # Convert RooDataSet to DataFrame and ensure the DataFrame shapes match
    wdf = pd.DataFrame(ds.to_numpy())
    assert (
        wdf.shape[0] == df.shape[0]
    ), "Dataframe and RooDataSet have different number of entries"

    # Reset index and concatenated id columns with wdf
    ndf = pd.concat(
        [df.reset_index(drop=True)[["runNumber", "eventNumber", "candNumber"]], wdf],
        axis=1,
    )

    # Save to pickle file
    of = os.path.join(Locations.get_conditions_dir(), "weights", f"{filename}.pkl")
    ndf.to_pickle(of)
    logging.info(f"Saved sWeights to {of}")


def main():

    args = parse_arguments()

    logging.basicConfig(level=logging.INFO)

    mcsamples = KeyValMap(data=get_eventtypes())

    try:
        eventtype, samplename = mcsamples.get_pair(args.mcsample)
    except Exception as e:
        print(e)
        exit(1)

    nameid = f"{samplename}_{args.decay}"
    inputf = os.path.join(Locations.get_df_dir(), f"{nameid}.pkl")

    ws = workspaces.pizero_workspace("Pi0_fit", "Pi0 fit")
    pimass = ws.var("pimass")

    pizrange = (pimass.getMin(), pimass.getMax())
    df = configure_dataframe(inputf, args.decay, pizrange, xwindow=args.xwindow)
    logging.info(f"Dataframe  with {df.shape[0]} entries")

    b2gm, b1gm, b0gm = configure_datasets(
        ws, df, args.decay, xrange=XRANGE, brange=BRANGE[args.decay]
    )

    sigmodel = ws.pdf("Pi0_sig")
    bkgmodel = ws.pdf("Pi0_bkg")
    bkgmodelext = ws.pdf("Pi0_bkg_ext")

    sigparams = ws.set("sigparams")
    bkgparams = ws.set("bkgparams")
    bkgparamsext = bkgmodelext.getParameters({pimass})

    ws.saveSnapshot("Pi0_sig_init", sigparams, True)
    ws.saveSnapshot("Pi0_bkg_init", bkgparams, True)

    ds_sig = ws.data("data_sig")
    ds_bkg = ws.data("data_bkg")
    ds_b1g = ws.data("data_b1g")
    ds_b0g = ws.data("data_b0g")

    r_sig = fit_signal_shape(ds_sig, sigmodel, sigparams)

    ws.saveSnapshot("Pi0_sig_fit", sigparams, True)  # Save the signal model parameters
    plot_fit(pimass, ds_sig, sigmodel, f"{nameid}_Pi0_fit_sig")
    plot_fit(pimass, ds_sig, sigmodel, f"{nameid}_Pi0_fit_sig_log", logy=True)

    # Fit the combined 1 true g, 0 true g background
    # and add sWeights to the datasets
    ws.loadSnapshot("Pi0_sig_fit")
    r_bkg = fit_sigbkg_shape(ds_bkg, bkgmodel, sigparams)
    ws.saveSnapshot("Pi0_bkg_fit", bkgparams, True)
    plot_fit(pimass, ds_bkg, bkgmodel, f"{nameid}_Pi0_fit_bkg")
    add_bkg_sweights(ds_bkg, bkgmodelext, bkgparamsext)
    save_sweights(ds_bkg, df[b0gm | b1gm], f"{nameid}_Pi0_fit_bkg_sweights")

    # Fit the 1 true g background shape
    # and add sWeights to the datasets
    ws.loadSnapshot("Pi0_bkg_init")
    ws.loadSnapshot("Pi0_sig_fit")
    r_b1g = fit_sigbkg_shape(ds_b1g, bkgmodel, sigparams)
    ws.saveSnapshot("Pi0_b1g_fit", bkgparams, True)
    plot_fit(pimass, ds_b1g, bkgmodel, f"{nameid}_Pi0_fit_b1g")
    add_bkg_sweights(ds_b1g, bkgmodelext, bkgparamsext)
    save_sweights(ds_b1g, df[b1gm], f"{nameid}_Pi0_fit_b1g_sweights")
    # ds_b1g.Print("v")

    # Fit the 0 true g background shape
    ws.loadSnapshot("Pi0_bkg_init")
    ws.loadSnapshot("Pi0_sig_fit")
    r_b0g = fit_sigbkg_shape(ds_b0g, bkgmodel, sigparams)
    ws.saveSnapshot("Pi0_b0g_fit", bkgparams, True)
    plot_fit(pimass, ds_b0g, bkgmodel, f"{nameid}_Pi0_fit_b0g")
    add_bkg_sweights(ds_b0g, bkgmodelext, bkgparamsext)
    save_sweights(ds_b0g, df[b0gm], f"{nameid}_Pi0_fit_b0g_sweights")
    # ds_b0g.Print("v")

    # Status of the fit
    logging.info(
        f"Signal shape fit status: {r_sig.status()}, covQual: {r_sig.covQual()}"
    )
    logging.info(
        f"Background shape fit status: {r_bkg.status()}, covQual: {r_bkg.covQual()}"
    )
    logging.info(
        f"1 true g background shape fit status: {r_b1g.status()}, covQual: {r_b1g.covQual()}"
    )
    logging.info(
        f"0 true g background shape fit status: {r_b0g.status()}, covQual: {r_b0g.covQual()}"
    )

    # Save the workspace
    ws.writeToFile(
        os.path.join(Locations.get_conditions_fitws_dir(), f"{nameid}_Pi0_fit_ws.root")
    )
    logging.info(f"Workspace saved to {nameid}_Pi0_fit_ws.root")

    # f = ROOT.TFile.Open(
    #    os.path.join(Locations.get_wsfits_dir(), f'{nameid}_Pi0_fit_ws.root'))
    # ws_check = f.Get('Pi0_fit')
    # d = ws_check.data('data_bkg')
    # d.Print("v")


if __name__ == "__main__":
    main()
