import os
import argparse
import importlib
import logging

import pandas as pd


from omegamumu.common.utils import KeyValMap
from omegamumu.datasets.config import get_eventtypes
from omegamumu.variables import multiplicity, neutrals
from omegamumu.common.histograms import *
from omegamumu.corrections import load_reweighter

from locations import Locations
from processing import filter_dataframe


DECAY_TO_CANDIDATE = {
    "omegamumu": ("B", "omega", "#pi^{+}#pi^{-}"),
    "kstplusmumu": ("Bplus", "kstplus", "K^{+}"),
    "rhoplusmumu": ("Bplus", "rhoplus", "#pi^{+}"),
}


def setup_logging():
    logging.basicConfig(level=logging.INFO)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "decay",
        choices=["omegamumu", "kstplusmumu", "rhoplusmumu"],
        help="Decay channel",
    )
    parser.add_argument(
        "xwindow",
        choices=["eta", "omega", "kst", "rho", "phi"],
        help=" x meson mass window",
    )
    parser.add_argument("mcsample", help='Event type or "name" of the MC sample')
    parser.add_argument("massvar", choices=["bmass", "bmass-piz", "bmass-psi"])
    parser.add_argument(
        "--gbreweight", action="store_true", help="Apply gradient boosting reweighting"
    )
    args = parser.parse_args()
    return args


def prepare_mc_df(args):
    mcsamples = KeyValMap(data=get_eventtypes())
    try:
        _, samplename = mcsamples.get_pair(args.mcsample)
    except Exception as e:
        print(e)
        exit(1)

    x_cand = DECAY_TO_CANDIDATE[args.decay][1]
    mc_prefix = f"{samplename}_{args.decay}"

    df = pd.read_pickle(os.path.join(Locations.get_df_dir(), f"{mc_prefix}.pkl"))

    df = filter_dataframe(df, args.decay, x_cand=x_cand, xwindow=args.xwindow)
    df.query("true_b2g", inplace=True)

    return df, mc_prefix


def prepare_data_df(args):
    df = pd.read_pickle(os.path.join(Locations.get_df_dir(), f"{args.decay}_jpsi.pkl"))

    data_prefix = f"{args.decay}_{args.xwindow}_{args.massvar}_sweights"
    df_sw = pd.read_pickle(
        os.path.join(Locations.get_conditions_weights_dir(), f"{data_prefix}.pkl")
    )

    m_df = pd.merge(
        df_sw, df, on=["runNumber", "eventNumber", "candNumber"], how="inner"
    )

    return m_df


def prepare_reweighter_inputs(df):
    arr_dict = {
        "nTracks": df["nTracks"],
        "nSPDHits": df["gamma1_PP_CaloNeutralSpd"] + df["gamma2_PP_CaloNeutralSpd"],
        "pimass": df["pi0_M"],
    }
    return pd.DataFrame.from_dict(arr_dict)


def main():

    setup_logging()

    args = parse_args()

    mc_df, mc_prefix = prepare_mc_df(args)

    data_df = prepare_data_df(args)

    try:
        variables = importlib.import_module(f"omegamumu.variables.{args.decay}")
    except ImportError:
        raise ValueError(f"Decay {args.decay} not recognized")

    kintop = variables.kintop
    isolation = variables.isolation
    pids = variables.pids
    pidsgen = variables.pidsgen
    # pidscorr = variables.pidscorr

    wmc = mc_df["kin_weight"] * mc_df["tistos_weight"] * mc_df["trk_weight"]
    # wmc = np.ones(len(mc_df))
    wdata = data_df["B_peak_Nsig_sw"]

    pidgen_sel = mc_df["pidgen_sel"]
    pidcorr_sel = mc_df["pidcorr_sel"]

    if args.gbreweight:
        gbr = load_reweighter(
            os.path.join(Locations.get_conditions_corrections_dir(), "gammapid"),
            "Bplus2KstPlusJpsi_gbreweighter",
        )
        wmc = wmc * gbr.predict_weights(prepare_reweighter_inputs(mc_df))

    h_kintop_inputs_mc = {var.name: hist_from_var(var, mc_df) for var in kintop}
    h_kintop_inputs_corrmc = {
        var.name: hist_from_var(var, mc_df, weight=wmc) for var in kintop
    }
    h_kintop_inputs_data = {
        var.name: hist_from_var(var, data_df, weight=wdata) for var in kintop
    }

    h_pid_mc = {var.name: hist_from_var(var, mc_df) for var in pids}
    h_pidgen_mc = {
        var.name: hist_from_var(var, mc_df[pidgen_sel], weight=wmc[pidgen_sel])
        for var in pidsgen
    }
    # h_pidcorr_mc = {
    #    var.name: hist_from_var(var, mc_df[pidcorr_sel], weight=wmc[pidcorr_sel])
    #    for var in pidscorr
    # }
    h_pid_data = {var.name: hist_from_var(var, data_df, weight=wdata) for var in pids}

    h_isolation_mc = {var.name: hist_from_var(var, mc_df) for var in isolation}
    h_isolation_corrmc = {
        var.name: hist_from_var(var, mc_df, weight=wmc) for var in isolation
    }
    h_isolation_data = {
        var.name: hist_from_var(var, data_df, weight=wdata) for var in isolation
    }

    h_multiplicity_mc = {var.name: hist_from_var(var, mc_df) for var in multiplicity}
    h_multiplicity_corrmc = {
        var.name: hist_from_var(var, mc_df, weight=wmc) for var in multiplicity
    }
    h_multiplicity_data = {
        var.name: hist_from_var(var, data_df, weight=wdata) for var in multiplicity
    }

    h_neutrals_mc = {var.name: hist_from_var(var, mc_df) for var in neutrals}
    h_neutrals_corrmc = {
        var.name: hist_from_var(var, mc_df, weight=wmc) for var in neutrals
    }
    h_neutrals_data = {
        var.name: hist_from_var(var, data_df, weight=wdata) for var in neutrals
    }

    colors = ["red", "m", "black"]
    histtypes = ["step", "step", "errorbar"]
    legends = ["MC", "MC corr.", "DATA"]
    # markers = ['s', 's', 'o']

    from omegamumu.common.plotting import set_lhcb_style, save_fig

    set_lhcb_style(size=12)

    plotdir = Locations.get_plots_dir()

    multi_hist1d_comparison(
        (h_kintop_inputs_mc, h_kintop_inputs_corrmc, h_kintop_inputs_data),
        legends,
        histtypes,
        colors,
        plot_height=4,
        plot_width=4,
    )
    save_fig(f"{mc_prefix}_datamc_kintop", plotdir, fig_extension="png")

    multi_hist1d_comparison(
        (h_pid_mc, h_pidgen_mc, h_pid_data),
        legends,
        histtypes,
        colors,
        plot_height=5,
        plot_width=5,
    )
    save_fig(f"{mc_prefix}_datamc_pidsgen", plotdir, fig_extension="png")

    # multi_hist1d_comparison(
    #    (h_pid_mc, h_pidcorr_mc, h_pid_data),
    #    legends,
    #    histtypes,
    #    colors,
    #    plot_height=5,
    #    plot_width=5,
    # )
    # save_fig(f"{mc_prefix}_datamc_pidscorr", plotdir, fig_extension="png")

    multi_hist1d_comparison(
        (h_multiplicity_mc, h_multiplicity_corrmc, h_multiplicity_data),
        legends,
        histtypes,
        colors,
        plot_height=4,
        plot_width=4,
    )
    save_fig(f"{mc_prefix}_datamc_multiplicity", plotdir, fig_extension="png")

    multi_hist1d_comparison(
        (h_isolation_mc, h_isolation_corrmc, h_isolation_data),
        legends,
        histtypes,
        colors,
        plot_height=4,
        plot_width=4,
    )
    save_fig(f"{mc_prefix}_datamc_isolation", plotdir, fig_extension="png")

    multi_hist1d_comparison(
        (h_neutrals_mc, h_neutrals_corrmc, h_neutrals_data),
        legends,
        histtypes,
        colors,
        plot_height=4,
        plot_width=4,
    )
    save_fig(f"{mc_prefix}_datamc_neutrals", plotdir, fig_extension="png")


if __name__ == "__main__":
    main()
