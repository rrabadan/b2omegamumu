import os
import argparse
import logging
import importlib

import numpy as np
import pandas as pd
from itertools import product

import omegamumu.datasets.local as datasets
import omegamumu.selection.reflections as reflections
from omegamumu.common.utils import KeyValMap
from omegamumu.datasets.config import get_eventtypes
from omegamumu.variables import neutrals, multiplicity, get_var_input_branches
from locations import Locations
from processing import load_and_process_sim_files

# psiID = 443 if not args.psi2s else 100443
eventype_mctruth_map = {
    "11144405": (("B", 511), ("Jpsi", 443), ("omega", 223)),
    "11144432": (("B", 511), ("Jpsi", 443), ("omega", 221)),
    "13144401": (("B", 531), ("Jpsi", 443), ("omega", 221)),
    "13144405": (("B", 531), ("Jpsi", 443), ("omega", 223)),
    "13244410": (("B", 531), ("Jpsi", 443), ("omega", 333)),
    "12143401": (("Bplus", 521), ("Jpsi", 443), ("kstplus", 323), ("kplus", 321)),
    "12143491": (("Bplus", 521), ("Jpsi", 443), ("rhoplus", 213), ("piplus", 211)),
    "11114401": (("B", 511), ("omega", 223)),
    "12113460": (("Bplus", 521), ("rhoplus", 213), ("piplus", 211)),
}

eventype_map = {
    "11144405": ("omega", "jpsi"),
    "11144432": ("eta", "jpsi"),
    "13144401": ("eta", "jpsi"),
    "13144405": ("omega", "jpsi"),
    "13244410": ("phi", "jpsi"),
    "12143401": ("kst", "jpsi"),
    "12143491": ("rho", "jpsi"),
    "11114401": ("omega", "rare"),
    "12113460": ("rho", "rare"),
}


def setup_logging():
    logging.basicConfig(level=logging.INFO)


def parse_args():
    parser = argparse.ArgumentParser(
        description="Prepare df for simulation resonant samples"
    )
    parser.add_argument("mcsample", help='Event type or "name" of the MC sample')
    parser.add_argument(
        "decay",
        choices=["omegamumu", "kstplusmumu", "rhoplusmumu"],
        help="Decay channel",
    )
    parser.add_argument(
        "--year", nargs="+", default=["2016", "2017", "2018"], help="Year to fit"
    )
    parser.add_argument(
        "--magpol", nargs="+", default=["magdown", "magup"], help="magnet polarity"
    )
    args = parser.parse_args()
    return args


def get_sample(args):
    mcsamples = KeyValMap(data=get_eventtypes())
    try:
        eventtype, samplename = mcsamples.get_pair(args.mcsample)
    except Exception as e:
        print(e)
        exit(1)
    return eventtype, samplename


def get_name_pattern(args):
    _, n = get_sample(args)
    name = "_".join([n, args.decay])
    if args.year == ["2016", "2017", "2018"]:
        return name
    return "_".join([name] + args.year)


def get_mctruth_func(decay, mode):
    import omegamumu.selection.mctruth as omctruth

    m_mode = "resonant" if mode in ["jpsi", "psi2s"] else "rare"
    if decay == "omegamumu":
        func = f"{decay}_{m_mode}_truth_info"
    else:
        func = f"kstplusmumu_{m_mode}_truth_info"

    try:
        return getattr(omctruth, func)
    except AttributeError:
        raise ValueError(f"Function {func} not found in omegamumu.selection.mctruth")


def prepare_df(args):

    eventtype, _ = get_sample(args)

    xres = eventype_map[eventtype][0]
    mode = eventype_map[eventtype][1]

    try:
        variables = importlib.import_module(f"omegamumu.variables.{args.decay}")
    except ImportError:
        raise ValueError(f"Decay {args.decay} not recognized")

    masses = variables.masses
    if mode == "psi2s":
        masses += [variables.B_Psi2sDTF_M]
    kintop = variables.kintop
    isolation = variables.isolation

    mctruth = get_mctruth_func(args.decay, mode)(*eventype_mctruth_map[eventtype])

    branches = get_var_input_branches(
        masses + kintop + multiplicity + isolation + neutrals
    )
    datadir = datasets.get_local_data_path(args.decay, eventtype)
    dfs = []

    for year, pol in product(args.year, args.magpol):

        main_file = os.path.join(datadir, f"{pol}_{year}.root")
        friend_file_pidgen = os.path.join(datadir, f"{pol}_{year}_pidgen.root")
        friend_file_pidcorr = os.path.join(datadir, f"{pol}_{year}_pidcorr.root")

        logging.info(f"Processing files for year {year} and polarization {pol}")

        df = load_and_process_sim_files(
            main_file,
            args.decay,
            mode,
            xres,
            year,
            pol,
            gfriend=friend_file_pidgen,
            cfriend=friend_file_pidcorr,
            branches=branches,
            mctruth=mctruth,
        )
        if df is not None:
            dfs.append(df)

    arr = pd.concat(dfs, ignore_index=True)
    del dfs

    try:
        reflections_array = getattr(reflections, args.decay)
    except AttributeError:
        raise ValueError(f"Function {args.decay} not found in reflections")
    varr = reflections_array(arr)
    logging.info(f"Adding reflection variables: {list(varr.keys())}")

    arr = pd.concat([arr, pd.DataFrame(varr)], axis=1)

    return arr


def main():

    setup_logging()

    args = parse_args()

    df = prepare_df(args)

    df_dir = Locations.get_df_dir()
    outname = get_name_pattern(args)
    df.to_pickle(os.path.join(df_dir, outname + ".pkl"))
    logging.info(f"Saved df to {df_dir}/{outname}.pkl")


if __name__ == "__main__":
    main()
