import os
import argparse
import logging

import numpy as np
import pandas as pd

import ROOT

import omegamumu.fitting.plots as plotter

from omegamumu.common.utils import KeyValMap
from omegamumu.datasets.config import get_eventtypes
from omegamumu.selection.selection import xcand_window

from locations import Locations

BRANGE = (5100, 6000)
BRANGE = (4900, 5900)

DECAY_TO_CANDIDATE = {
    "omegamumu": ("B", "omega", "#pi^{+}#pi^{-}"),
    "kstplusmumu": ("Bplus", "kstplus", "K^{+}"),
    "rhoplusmumu": ("Bplus", "rhoplus", "#pi^{+}"),
}


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "decay", choices=["omegamumu", "kstplusmumu"], help="Decay channel"
    )
    parser.add_argument(
        "xwindow",
        nargs="?",
        default=None,
        choices=["eta", "omega", "kst", "rho", "phi"],
        help=" x meson mass window",
    )
    parser.add_argument(
        "mcsample", nargs="+", help='Event type or "name" of the MC sample'
    )
    return parser.parse_args()


def setup_logging():
    logging.basicConfig(level=logging.INFO)


def get_sample(mcsample):
    mcsamples = KeyValMap(data=get_eventtypes())
    try:
        _, samplename = mcsamples.get_pair(mcsample)
    except Exception as e:
        print(e)
        exit(1)

    return samplename


def configure_dataframe(file, decay, xwindow=None):

    df = pd.read_pickle(file)
    logging.info(f"Loaded {file}")
    logging.info(df.shape)

    # df.query('pidsel', inplace=True)

    decay_veto_cuts = {
        "omegamumu": "mumuphi_veto & mumukst_veto",
        "kstplusmumu": "mumukplus_veto & mumupiplus_veto",
        "rhoplusmumu": "mumukplus_veto & mumupiplus_veto",
    }

    # df.query(decay_veto_cuts[decay], inplace=True)

    b_cand = DECAY_TO_CANDIDATE[decay][0]
    x_cand = DECAY_TO_CANDIDATE[decay][1]

    # df.query(f'{b_cand}_BKGCAT == 50 | {b_cand}_BKGCAT == 40', inplace=True)
    df.query(f"{b_cand}_BKGCAT == 40", inplace=True)

    # x0 mass window
    if xwindow is not None:
        xwindow = xcand_window(x_cand, xwindow, op="&")
        df.query(xwindow, inplace=True)
        logging.info(df.shape)

    # Select columns
    cols = [
        f"{b_cand}_M",
        f"{b_cand}_piz_DTF_M",
        f"{b_cand}_Jpsi_DTF_M",
        f"{x_cand}_M",
        "pi0_M",
    ]
    # df = df[cols]
    df = df.loc[:, cols].copy()

    # rename columns
    df.rename(
        columns={
            f"{b_cand}_M": "bmass",
            f"{b_cand}_piz_DTF_M": "bmass-piz",
            f"{b_cand}_Jpsi_DTF_M": "bmass-psi",
            f"{x_cand}_M": "xmass",
            "pi0_M": "pimass",
        },
        inplace=True,
    )

    return df


def configure_variables(decay, brange):
    bmin, bmax = brange

    x_title = DECAY_TO_CANDIDATE[decay][2]

    bmass = ROOT.RooRealVar(
        "bmass",
        "m(#mu^{+} #mu^{-} " + x_title + " #gamma #gamma) (MeV/c^{2})",
        bmin,
        bmax,
    )
    bmasspiz = ROOT.RooRealVar(
        "bmass-piz", "m(#mu^{+}#mu^{-}" + x_title + "#pi^{0}) (MeV/c^{2})", bmin, bmax
    )
    bmasspsi = ROOT.RooRealVar(
        "bmass-psi", "m(J/#psi " + x_title + " #pi^{0}) (MeV/c^{2})", bmin, bmax
    )

    return bmass, bmasspiz, bmasspsi


def fill_dataset(df, name, bmass, bmasspiz, bmasspsi):
    ds = ROOT.RooDataSet.from_numpy(
        {
            "bmass": df["bmass"].to_numpy(),
            "bmass-piz": df["bmass-piz"].to_numpy(),
            "bmass-psi": df["bmass-psi"].to_numpy(),
        },
        [bmass, bmasspiz, bmasspsi],
        name=name,
    )
    return ds


def configure_workspace(name, title, df, decay, brange=BRANGE):

    ws = ROOT.RooWorkspace(name, title)

    bmass, bmasspiz, bmasspsi = configure_variables(decay, brange)

    logging.info(f"Dataframe  with {df.shape[0]} entries")

    # Create datasets from the dataframes
    ds = fill_dataset(df, "data", bmass, bmasspiz, bmasspsi)
    ws.Import(ds)

    return ws


def plot_fit(mass, data, model, name, extension="png"):
    mframe, pulls = plotter.draw_frame(
        mass, data, model, pull_hist=True, params=True, layout=(0.4, 0.7, 0.4)
    )
    canvas = plotter.plot_frame_with_pulls(mframe, pulls)
    canvas.SaveAs(os.path.join(Locations.get_plots_dir(), f"{name}.{extension}"))


def fit_and_plot(ws, m_name, data, name):
    massvar = ws.var(m_name)
    pdf = ROOT.RooKeysPdf(
        f"kde_{m_name}", f"kde_{m_name}", massvar, data, ROOT.RooKeysPdf.NoMirror
    )
    plot_fit(massvar, data, pdf, f"{name}_{m_name}_kde", extension="png")
    ws.Import(pdf)


def main():

    args = parse_arguments()

    print(args.mcsample)

    setup_logging()

    samples = [get_sample(mcsample) for mcsample in args.mcsample]

    df = [
        configure_dataframe(
            os.path.join(Locations.get_df_dir(), f"{sample}_{args.decay}.pkl"),
            args.decay,
            args.xwindow,
        )
        for sample in samples
    ]

    df = pd.concat(df, ignore_index=True) if len(df) > 1 else df[0]

    nameid = (
        "".join(samples + [args.decay])
        if len(samples) > 1
        else "_".join([samples[0], args.decay])
    )
    if args.xwindow:
        nameid += f"_{args.xwindow}"

    ws = configure_workspace("B_bkg", "B_bkg", df, args.decay)

    ws.Print()

    ds = ws.data("data")

    fit_and_plot(ws, "bmass-psi", ds, nameid)
    fit_and_plot(ws, "bmass-piz", ds, nameid)
    fit_and_plot(ws, "bmass", ds, nameid)

    ws.writeToFile(
        os.path.join(Locations.get_conditions_fitws_dir(), f"{nameid}_kde_ws.root")
    )
    logging.info(f"Workspace saved to {Locations.get_conditions_fitws_dir()}")

    return


if __name__ == "__main__":
    main()
