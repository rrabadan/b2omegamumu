import os
import logging
import argparse
import pandas as pd

import matplotlib.pyplot as plt
from sklearn.pipeline import Pipeline

from omegamumu.classification import ColumnTransformer, load_model

from omegamumu.common.histograms import hist_from_var, plot_hist
from omegamumu.common.utils import KeyValMap
from omegamumu.common.plotting import set_lhcb_style, save_fig

from omegamumu.datasets.config import get_eventtypes
from locations import Locations

from omegamumu.variables import Var, neutrals
from omegamumu.variables.omegamumu import kintop, pidsgen

CLASSIFIER = "omegamumu_reduced_model.json"
DECAY = "omegamumu"
CONDDIR = os.path.join(Locations.get_conditions_dir(), "classification")


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("mcsample", help='Event type or "name" of the MC sample')
    return parser.parse_args()


def get_sample(args):
    mcsamples = KeyValMap(data=get_eventtypes())
    try:
        eventtype, samplename = mcsamples.get_pair(args.mcsample)
    except Exception as e:
        print(e)
        exit(1)
    return eventtype, samplename


def plot_scores(scores, name):
    set_lhcb_style()
    plot_dir = Locations.get_plots_dir()

    BDT = Var(
        name="BDT",
        branch="bdt_score",
        label="BDT",
        x_title="BDT",
        binning=(100, 0.0, 1),
    )
    h_bdt = hist_from_var(BDT, scores)
    fig, ax = plt.subplots(figsize=(4, 4))
    plot_hist(ax, h_bdt)
    save_fig(name, plot_dir, fig_extension="png")


def main():

    args = parse_arguments()

    logging.basicConfig(level=logging.INFO)

    _, samplename = get_sample(args)

    df = pd.read_pickle(
        os.path.join(Locations.get_df_dir(), f"{samplename}_{DECAY}.pkl")
    )
    logging.info(f"Loaded {df.shape[0]} events")

    classifier = load_model(os.path.join(CONDDIR, CLASSIFIER))

    vars = [
        f for f in neutrals + kintop + pidsgen if f.name in classifier.feature_names_
    ]

    transformer = ColumnTransformer(vars)
    pipeline = Pipeline(
        steps=[
            ("transformer", transformer),
            ("classifier", classifier),
        ]
    )

    scores = pipeline.predict_proba(df)

    bdt_df = transformer.id_df
    bdt_df["bdt_score"] = scores[:, 1]
    logging.info(f"Computed BDT scores for {bdt_df.shape[0]} events")

    bdt_df.to_pickle(os.path.join(CONDDIR, f"{samplename}_{DECAY}_bdt_scores.pkl"))
    logging.info(f"BDT scores saved to {CONDDIR}")

    plot_scores(bdt_df, f"{samplename}_{DECAY}_bdt_scores")
    logging.info(f"BDT scores plot saved to {Locations.get_plots_dir()}")


if __name__ == "__main__":
    main()
