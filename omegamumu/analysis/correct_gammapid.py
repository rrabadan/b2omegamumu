import os
import argparse
import importlib
import logging

import pandas as pd


from omegamumu.common.utils import KeyValMap
from omegamumu.datasets.config import get_eventtypes
from omegamumu.common.histograms import *
from omegamumu.variables import multiplicity, neutrals
from omegamumu.common.plotting import set_lhcb_style, save_fig
from omegamumu.corrections import create_reweighter, save_reweighter

from processing import filter_dataframe
from locations import Locations

set_lhcb_style(size=12)

DECAY_TO_CANDIDATE = {
    "omegamumu": ("B", "omega", "#pi^{+}#pi^{-}"),
    "kstplusmumu": ("Bplus", "kstplus", "K^{+}"),
    "rhoplusmumu": ("Bplus", "rhoplus", "#pi^{+}"),
}


def setup_logging():
    logging.basicConfig(level=logging.INFO)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "decay",
        choices=["omegamumu", "kstplusmumu", "rhoplusmumu"],
        help="Decay channel",
    )
    parser.add_argument(
        "xwindow",
        choices=["eta", "omega", "kst", "rho", "phi"],
        help=" x meson mass window",
    )
    parser.add_argument("mcsample", help='Event type or "name" of the MC sample')
    parser.add_argument(
        "massvar",
        choices=["bmass", "bmass-piz", "bmass-psi"],
        help="Fit mass variable",
    )
    args = parser.parse_args()
    return args


def prepare_mc_df(args):
    mcsamples = KeyValMap(data=get_eventtypes())
    try:
        _, samplename = mcsamples.get_pair(args.mcsample)
    except Exception as e:
        print(e)
        exit(1)

    x_cand = DECAY_TO_CANDIDATE[args.decay][1]
    mc_prefix = f"{samplename}_{args.decay}"

    df = pd.read_pickle(os.path.join(Locations.get_df_dir(), f"{mc_prefix}.pkl"))

    df = filter_dataframe(df, args.decay, x_cand=x_cand, xwindow=args.xwindow)
    df.query("true_b2g", inplace=True)

    return df, mc_prefix


def prepare_data_df(args):
    df = pd.read_pickle(os.path.join(Locations.get_df_dir(), f"{args.decay}_jpsi.pkl"))

    data_prefix = f"{args.decay}_{args.xwindow}_{args.massvar}_sweights"
    df_sw = pd.read_pickle(
        os.path.join(Locations.get_conditions_weights_dir(), f"{data_prefix}.pkl")
    )

    m_df = pd.merge(
        df_sw, df, on=["runNumber", "eventNumber", "candNumber"], how="inner"
    )

    return m_df


def prepare_reweighter_inputs(df):
    arr_dict = {
        "nTracks": df["nTracks"],
        "nSPDHits": df["gamma1_PP_CaloNeutralSpd"] + df["gamma2_PP_CaloNeutralSpd"],
        "pimass": df["pi0_M"],
        #'gamMinPT': np.minimum(df['gamma1_PT'], df['gamma2_PT']),
        #'gamMaxPT': np.maximum(df['gamma1_PT'], df['gamma2_PT']),
    }
    return pd.DataFrame.from_dict(arr_dict)


def main():

    setup_logging()

    args = parse_args()

    try:
        variables = importlib.import_module(f"omegamumu.variables.{args.decay}")
    except ImportError:
        raise ValueError(f"Decay {args.decay} not recognized")

    masses = variables.masses

    mc_df, mc_prefix = prepare_mc_df(args)

    data_df = prepare_data_df(args)

    wmc = mc_df["kin_weight"] * mc_df["tistos_weight"] * mc_df["trk_weight"]
    wdata = data_df["B_peak_Nsig_sw"]

    mc_inputs = prepare_reweighter_inputs(mc_df)
    data_inputs = prepare_reweighter_inputs(data_df)

    rw = create_reweighter(
        mc_inputs,
        data_inputs,
        original_weight=wmc,
        target_weight=wdata,
        n_folds=5,
    )

    w1 = rw.predict_weights(mc_inputs)
    w2 = rw.predict_weights(mc_inputs, original_weight=wmc)

    vars_gbr = multiplicity + [masses[2]]

    h_neutrals_mc = {var.name: hist_from_var(var, mc_df) for var in neutrals}
    h_neutrals_corrmc = {
        var.name: hist_from_var(var, mc_df, weight=w2) for var in neutrals
    }
    h_neutrals_data = {
        var.name: hist_from_var(var, data_df, weight=wdata) for var in neutrals
    }

    h_vargbr_mc = {var.name: hist_from_var(var, mc_df) for var in vars_gbr}
    h_vargbr_corrmc = {
        var.name: hist_from_var(var, mc_df, weight=w1) for var in vars_gbr
    }
    h_vargbr_data = {
        var.name: hist_from_var(var, data_df, weight=wdata) for var in vars_gbr
    }

    colors = ["red", "m", "black"]
    histtypes = ["step", "step", "errorbar"]
    legends = ["MC", "MC corr.", "DATA"]
    plotdir = Locations.get_plots_dir()

    multi_hist1d_comparison(
        (h_vargbr_mc, h_vargbr_corrmc, h_vargbr_data),
        legends,
        histtypes,
        colors,
        plot_height=4,
        plot_width=4,
    )
    save_fig(f"{mc_prefix}_gammapid_reweight_inputs", plotdir, fig_extension="png")

    multi_hist1d_comparison(
        (h_neutrals_mc, h_neutrals_corrmc, h_neutrals_data),
        legends,
        histtypes,
        colors,
        plot_height=4,
        plot_width=4,
    )
    save_fig(f"{mc_prefix}_gammapid_reweight_neutrals", plotdir, fig_extension="png")

    save_reweighter(
        rw,
        os.path.join(Locations.get_conditions_corrections_dir(), "gammapid"),
        f"{args.mcsample}_gbreweighter",
    )
    return


if __name__ == "__main__":
    main()
