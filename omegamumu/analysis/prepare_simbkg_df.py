import os
import argparse
import logging
import importlib

import numpy as np
import pandas as pd
from itertools import product
from matplotlib import pyplot as plt

import omegamumu.datasets.local as datasets
from omegamumu.common.utils import KeyValMap
from omegamumu.datasets.config import get_eventtypes
from omegamumu.variables import neutrals, multiplicity, get_var_input_branches
from locations import Locations
from processing import load_and_process_sim_files


def setup_logging():
    logging.basicConfig(level=logging.INFO)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("mcsample", help='Event type or "name" of the MC sample')
    parser.add_argument(
        "decay",
        choices=["omegamumu", "kstplusmumu", "rhoplusmumu"],
        help="Decay channel",
    )
    parser.add_argument(
        "mode", choices=["jpsi", "psi2s", "rare"], help="Dimuon decay mode"
    )
    parser.add_argument(
        "--year", nargs="+", default=["2016", "2017", "2018"], help="Year"
    )
    parser.add_argument(
        "--magpol", nargs="+", default=["magdown", "magup"], help="magnet polarity"
    )
    parser.add_argument(
        "--xwindow",
        nargs="?",
        default=None,
        choices=["eta", "omega", "kst", "rho", "phi"],
        help=" x meson mass window",
    )
    args = parser.parse_args()
    return args


def get_sample(args):
    mcsamples = KeyValMap(data=get_eventtypes())

    try:
        eventtype, samplename = mcsamples.get_pair(args.mcsample)
    except Exception as e:
        print(e)
        exit(1)

    return eventtype, samplename


def get_name_pattern(args):
    _, n = get_sample(args)
    name = (
        "_".join([n, args.decay, args.xwindow])
        if args.xwindow
        else "_".join([n, args.decay])
    )
    return name


def prepare_df(args):

    eventtype, _ = get_sample(args)

    try:
        variables = importlib.import_module(f"omegamumu.variables.{args.decay}")
    except ImportError:
        raise ValueError(f"Decay {args.decay} not recognized")

    masses = variables.masses
    if args.mode == "psi2s":
        masses += [variables.B_Psi2sDTF_M]
    kintop = variables.kintop
    isolation = variables.isolation

    branches = get_var_input_branches(
        masses + kintop + multiplicity + isolation + neutrals
    )
    pattern = r"^(.*TRUEID$|.*MOTHER_ID$|.*KEY$)"
    datadir = datasets.get_local_data_path(args.decay, eventtype)
    dfs = []

    for year, pol in product(args.year, args.magpol):
        main_file = os.path.join(datadir, f"{pol}_{year}.root")

        friend_file_pidgen = os.path.join(datadir, f"{pol}_{year}_pidgen.root")
        friend_file_pidcorr = os.path.join(datadir, f"{pol}_{year}_pidcorr.root")

        friend_file_pidgen = (
            friend_file_pidgen if os.path.exists(friend_file_pidgen) else None
        )
        friend_file_pidcorr = (
            friend_file_pidcorr if os.path.exists(friend_file_pidcorr) else None
        )

        df = load_and_process_sim_files(
            main_file,
            args.decay,
            args.mode,
            args.xwindow,
            year,
            pol,
            gfriend=friend_file_pidgen,
            cfriend=friend_file_pidcorr,
            branches=branches,
            br_pattern=pattern,
        )
        if df is not None:
            dfs.append(df)

    arr = pd.concat(dfs, ignore_index=True)
    del dfs

    return arr


def main():

    setup_logging()

    args = parse_args()

    df = prepare_df(args)

    df_dir = Locations.get_df_dir()
    outname = get_name_pattern(args)
    df.to_pickle(os.path.join(df_dir, outname + ".pkl"))
    logging.info(f"Saved df to {df_dir}/{outname}.pkl")


if __name__ == "__main__":
    main()
