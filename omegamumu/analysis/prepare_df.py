import os
import argparse
import importlib
import logging

import pandas as pd
from itertools import product

import omegamumu.selection.reflections as reflections
from omegamumu.datasets.config import config_instance
from omegamumu.datasets.remote import apd_dataset
from omegamumu.variables import neutrals, multiplicity, get_var_input_branches
from locations import Locations
from processing import load_and_process_data_files


def setup_logging():
    logging.basicConfig(level=logging.INFO)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "decay",
        choices=["omegamumu", "kstplusmumu", "rhoplusmumu"],
        type=str,
        help="Decay channel",
    )
    parser.add_argument(
        "mode",
        choices=["jpsi", "psi2s", "rare"],
        help="Dimuon region: jpsi, psi2s, or rare",
    )
    parser.add_argument(
        "--year",
        nargs="+",
        type=str,
        default=["2016", "2017", "2018"],
        help="Year to fit",
    )
    parser.add_argument(
        "--magpol",
        nargs="+",
        type=str,
        default=["magdown", "magup"],
        help="magnet polarity",
    )
    parser.add_argument(
        "--xwindow",
        nargs="?",
        default=None,
        choices=["eta", "omega", "kst", "rho", "phi"],
        help=" x meson mass window",
    )
    parser.add_argument("--apd-input", action="store_true", help="Use APD input files")
    parser.add_argument("--cut-report", action="store_true", help="Cuts report")
    args = parser.parse_args()

    return args


def get_name_pattern(args):
    name = "_".join([args.decay, args.mode])
    if args.xwindow:
        name += f"_{args.xwindow}"
    if args.year == ["2016", "2017", "2018"]:
        return name
    return "_".join([name] + args.year)


def get_local_input_files(args):
    from omegamumu.datasets.local import get_local_data_path

    datadir = get_local_data_path(args.decay, 90000000)
    files = [
        os.path.join(datadir, f"{pol}_{year}.root")
        for year, pol in product(args.year, args.magpol)
    ]
    return files, "DecayTree"


def get_apd_input_files(args):
    files = []
    for year, pol in product(args.year, args.magpol):
        dataset = apd_dataset(str(90000000), year, pol, decay=args.decay)
        files += dataset.keys
    treename = config_instance.get("tool", "analysis", "strippingtuples")[args.decay]
    return files, treename


def prepare_df(args):

    if args.apd_input:
        files, treename = get_apd_input_files(args)
    else:
        files, treename = get_local_input_files(args)
    logging.info(f"Processing {len(files)} files, treename: {treename}")

    try:
        variables = importlib.import_module(f"omegamumu.variables.{args.decay}")
    except ImportError:
        raise ValueError(f"Decay {args.decay} not recognized")

    masses = variables.masses
    if args.mode == "psi2s":
        masses += [variables.B_Psi2sDTF_M]
    kintop = variables.kintop
    isolation = variables.isolation

    branches = get_var_input_branches(
        masses + kintop + multiplicity + isolation + neutrals
    )

    report = None
    if args.cut_report:
        name_pattern = get_name_pattern(args)
        report = os.path.join(Locations.get_tables_dir(), f"{name_pattern}_cuts.json")
        logging.info(f"Writing cuts report to {report}")

    arr = load_and_process_data_files(
        files,
        args.decay,
        args.mode,
        xwindow=args.xwindow,
        branches=branches,
        treename=treename,
        cutreport=report,
    )

    try:
        reflections_array = getattr(reflections, args.decay)
    except AttributeError:
        raise ValueError(f"Function {args.decay} not found in reflections")

    varr = reflections_array(arr)
    logging.info(f"Adding reflection variables: {list(varr.keys())}")

    arr = pd.concat([arr, pd.DataFrame(varr)], axis=1)

    return arr


def main():

    setup_logging()

    args = parse_args()

    df = prepare_df(args)

    df_dir = Locations.get_df_dir()
    outname = get_name_pattern(args)
    df.to_pickle(os.path.join(df_dir, outname + ".pkl"))
    logging.info(f"Saved df to {df_dir}/{outname}.pkl")


if __name__ == "__main__":
    main()
