### README for `conditions/corrections`

## Overview 

This directory contains corrections to account for discrepancies between data and Monte Carlo (MC) simulations. 

The following corrections are provided by the [EWP-Bu2pimumu](https://gitlab.cern.ch/LHCb-RD/ewp-bu2pimumu) project.

- [`kin-hist`](kin-hist): $B$ kinematic corrections.

- [`muid`](muid): Muon ID efficiency.

- [`track`](track): Tracking efficiency.

- [`tistos`](tistos): Trigger efficiency.
