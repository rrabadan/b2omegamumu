### README for `conditions/weights`

## Overview
The `conditions/weights` directory contains DataFrames that include various weights used in the analysis. Each DataFrame is provided with the corresponding `RunNumber`, `eventNumber`, and `candNumber` to uniquely identify each event.

## Contents
- **DataFrames**: Each file in this directory is a pkl file containing a DataFrame with different weights.
- **Identifiers**: The DataFrames include the following columns to uniquely identify each event:
  - `RunNumber`: The run number of the event.
  - `eventNumber`: The event number within the run.
  - `candNumber`: The candidate number within the event.

## Usage
These DataFrames are used to apply weights by merging dataframes.


## How to Use
1. **Load the DataFrame**: Use pandas to load the pickle file into a DataFrame.
   ```python
   import pandas as pd
   df_weights = pd.read_pickle('path/to/conditions/weights/weight_file.pkl')
   ```
2. **Merge DataFrames**: Use the `RunNumber`, `eventNumber`, and `candNumber` columns to merge on
   ```
   merged_df = pd.merge(
       df_weights, df, on=["runNumber", "eventNumber", "candNumber"], how="inner",
   )
   ```
