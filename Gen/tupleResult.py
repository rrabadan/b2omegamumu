from Configurables import (
    DaVinci,
    EventSelector,
    PrintMCTree,
    MCDecayTreeTuple
)
from DecayTreeTuple.Configuration import *

"""Configure the variables below with:
decay: Decay you want to inspect, using 'newer' LoKi decay descriptor syntax,
decay_heads: Particles you'd like to see the decay tree of,
datafile: Where the file created by the Gauss generation phase is, and
year: What year the MC is simulating.
"""

# https://twiki.cern.ch/twiki/bin/view/LHCb/FAQ/LoKiNewDecayFinders
decay = '[B0 ==> ^(J/psi(1S) ==> ^mu+ ^mu-) ^(K_1(1270)0 ==> ^(KS0 ==> ^pi0 ^pi0) ^(rho(770)0 ==> ^pi+ ^pi-))]CC'
decay_heads = ['B0', 'B~0']
#datafile = 'GaussDev_v49r17/Gauss-11142430-10ev-20231105.xgen'
year = 2018

mc_basic_loki_vars = {
    'ETA': 'MCETA',
    'PHI': 'MCPHI',
    'PT': 'MCPT',
    'PX': 'MCPX',
    'PY': 'MCPY',
    'PZ': 'MCPZ',
    'E': 'MCE',
    'P': 'MCP',
    'THETA': 'MCTHETA',
    'ID': 'MCID'
}

# For a quick and dirty check, you don't need to edit anything below here.
##########################################################################

# Create an MC DTT containing any candidates matching the decay descriptor
mctuple = MCDecayTreeTuple('MCDecayTreeTuple')
mctuple.Decay = decay
mctuple.ToolList = [
    #'MCTupleToolHierarchy',
    #'LoKi::Hybrid::MCTupleTool/LoKi_Photos'
]

mctuple.addTupleTool(
    'LoKi::Hybrid::MCTupleTool/basicLoKiTT'
).Variables = mc_basic_loki_vars
    
mctuple.setDescriptorTemplate('${B}[B0 ==> ${Jpsi}(J/psi(1S) ==> ${muplus}mu+ ${muminus}mu-) ${K1}(K_1(1270)0 ==> ${KS}(KS0 ==> ${pizero_1}pi0 ${pizero_2}pi0) ${rho}(rho(770)0 => ${piplus}pi+ ${piminus}pi-))]CC'
)

# Add a 'number of photons' branch
# tuple.addTupleTool("MCTupleToolKinematic").Verbose = True
# mctuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Photos").Variables = {
#    "nPhotos": "MCNINTREE(('gamma' == MCABSID))"
#}

# Print the decay tree for any particle in decay_heads
# printMC = PrintMCTree()
# printMC.ParticleNames = decay_heads

# Name of the .xgen file produced by Gauss
files = [
    '/afs/cern.ch/work/r/rrabadan/gangadir/workspace/rrabadan/LocalXML/39/0/output/Gauss-11142430-10000ev-20231105.xgen',
    '/afs/cern.ch/work/r/rrabadan/gangadir/workspace/rrabadan/LocalXML/39/10/output/Gauss-11142430-10000ev-20231105.xgen',
    '/afs/cern.ch/work/r/rrabadan/gangadir/workspace/rrabadan/LocalXML/39/11/output/Gauss-11142430-10000ev-20231105.xgen',
    '/afs/cern.ch/work/r/rrabadan/gangadir/workspace/rrabadan/LocalXML/39/12/output/Gauss-11142430-10000ev-20231105.xgen',
    '/afs/cern.ch/work/r/rrabadan/gangadir/workspace/rrabadan/LocalXML/39/13/output/Gauss-11142430-10000ev-20231105.xgen',
    '/afs/cern.ch/work/r/rrabadan/gangadir/workspace/rrabadan/LocalXML/39/14/output/Gauss-11142430-10000ev-20231105.xgen',
    '/afs/cern.ch/work/r/rrabadan/gangadir/workspace/rrabadan/LocalXML/39/15/output/Gauss-11142430-10000ev-20231105.xgen',
    '/afs/cern.ch/work/r/rrabadan/gangadir/workspace/rrabadan/LocalXML/39/16/output/Gauss-11142430-10000ev-20231105.xgen',
    '/afs/cern.ch/work/r/rrabadan/gangadir/workspace/rrabadan/LocalXML/39/17/output/Gauss-11142430-10000ev-20231105.xgen',
    '/afs/cern.ch/work/r/rrabadan/gangadir/workspace/rrabadan/LocalXML/39/18/output/Gauss-11142430-10000ev-20231105.xgen',
    '/afs/cern.ch/work/r/rrabadan/gangadir/workspace/rrabadan/LocalXML/39/19/output/Gauss-11142430-10000ev-20231105.xgen',
    '/afs/cern.ch/work/r/rrabadan/gangadir/workspace/rrabadan/LocalXML/39/1/output/Gauss-11142430-10000ev-20231105.xgen',
    '/afs/cern.ch/work/r/rrabadan/gangadir/workspace/rrabadan/LocalXML/39/2/output/Gauss-11142430-10000ev-20231105.xgen',
    '/afs/cern.ch/work/r/rrabadan/gangadir/workspace/rrabadan/LocalXML/39/3/output/Gauss-11142430-10000ev-20231105.xgen',
    '/afs/cern.ch/work/r/rrabadan/gangadir/workspace/rrabadan/LocalXML/39/4/output/Gauss-11142430-10000ev-20231105.xgen',
    '/afs/cern.ch/work/r/rrabadan/gangadir/workspace/rrabadan/LocalXML/39/5/output/Gauss-11142430-10000ev-20231105.xgen',
    '/afs/cern.ch/work/r/rrabadan/gangadir/workspace/rrabadan/LocalXML/39/6/output/Gauss-11142430-10000ev-20231105.xgen',
    '/afs/cern.ch/work/r/rrabadan/gangadir/workspace/rrabadan/LocalXML/39/7/output/Gauss-11142430-10000ev-20231105.xgen',
    '/afs/cern.ch/work/r/rrabadan/gangadir/workspace/rrabadan/LocalXML/39/8/output/Gauss-11142430-10000ev-20231105.xgen',
    '/afs/cern.ch/work/r/rrabadan/gangadir/workspace/rrabadan/LocalXML/39/9/output/Gauss-11142430-10000ev-20231105.xgen'
]
EventSelector().Input = [
    "DATAFILE='{0}' TYP='POOL_ROOTTREE' Opt='READ'".format(file) for file in files
]

# Configure DaVinci
DaVinci().TupleFile = "DVntuple.root"
DaVinci().Simulation = True
DaVinci().Lumi = False
DaVinci().DataType = str(year)
# DaVinci().UserAlgorithms = [printMC, mctuple]
DaVinci().UserAlgorithms = [mctuple]

from Gaudi.Configuration import appendPostConfigAction
def doIt():
    """
    specific post-config action for (x)GEN-files 
    """
    extension = "xgen"
    ext = extension.upper()

    from Configurables import DataOnDemandSvc
    dod  = DataOnDemandSvc ()
    from copy import deepcopy 
    algs = deepcopy ( dod.AlgMap ) 
    bad  = set() 
    for key in algs :
        if     0 <= key.find ( 'Rec'     )                  : bad.add ( key )
        elif   0 <= key.find ( 'Raw'     )                  : bad.add ( key )
        elif   0 <= key.find ( 'DAQ'     )                  : bad.add ( key )
        elif   0 <= key.find ( 'Trigger' )                  : bad.add ( key )
        elif   0 <= key.find ( 'Phys'    )                  : bad.add ( key )
        elif   0 <= key.find ( 'Prev/'   )                  : bad.add ( key )
        elif   0 <= key.find ( 'Next/'   )                  : bad.add ( key )
        elif   0 <= key.find ( '/MC/'    ) and 'GEN' == ext : bad.add ( key )
        
    for b in bad :
        del algs[b]
            
    dod.AlgMap = algs
    
    from Configurables import EventClockSvc, CondDB 
    EventClockSvc ( EventTimeDecoder = "FakeEventTime" )
    CondDB  ( IgnoreHeartBeat = True )
    
appendPostConfigAction( doIt )
