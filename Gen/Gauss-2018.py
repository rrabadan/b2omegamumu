##############################################################################
# File for running Gauss with Sim09 configuration and beam conditions as in
# production for 2018 data (6.5 TeV beams, nu=1.6, spill-over)
#
# Syntax is:
#   gaudirun.py Gauss-2018.py <someInputJobConfiguration>.py
##############################################################################

#--Pick beam conditions as set in AppConfig
from Gaudi.Configuration import *
importOptions("$APPCONFIGOPTS/Gauss/Beam6500GeV-md100-2018-nu1.6.py")
#importOptions("$APPCONFIGOPTS/Gauss/EnableSpillover-25ns.py")
importOptions("$APPCONFIGOPTS/Gauss/DataType-2018.py")
importOptions("$APPCONFIGOPTS/Gauss/RICHRandomHits.py")


#--Set database tags
importOptions("$GAUSSOPTS/DBTags-2018.py")

