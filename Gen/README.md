# PGuns

## Env setup

```
lb-set-platform x86_64-slc6-gcc49-opt
lb-dev Gauss/v49r17
cd ./GaussDev_v49r17
make install
```
## Run PGun

```
./run bash
gaudirun.py $GAUSSOPTS/Gauss-Job.py Gauss-2018.py $GAUSSOPTS/GenStandAlone.py $DECFILESROOT/options/11142430.py $LBPGUNSROOT/options/PGuns.py
```
