import os
import argparse
import json

parser = argparse.ArgumentParser(description='ganga submit script mc')
parser.add_argument('year', type=str,
                    choices=['2011', '2012', '2015', '2016', '2017', '2018'])
parser.add_argument('magpol', type=str,
                    choices=['MagDown', 'MagUp'])
parser.add_argument('--decay', type=str, nargs='?',
                    choices=['B2omegaMuMu'],
                    default='B2omegaMuMu')
parser.add_argument('--options', type=str, nargs='?',
                    default=None)
parser.add_argument('--filesperjob', type=int, nargs='?',
                    default=50)
parser.add_argument('--test', action='store_true')
parser.add_argument('--name', type=str, nargs='?',
                    default=None)
parser.add_argument('--devpath', type=str, nargs='?',
                    default='.')
parser.add_argument('--dv', type=str, nargs='?',
                    default='v46r7')
parser.add_argument('--platform', type=str, nargs='?',
                    default='x86_64_v2-centos7-gcc12-opt')
args = parser.parse_args()

datasets = {
    'MagDown_2018': '/LHCb/Collision18/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco18/Stripping34r0p2/90000000/LEPTONIC.MDST',
    'MagUp_2018': '/LHCb/Collision18/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco18/Stripping34r0p2/90000000/LEPTONIC.MDST',
    'MagDown_2017': '/LHCb/Collision17/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco17/Stripping29r2p2/90000000/LEPTONIC.MDST',
    'MagUp_2017': '/LHCb/Collision17/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco17/Stripping29r2p2/90000000/LEPTONIC.MDST',
    'MagDown_2016': '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping28r2p1/90000000/LEPTONIC.MDST',
    'MagUp_2016': '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Stripping28r2p1/90000000/LEPTONIC.MDST',
    'MagDown_2015': '/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24r2/90000000/LEPTONIC.MDST',
    'MagUp_2015': '/LHCb/Collision15/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco15a/Stripping24r2/90000000/LEPTONIC.MDST',
    'MagDown_2012': '/LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r0p2/90000000/LEPTONIC.MDST',
    'MagUp_2012': '/LHCb/Collision12/Beam4000GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21r0p2/90000000/LEPTONIC.MDST',
    'MagDown_2011': '/LHCb/Collision11/Beam3500GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r1p2/90000000/LEPTONIC.MDST',
    'MagUp_2011': '/LHCb/Collision11/Beam3500GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21r1p2/90000000/LEPTONIC.MDST'
}

bkloc = datasets[f'{args.magpol}_{args.year}']

name = args.name
optsfile = args.options

if name is None:
    name = '_'.join([args.decay, args.magpol, args.year])
if optsfile is None:
    optsfile = f'{args.decay}Ntuple.py'

# Write extra opts
extraopts = os.path.join('extraopts', f'{name}.py')
if not os.path.exists('./extraopts'):
    os.makedirs('./extraopts')

with open(extraopts, 'w') as f:
    f.write("from Configurables import DaVinci\n")
    f.write("DaVinci().InputType = 'MDST'\n")
    f.write(f"DaVinci().DataType = '{args.year}'\n")
    f.write("DaVinci().Simulation = False\n")
    f.write(f"DaVinci().TupleFile = '{args.decay}.root'\n")

devarea = os.path.join(args.devpath, f'DaVinciDev_{args.dv}')
if not os.path.exists(devarea):
    print("No Dev ", devarea)
    app = prepareGaudiExec('DaVinci', args.dv, myPath=args.devpath)
else:
    print("Using dev area", devarea)
    app = GaudiExec()
    app.directory = devarea
data = BKQuery(bkloc, dqflag=['OK']).getDataset()

j = Job(name=name)
j.backend = Dirac()
j.application = app
j.application.options = [extraopts, optsfile]
j.application.platform = args.platform
j.outputfiles = [DiracFile('*.root'), LocalFile('*.xml')]
j.inputdata = data if not args.test else data[0:1]
j.splitter = SplitByFiles(filesPerJob=args.filesperjob)
#j.parallel_submit = True
#j.do_auto_resubmit = True

j.submit()
