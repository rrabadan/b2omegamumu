from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = '2018'
DaVinci().Simulation = True
DaVinci().CondDBtag = 'sim-20190430-vc-md100'
DaVinci().DDDBtag = 'dddb-20170721-3'
#DaVinci().Simulation = False
DaVinci().TupleFile = 'B2omegaMuMu.root'
DaVinci().EvtMax = 300

from GaudiConf import IOHelper
IOHelper().inputFiles([
    '00153186_00000496_7.AllStreams.dst'
    #'/storage/epp2/phsdpd/00162562_00000216_7.AllStreams.dst'
    #'/storage/epp2/phsdpd/00146430_00006590_1.leptonic.mdst'
    #'./00146430_00006590_1.leptonic.mdst'
    ], clear=True)
