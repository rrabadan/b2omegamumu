from Gaudi.Configuration import *

from Configurables import (
    EventTuple, DecayTreeTuple, TupleToolPi0Info, TupleToolPropertime,
    MCDecayTreeTuple, TupleToolGeneration, TupleToolCaloHypo, TupleToolTrigger,
    TupleToolTISTOS, TupleToolL0Data, TupleToolDecay, TupleToolDecayTreeFitter, TupleToolGeometry,
    LoKi__Hybrid__TupleTool, LoKi__Hybrid__EvtTupleTool, CheckPV, CondDB, TrackScaleState
)
from DecayTreeTuple.Configuration import *
from Configurables import DaVinci

from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive
from Configurables import ProcStatusCheck

# DaVinci config

DaVinci().PrintFreq = 1000

# Adapted from Jascha Grabowski's readAndMakeBplus2JpsiRhoPlusDST.py
# and Alex Ward's B2pipimumuNtuple.py

# Triggers
def triggers(era):
    if era == '2011' or era == '2012':
        return [
            'L0MuonDecision', 'L0DiMuonDecision', 'Hlt1TrackAllL0Decision',
            'Hlt1TrackMuonDecision', 'Hlt1GlobalDecision',
            'Hlt2Topo2BodyBBDTDecision', 'Hlt2Topo3BodyBBDTDecision',
            'Hlt2Topo4BodyBBDTDecision', 'Hlt2TopoMu2BodyBBDTDecision',
            'Hlt2TopoMu3BodyBBDTDecision', 'Hlt2TopoMu4BodyBBDTDecision'
        ]
    elif era == '2015':
        return [
            'L0MuonDecision', 'L0DiMuonDecision', 'Hlt1TrackMVADecision',
            'Hlt1TrackMuonDecision', 'Hlt1GlobalDecision',
            'Hlt2Topo2BodyDecision', 'Hlt2Topo3BodyDecision',
            'Hlt2Topo4BodyDecision', 'Hlt2TopoMu2BodyDecision',
            'Hlt2TopoMu3BodyDecision', 'Hlt2TopoMu4BodyDecision'
        ]
    else:
        return [
            'L0MuonDecision', 'L0DiMuonDecision', 
            'Hlt1TrackMVADecision', 'Hlt1TrackMuonDecision', 'Hlt1GlobalDecision',
            'Hlt2DimuonDecision', 'Hlt2DimuonDetachedDecision',
            'Hlt2GlobalDecision', 'Hlt2Topo2BodyDecision',
            'Hlt2Topo3BodyDecision', 'Hlt2Topo4BodyDecision',
            'Hlt2TopoMu2BodyDecision', 'Hlt2TopoMu3BodyDecision',
            'Hlt2TopoMu4BodyDecision', 'Hlt2TopoMuMu2BodyDecision',
            'Hlt2TopoMuMu3BodyDecision', 'Hlt2TopoMuMu4BodyDecision'
        ]

def ntupleIso(isoLocation):#check these are correct on stripping doc
    LoKi_Cone = LoKi__Hybrid__TupleTool('LoKi_Cone')
    LoKi_Cone.Variables = {
        "CONEANGLE": "RELINFO('"+isoLocation+"/ConeIsoInfo', 'CONEANGLE', -1.)",
        "CONEMULT": "RELINFO('"+isoLocation+"/ConeIsoInfo', 'CONEMULT', -1.)",
        "CONEPTASYM": "RELINFO('"+isoLocation+"/ConeIsoInfo', 'CONEPTASYM', -1.)",

        "VTXISONUMVTX": "RELINFO('"+isoLocation+"/VtxIsoInfo', 'VTXISONUMVTX', -1.)",
        "VTXISODCHI2ONETRACK": "RELINFO('"+isoLocation+"/VtxIsoInfo', 'VTXISODCHI2ONETRACK', -1.)",
        "VTXISODCHI2MASSONETRACK": "RELINFO('"+isoLocation+"/VtxIsoInfo', 'VTXISODCHI2MASSONETRACK', -1.)",
        "VTXISODCHI2TWOTRACK": "RELINFO('"+isoLocation+"/VtxIsoInfo', 'VTXISODCHI2TWOTRACK', -1.)",
        "VTXISODCHI2MASSTWOTRACK": "RELINFO('"+isoLocation+"/VtxIsoInfo', 'VTXISODCHI2MASSTWOTRACK', -1.)",

        "VTXISOBDTHARDFIRSTVALUE": "RELINFO('"+isoLocation+"/VtxIsoBDTInfo', 'VTXISOBDTHARDFIRSTVALUE', -1.)",
        "VTXISOBDTHARDSECONDVALUE": "RELINFO('"+isoLocation+"/VtxIsoBDTInfo', 'VTXISOBDTHARDSECONDVALUE', -1.)",
        "VTXISOBDTHARDTHIRDVALUE": "RELINFO('"+isoLocation+"/VtxIsoBDTInfo', 'VTXISOBDTHARDTHIRDVALUE', -1.)",
    }
    return LoKi_Cone

# Configuration
line = 'B2XMuMu_Line'
location = 'Phys/{}/Particles'.format(line)

stream = '/Event/AllStreams'
if DaVinci().Simulation is False:
    stream = '/Event/Leptonic'
    CondDB(LatestGlobalTagByDataType=str(DaVinci().DataType))

candlocation = '{}/{}'.format(stream, location)
if DaVinci().InputType == 'MDST':
    DaVinci().RootInTES = stream
    candlocation = location

isoLocation = stream + '/Phys/' + line
#smear = TrackScaleState('TrackScaleState')
scaler = TrackScaleState('StateScale')


BomegaMuMu1 = DecayTreeTuple('B2omegaMuMuTuple1')
BomegaMuMu2 = DecayTreeTuple('B2omegaMuMuTuple2')

BomegaMuMu1.Inputs = [candlocation]
BomegaMuMu2.Inputs = [candlocation]

# decay descriptors
# resolved pi0
# and merged pi0
BomegaMuMu1.Decay = '[B0 -> ^(J/psi(1S) -> ^mu- ^mu+) ^(omega(782) -> ^pi- ^pi+ ^(pi0 -> ^gamma ^gamma))]CC'
BomegaMuMu2.Decay = '[B0 -> ^(J/psi(1S) -> ^mu- ^mu+) ^(omega(782) -> ^pi- ^pi+ ^<pi0>)]CC'

tools = [
    'TupleToolEventInfo',
    'TupleToolGeometry',
    'TupleToolKinematic',
    'TupleToolPid',
    'TupleToolPrimaries',
    'TupleToolPropertime',
    'TupleToolAngles',
    'TupleToolTrackInfo',
    'TupleToolRecoStats',
    'TupleToolL0Data'
]

BomegaMuMu1.ToolList = tools

if DaVinci().Simulation is True:
    BomegaMuMu1.ToolList += [
        #'TupleToolMCTruth',
        'TupleToolMCBackgroundInfo'
    ]
    mctruthtool = BomegaMuMu1.addTupleTool('TupleToolMCTruth')
    mctruthtool.ToolList += ['MCTupleToolHierarchy']

# Branches for resolved pi0
BomegaMuMu1.addBranches({
    'B': '^([B0 -> (J/psi(1S) -> mu- mu+) (omega(782) -> pi- pi+ (pi0 -> gamma gamma))]CC)',
    'muplus': '[B0 -> (J/psi(1S) -> mu- ^mu+) (omega(782) -> pi- pi+ (pi0 -> gamma gamma))]CC',
    'muminus': '[B0 -> (J/psi(1S) -> ^mu- mu+) (omega(782) -> pi- pi+ (pi0 -> gamma gamma))]CC',
    'Jpsi': '[B0 -> ^(J/psi(1S) -> mu- mu+) (omega(782) -> pi- pi+ (pi0 -> gamma gamma))]CC',
    'omega': '[B0 -> (J/psi(1S) -> mu- mu+) ^(omega(782) -> pi- pi+ (pi0 -> gamma gamma))]CC',
    'piplus': '[B0 -> (J/psi(1S) -> mu- mu+) (omega(782) -> pi- ^pi+ (pi0 -> gamma gamma))]CC',
    'piminus': '[B0 -> (J/psi(1S) -> mu- mu+) (omega(782) -> ^pi- pi+ (pi0 -> gamma gamma))]CC',
    'pi0': '[B0 -> (J/psi(1S) -> mu- mu+) (omega(782) -> pi- pi+ ^(pi0 -> gamma gamma))]CC',
    'gamma1': '[B0 -> (J/psi(1S) -> mu- mu+) (omega(782) -> pi- pi+ (pi0 -> ^gamma gamma))]CC',
    'gamma2': '[B0 -> (J/psi(1S) -> mu- mu+) (omega(782) -> pi- pi+ (pi0 -> gamma ^gamma))]CC'
})

LoKi_B = LoKi__Hybrid__TupleTool('LoKi_B')
LoKi_B.Variables = {
    'BPVDLS': 'BPVDLS',
    'MAXMIPCHI2DV': 'MAXTREE(ISBASIC,MIPCHI2DV(PRIMARY))',
}
LoKi_Jpsi = LoKi__Hybrid__TupleTool('LoKi_Jpsi')
LoKi_Jpsi.Variables = {
    'BPVDLS': 'BPVDLS',
}

BomegaMuMu1.B.addTupleTool('TupleToolTISTOS')
BomegaMuMu1.B.TupleToolTISTOS.TriggerList = triggers(DaVinci().DataType)
BomegaMuMu1.B.TupleToolTISTOS.Verbose = True 
BomegaMuMu1.B.TupleToolTISTOS.VerboseL0 = True 
BomegaMuMu1.B.TupleToolTISTOS.VerboseHlt1 = True 
BomegaMuMu1.B.TupleToolTISTOS.VerboseHlt2 = True
BomegaMuMu1.B.addTupleTool(ntupleIso(isoLocation))
BomegaMuMu1.B.addTupleTool(LoKi_B)

# DTF
BomegaMuMu1.B.addTupleTool('TupleToolDecayTreeFitter/JpsiConsDTF')
BomegaMuMu1.B.JpsiConsDTF.constrainToOriginVertex = True
BomegaMuMu1.B.JpsiConsDTF.daughtersToConstrain = ['J/psi(1S)', 'pi0']
BomegaMuMu1.B.addTupleTool('TupleToolDecayTreeFitter/omegaJpsiConsDTF')
BomegaMuMu1.B.omegaJpsiConsDTF.constrainToOriginVertex = True
BomegaMuMu1.B.omegaJpsiConsDTF.daughtersToConstrain = ['J/psi(1S)', 'omega(782)', 'pi0']
BomegaMuMu1.B.addTupleTool('TupleToolDecayTreeFitter/omegaConsDTF')
BomegaMuMu1.B.omegaConsDTF.constrainToOriginVertex = True
BomegaMuMu1.B.omegaConsDTF.daughtersToConstrain = ['omega(782)', 'pi0']
BomegaMuMu1.B.addTupleTool('TupleToolDecayTreeFitter/pi0ConsDTF')
BomegaMuMu1.B.pi0ConsDTF.constrainToOriginVertex = True
BomegaMuMu1.B.pi0ConsDTF.daughtersToConstrain = ['pi0']
BomegaMuMu1.B.pi0ConsDTF.Verbose = True
BomegaMuMu1.B.pi0ConsDTF.UpdateDaughters = True


if DaVinci().Simulation is False:
    subst_psi = {
        'B0 -> ^J/psi(1S) (omega(782) -> pi- pi+ pi0)': 'psi(2S)',
        #'B~0 -> ^J/psi(1S) (omega~(782) -> pi- pi+ pi0)': 'psi(2S)'
    }
    BomegaMuMu1.B.addTupleTool('TupleToolDecayTreeFitter/Psi2SConsDTF')
    BomegaMuMu1.B.Psi2SConsDTF.constrainToOriginVertex = True
    BomegaMuMu1.B.Psi2SConsDTF.daughtersToConstrain = ['psi(2S)', 'pi0']
    BomegaMuMu1.B.Psi2SConsDTF.Substitutions = subst_psi

# Jpsi
BomegaMuMu1.Jpsi.addTupleTool('TupleToolTISTOS')
BomegaMuMu1.Jpsi.TupleToolTISTOS.TriggerList = triggers(DaVinci().DataType)[:5]
BomegaMuMu1.Jpsi.TupleToolTISTOS.Verbose = True 
BomegaMuMu1.Jpsi.TupleToolTISTOS.VerboseL0 = True 
BomegaMuMu1.Jpsi.TupleToolTISTOS.VerboseHlt1 = True 
BomegaMuMu1.Jpsi.TupleToolTISTOS.VerboseHlt2 = False
BomegaMuMu1.Jpsi.addTupleTool(LoKi_Jpsi)

# omega
BomegaMuMu1.omega.addTupleTool('TupleToolDecayTreeFitter/pi0ConsDTF')
BomegaMuMu1.omega.pi0ConsDTF.constrainToOriginVertex = False
BomegaMuMu1.omega.pi0ConsDTF.daughtersToConstrain = ['pi0']

#BomegaMuMu1.omega.addTupleTool('TupleToolConeIsolation/coneIso')
#BomegaMuMu1.omega.coneIso.ExtraParticlesLocation = 'Phys/StdAllNoPIDsMuons/Particles'
#BomegaMuMu1.omega.coneIso.ExtraPhotonsLocation = 'Phys/StdLooseAllPhotons/Particles'
#BomegaMuMu1.omega.coneIso.PizerosLocation = 'Phys/StdLoosePi02gg/Particles'
#BomegaMuMu1.omega.coneIso.MergedPizerosLocation = 'Phys/StdLooseMergedPi02/Particles'
#BomegaMuMu1.omega.coneIso.MaxPtParticlesLocation = 'Phys/StdAllLooseMuons/Particles'

# resolved pi0 tools
BomegaMuMu1.pi0.addTupleTool("TupleToolPi0Info")

# omega tools
BomegaMuMu1.gamma1.addTupleTool("TupleToolPhotonInfo")
# BomegaMuMu1.gamma1.addTupleTool("TupleToolCaloHypo")

BomegaMuMu1.gamma2.addTupleTool("TupleToolPhotonInfo")
# BomegaMuMu1.gamma2.addTupleTool("TupleToolCaloHypo")


BomegaMuMu2.ToolList = tools
if DaVinci().Simulation is True:
    BomegaMuMu2.ToolList += [
        #'TupleToolMCTruth',
        'TupleToolMCBackgroundInfo'
    ]
    #mctruthtool2 = BomegaMuMu2.addTupleTool('TupleToolMCTruth')
    #mctruthtool2.ToolList += ['MCTupleToolHierarchy']

# Branches for resolved pi0
BomegaMuMu2.addBranches({
    'B': '^([B0 -> (J/psi(1S) -> mu- mu+) (omega(782) -> pi- pi+ pi0)]CC)',
    'muplus': '[B0 -> (J/psi(1S) -> mu- ^mu+) (omega(782) -> pi- pi+ pi0)]CC',
    'muminus': '[B0 -> (J/psi(1S) -> ^mu- mu+) (omega(782) -> pi- pi+ pi0)]CC',
    'Jpsi': '[B0 -> ^(J/psi(1S) -> mu- mu+) (omega(782) -> pi- pi+ pi0)]CC',
    'omega': '[B0 -> (J/psi(1S) -> mu- mu+) ^(omega(782) -> pi- pi+ pi0)]CC',
    'piplus': '[B0 -> (J/psi(1S) -> mu- mu+) (omega(782) -> pi- ^pi+ pi0)]CC',
    'piminus': '[B0 -> (J/psi(1S) -> mu- mu+) (omega(782) -> ^pi- pi+ pi0)]CC',
    'pi0': '[B0 -> (J/psi(1S) -> mu- mu+) (omega(782) -> pi+ ^pi0)]CC',
})

BomegaMuMu2.B.addTupleTool('TupleToolDecayTreeFitter/JpsiConsDTF')
BomegaMuMu2.B.JpsiConsDTF.constrainToOriginVertex = True
BomegaMuMu2.B.JpsiConsDTF.daughtersToConstrain = ['J/psi(1S)', 'pi0']
BomegaMuMu2.B.addTupleTool('TupleToolDecayTreeFitter/omegaJpsiConsDTF')
BomegaMuMu2.B.omegaJpsiConsDTF.constrainToOriginVertex = True
BomegaMuMu2.B.omegaJpsiConsDTF.daughtersToConstrain = ['J/psi(1S)', 'omega(782)', 'pi0']
BomegaMuMu2.B.addTupleTool('TupleToolDecayTreeFitter/omegaConsDTF')
BomegaMuMu2.B.omegaConsDTF.constrainToOriginVertex = True
BomegaMuMu2.B.omegaConsDTF.daughtersToConstrain = ['omega(782)', 'pi0']
BomegaMuMu2.B.addTupleTool('TupleToolDecayTreeFitter/pi0ConsDTF')
BomegaMuMu2.B.pi0ConsDTF.constrainToOriginVertex = True
BomegaMuMu2.B.pi0ConsDTF.daughtersToConstrain = ['pi0']
BomegaMuMu2.B.pi0ConsDTF.Verbose = True
BomegaMuMu2.B.pi0ConsDTF.UpdateDaughters = True
BomegaMuMu2.B.addTupleTool('TupleToolTISTOS')
BomegaMuMu2.B.TupleToolTISTOS.TriggerList = triggers(DaVinci().DataType)
BomegaMuMu2.B.TupleToolTISTOS.Verbose = True 
BomegaMuMu2.B.TupleToolTISTOS.VerboseL0 = True 
BomegaMuMu2.B.TupleToolTISTOS.VerboseHlt1 = True 
BomegaMuMu2.B.TupleToolTISTOS.VerboseHlt2 = True
BomegaMuMu2.B.addTupleTool(ntupleIso(isoLocation))
BomegaMuMu2.B.addTupleTool(LoKi_B)

if DaVinci().Simulation is False:
    BomegaMuMu2.B.addTupleTool('TupleToolDecayTreeFitter/Psi2SConsDTF')
    BomegaMuMu2.B.Psi2SConsDTF.constrainToOriginVertex = True
    BomegaMuMu2.B.Psi2SConsDTF.daughtersToConstrain = ['psi(2S)', 'pi0']
    BomegaMuMu2.B.Psi2SConsDTF.Substitutions = subst_psi

# Jpsi
BomegaMuMu2.Jpsi.addTupleTool('TupleToolTISTOS')
BomegaMuMu2.Jpsi.TupleToolTISTOS.TriggerList = triggers(DaVinci().DataType)[:5]
BomegaMuMu2.Jpsi.TupleToolTISTOS.Verbose = True 
BomegaMuMu2.Jpsi.TupleToolTISTOS.VerboseL0 = True 
BomegaMuMu2.Jpsi.TupleToolTISTOS.VerboseHlt1 = True 
BomegaMuMu2.Jpsi.TupleToolTISTOS.VerboseHlt2 = False
BomegaMuMu2.Jpsi.addTupleTool(LoKi_Jpsi)

# omega
BomegaMuMu2.omega.addTupleTool('TupleToolDecayTreeFitter/pi0ConsDTF')
BomegaMuMu2.omega.pi0ConsDTF.constrainToOriginVertex = False
BomegaMuMu2.omega.pi0ConsDTF.daughtersToConstrain = ['pi0']

# pi0 tools
BomegaMuMu2.pi0.addTupleTool("TupleToolPi0Info")

DaVinci().appendToMainSequence([CheckPV(), BomegaMuMu1, BomegaMuMu2])
