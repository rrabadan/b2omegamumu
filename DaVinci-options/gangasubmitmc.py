import os
import argparse
import json
import subprocess

parser = argparse.ArgumentParser(description='ganga submit script mc')
parser.add_argument('mctype', type=str)
parser.add_argument('year', type=str,
                    choices=['2011', '2012', '2015', '2016', '2017', '2018'])
parser.add_argument('magpol', type=str,
                    choices=['MagDown', 'MagUp'])
parser.add_argument('--inputtype', type=str, nargs='?',
                    choices=['MDST', 'DST'], default='MDST')
parser.add_argument('--options', type=str, nargs='?',
                    default=None)
parser.add_argument('--filesperjob', type=int, nargs='?',
                    default=20)
parser.add_argument('--name', type=str, nargs='?',
                    default=None)
parser.add_argument('--mcsamples', type=str, nargs='?',
                    default='mcsamples.json')
parser.add_argument('--devpath', type=str, nargs='?',
                    default='.')
parser.add_argument('--dv', type=str, nargs='?',
                    default='v46r7')
parser.add_argument('--platform', type=str, nargs='?',
                    default='x86_64_v2-centos7-gcc12-opt')
parser.add_argument('--test', action='store_true')
args = parser.parse_args()

with open(args.mcsamples, 'r') as f:
    sample = json.load(f)[args.mctype]

samplename = sample['name']
dataset = sample['data'][f'{args.magpol}_{args.year}']
bkloc = dataset[0]
dddbtag = dataset[1]
condbtag = dataset[2]

name = args.name
optsfile = args.options

if name is None:
    name = '_'.join([samplename, args.mctype, args.magpol, args.year])
if optsfile is None:
    optsfile = 'B2omegaMuMuNtuple.py'

# Write extra opts
extraopts = os.path.join('extraopts', f'{name}.py')
if not os.path.exists('./extraopts'):
    os.makedirs('./extraopts')

with open(extraopts, 'w') as f:
    f.write("from Configurables import DaVinci\n")
    f.write(f"DaVinci().InputType = '{args.inputtype}'\n")
    f.write(f"DaVinci().DataType = '{args.year}'\n")
    f.write("DaVinci().Simulation = True\n")
    f.write(f"DaVinci().CondDBtag = '{condbtag}'\n")
    f.write(f"DaVinci().DDDBtag = '{dddbtag}'\n")
    f.write(f"DaVinci().TupleFile = 'B2omegaMuMu.root'")

devarea = os.path.join(args.devpath, f'DaVinciDev_{args.dv}')
if not os.path.exists(devarea):
    print("No Dev ", devarea)
    app = prepareGaudiExec('DaVinci', args.dv, myPath=args.devpath)
else:
    print("Using dev area", devarea)
    app = GaudiExec()
    app.directory = devarea
data = BKQuery(bkloc, dqflag=['OK']).getDataset()

j = Job(name=name)
j.backend = Dirac()
j.application = app
j.application.options = [extraopts, optsfile]
j.application.platform = args.platform
j.outputfiles = [LocalFile('*.root'), LocalFile('*.xml')]
j.inputdata = data if not args.test else data[0:2]
j.splitter = SplitByFiles(filesPerJob=args.filesperjob)
j.parallel_submit = True
#j.do_auto_resubmit = True

j.submit()
