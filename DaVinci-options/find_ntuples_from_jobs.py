import json
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('jobs', type=int, nargs='*')
parser.add_argument('--outfile', type=str, nargs='?', default='tupleslocs.json')
args = parser.parse_args()

def find_ntuples(jobsid):
    output = {}
    for i in jobsid:
        job = jobs(i)
        jobname = job.name.split('_')
        print('processing job:', i, jobname)
        if len(jobname) > 3:
            _, info = tuple(job.name.split('_', maxsplit=1))
            evtype, ds = tuple(info.split('_', maxsplit=1))
        else:
            evtype = 90000000
            ds = job.name
        try:
            output[evtype][ds] = []
        except:
            output[evtype] = {ds: []}
        for sj in job.subjobs:
            if sj.status != 'completed':
                continue
            for f in sj.outputfiles:
                if f.namePattern == 'B2omegaMuMu.root':
                    path = f'{f.localDir}/{f.namePattern}'
                    output[evtype][ds] += [path]

    outfile = args.outfile
    if not outfile.endswith('.json'):
        outfile += '.json'
    with open(outfile, 'w') as jf:
        json.dump(output, jf, indent=4)
    return

find_ntuples(args.jobs)
